import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const HeaderView = ({style, onSelect}) => {

	const navigation = useNavigation();

	return (
        <View style={[{
            width: Common.getLengthByIPhone7(0),
			marginTop: Common.getLengthByIPhone7(30),
			marginBottom: Common.getLengthByIPhone7(30),
			alignItems: 'center',
			justifyContent: 'center',
        }, style]}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<View>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'black',
					}}>
						Готово! Все впорядке?
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						color: 'black',
						opacity: 0.56,
						marginTop: Common.getLengthByIPhone7(7),
					}}>
						{`Перетащите фото, чтобы\nизменить порядок`}
					</Text>
				</View>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(128),
					height: Common.getLengthByIPhone7(41),
					borderRadius: Common.getLengthByIPhone7(22),
					alignItems: 'center',
					justifyContent: 'center',
					flexDirection: 'row',
					borderColor: '#b0b0b0',
					borderWidth: 1,
				}}
				onPress={() => {
					if (onSelect) {
						onSelect();
					}
				}}>
					<Image
						source={require('./../../assets/ic-upload.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(20),
							resizeMode: 'contain',
						}}
					/>
					<Text style={{
						marginLeft: 3,
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						color: 'black',
					}}>
						Загрузить
					</Text>
				</TouchableOpacity>
			</View>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(HeaderView);