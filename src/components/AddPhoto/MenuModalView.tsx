import React, { useEffect } from 'react';
import {Image, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { colors, typography } from './../../styles';
import Modal from "react-native-modal";
import { APP_NAME } from './../../constants';

const MenuModalView = ({title, data, show, onClose, onSelect}) => {

	const [body, setBody] = React.useState(null);
	const [showModal, setShowModal] = React.useState(false);

	useEffect(() => {
		setShowModal(show);
	}, [show]);

	const renderView = (id, title, icon, style) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(58),
			borderRadius: Common.getLengthByIPhone7(9),
			backgroundColor: colors.WHITE_COLOR,
			borderColor: '#cbcbcb',
			borderWidth: 1,
			flexDirection: 'row',
			alignItems: 'center',
		}, style]}
		onPress={() => {
			if (onSelect) {
				onSelect(id);
			}
		}}>
			<Image source={icon}
				style={{
					width: Common.getLengthByIPhone7(22),
					height: Common.getLengthByIPhone7(22),
					resizeMode: 'contain',
					marginLeft: Common.getLengthByIPhone7(16),
					marginRight: Common.getLengthByIPhone7(12),
				}}
			/>
			<Text style={typography.MAIN_TEXT_15}>
				{title}
			</Text>
		</TouchableOpacity>);
	}

    return (
		<Modal
			isVisible={showModal}
			backdropColor={'black'}
			backdropOpacity={0.7}
			style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<View style={{
				borderTopLeftRadius: Common.getLengthByIPhone7(24),
				borderTopRightRadius: Common.getLengthByIPhone7(24),
				width: Common.getLengthByIPhone7(0),
				marginBottom: -Common.getLengthByIPhone7(20),
				alignItems: 'center',
				justifyContent: 'center',
				paddingBottom: Common.getLengthByIPhone7(32),
				backgroundColor: 'white',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(60),
					alignItems: 'center',
					justifyContent: 'center',
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(18),
					}}>
						Выберите источник
					</Text>
				</View>
				{renderView(0, 'Загрузить фото', require('./../../assets/ic-upload-gallery.png'), {
					marginBottom: Common.getLengthByIPhone7(20),
				})}
				{renderView(1, 'Сделать новые фото ', require('./../../assets/ic-camera.png'), {
					marginBottom: Common.getLengthByIPhone7(40),
				})}
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(46),
					borderRadius: Common.getLengthByIPhone7(8),
					backgroundColor: 'black',
					alignItems: 'center',
					justifyContent: 'center',
				}}
				onPress={() => {
					if (onClose) {
						onClose();
					}
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
						color: 'white',
					}}>
						Отмена
					</Text>
				</TouchableOpacity>
			</View>
		</Modal>
	);
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	logout: () => dispatch.user.logout(),
});

export default connect(mstp, mdtp)(MenuModalView);