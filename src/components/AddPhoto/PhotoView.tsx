import React, { useCallback, useEffect, useRef } from 'react';
import { Image, StatusBar, View, TouchableOpacity, ScrollView, Linking, Text, Alert } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Common from './../../utilities/Common';
import { colors } from './../../styles';
import { typography } from './../../styles';
import LinearGradient from 'react-native-linear-gradient';

const PhotoView = ({style, type, photo, index, onDelete}) => {

	const [body, setBody] = React.useState(null);

	useEffect(() => {
		console.warn('photo: ', photo);
		if (photo) {
			setBody(renderImage());
		} else {
			if (type === 'big') {
				setBody(renderPlaceholder());
			} else {
				setBody(renderEmpty());
			}
		}
	}, [photo]);

	const renderPlaceholder = () => {
		return (
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(273),
				backgroundColor: 'white',
				alignItems: 'center',
				justifyContent: 'center',
				borderColor: 'rgba(0, 0, 0, 0.56)',
				borderWidth: 1,
				borderStyle: 'dashed',
			}}
			key={index}>
				<Image source={require('./../../assets/ic-placeholder-image.png')}
					style={{
						width: Common.getLengthByIPhone7(28),
						height: Common.getLengthByIPhone7(33),
						resizeMode: 'contain',
					}}
				/>
				<Text style={{
					textAlign: 'center',
					marginTop: Common.getLengthByIPhone7(10),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					lineHeight: Common.getLengthByIPhone7(27),
				}}>
					{`Загрузите фото сюда`}
				</Text>
				<Text style={{
					textAlign: 'center',
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					lineHeight: Common.getLengthByIPhone7(27),
					opacity: 0.53,
				}}>
					Добавьте хотя бы 5 фото 
				</Text>
			</View>);
	}

	const renderEmpty = () => {
		return (
		<View style={{
			width: Common.getLengthByIPhone7(158),
			height: Common.getLengthByIPhone7(110),
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'center',
			borderColor: 'rgba(0, 0, 0, 0.56)',
			borderWidth: 1,
			borderStyle: 'dashed',
		}}>
			<Image source={require('./../../assets/ic-placeholder-image.png')}
				style={{
					width: Common.getLengthByIPhone7(28),
					height: Common.getLengthByIPhone7(33),
					resizeMode: 'contain',
				}}
			/>
		</View>);
	}

	const renderImage = () => {
		return (
		<View style={[{
			width: type === 'big' ? Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40) : Common.getLengthByIPhone7(158),
			height: type === 'big' ? Common.getLengthByIPhone7(273) : Common.getLengthByIPhone7(96),
			backgroundColor: colors.LIGHT_GRAY_COLOR,
			overflow: 'hidden',
		}, style]}>
			<Image source={{uri: photo}}
				style={{
					resizeMode: 'cover',
					position: 'absolute',
					left: 0,
					top: 0,
					bottom: 0,
					right: 0,
				}}
			/>
			<TouchableOpacity style={{
				position: 'absolute',
				top: Common.getLengthByIPhone7(8),
				right: Common.getLengthByIPhone7(9),
			}}
			onPress={() => {
				if (onDelete) {
					onDelete(index);
				}
			}}>
				<Image source={require('./../../assets/ic-close.png')}
					style={{
						width: Common.getLengthByIPhone7(29),
						height: Common.getLengthByIPhone7(29),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
		</View>);
	}

	return (body);
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
	// userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	// getMyCategories: () => dispatch.user.getMyCategories(),
	// saveMyCategories:payload => dispatch.user.saveMyCategories(payload),
	// getPicture:payload => dispatch.user.getPicture(payload),
	// showToastMessage: payload => dispatch.buttons.showToastMessage(payload),
});

export default connect(mstp, mdtp)(PhotoView);