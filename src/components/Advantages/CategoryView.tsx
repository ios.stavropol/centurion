import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const CategoryView = ({title, style, icon, onSelect}) => {

	const navigation = useNavigation();
	const [selected, setSelected] = React.useState(false);

	useEffect(() => {
		if (onSelect) {
			onSelect(selected);
		}
	}, [selected]);

	return (
        <TouchableOpacity style={[{
            width: Common.getLengthByIPhone7(161),
			height: Common.getLengthByIPhone7(93),
			borderRadius: Common.getLengthByIPhone7(7),
			borderColor: '#bbbbbb',
			borderWidth: 1,
            backgroundColor: selected ? 'black' : 'white',
            alignItems: 'center',
			justifyContent: 'center',
        }, style]}
		onPress={() => {
			setSelected(!selected);
		}}>
			<Image
				source={{uri: icon}}
				style={{
					width: Common.getLengthByIPhone7(31),
					height: Common.getLengthByIPhone7(25),
					marginBottom: Common.getLengthByIPhone7(8),
					resizeMode: 'contain',
					tintColor: selected ? 'white' : 'black',
				}}
			/>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(14),
				color: selected ? 'white' : 'black',
			}}>
				{title}
			</Text>
        </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(CategoryView);