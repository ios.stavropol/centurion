import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';
import MapView, {Marker} from 'react-native-maps';

const AboutView = ({style, data}) => {

	const navigation = useNavigation();

	const [lat, setLat] = React.useState(0);
	const [lng, setLng] = React.useState(0);
	const mapRef = React.createRef();

	useEffect(() => {
		if (data) {
			let geo = JSON.parse(data?.geo);
			console.warn(geo);
			if (geo?.length === 2) {
				setLat(geo[0]);
				setLng(geo[1]);
			}
		}
	}, [data])

	useEffect(() => {
		if (mapRef !== null) {
			setTimeout(() => {
				mapRef?.current?.animateToRegion({
					latitude: lat,
					longitude: lng,
					latitudeDelta: 0.01,
					longitudeDelta: 0.01,
				}, 100);
			}, 100);
		}
	}, [lat, lng]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
			paddingBottom: Common.getLengthByIPhone7(30),
			paddingTop: Common.getLengthByIPhone7(30),
		}, style]}>
			<Text style={{
				fontFamily: 'SFProDisplay-Medium',
				fontSize: Common.getLengthByIPhone7(20),
			}}>
				Где вы будете 
			</Text>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				marginTop: Common.getLengthByIPhone7(20),
			}}>
				{data?.description}
			</Text>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(210),
				borderRadius: Common.getLengthByIPhone7(10),
				overflow: 'hidden',
				marginTop: Common.getLengthByIPhone7(25),
			}}>
				<MapView
					ref={mapRef}
					// initialRegion={{
					// 	latitude: lat,
					// 	longitude: lng,
					// 	latitudeDelta: 0.0922,
					// 	longitudeDelta: 0.0421,
					// }}
					// scrollEnabled = {false}
					style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(210),
					}}
					urlTemplate={'http://c.tile.openstreetmap.org/{z}/{x}/{y}.png'}
				>
					<Marker
						coordinate={{
							latitude: lat,
							longitude: lng,
						}}
					>
						<Image source={require('./../../assets/ic-home-location.png')}
							style={{
								height: Common.getLengthByIPhone7(120),
								width: Common.getLengthByIPhone7(120),
								resizeMode: 'contain',
							}}
						/>
					</Marker>
				</MapView>
			</View>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				marginTop: Common.getLengthByIPhone7(30),
			}}>
				{data?.region}
			</Text>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(12),
				marginTop: Common.getLengthByIPhone7(11),
				opacity: 0.5,
			}}>
				Точное местоположение жилья вы узнаете после бронирования
			</Text>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(AboutView);