import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const ButtonView = ({style, title, subtitle, action}) => {

	const navigation = useNavigation();

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			paddingBottom: Common.getLengthByIPhone7(20),
			paddingTop: Common.getLengthByIPhone7(20),
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
			borderTopColor: 'rgba(0, 0, 0, 0.33)',
			borderTopWidth: 1,
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
		}, style]}
		onPress={() => {
			if (action) {
				action();
			}
		}}>
			<View>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(20),
				}}>
					{title}
				</Text>
				{subtitle ? (<Text style={{
					marginTop: Common.getLengthByIPhone7(10),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					opacity: 0.5,
				}}>
					{subtitle}
				</Text>) : null}
			</View>
			<Image
				source={require('./../../assets/ic-arrow-right.png')}
				style={{
					width: Common.getLengthByIPhone7(7),
					height: Common.getLengthByIPhone7(14),
					resizeMode: 'contain',
				}}
			/>
		</TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(ButtonView);