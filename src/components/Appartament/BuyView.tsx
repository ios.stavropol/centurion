import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';
import GradientButton from '../GradientButton';

const BuyView = ({style, data, onClick, action}) => {

	const navigation = useNavigation();

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			paddingBottom: Common.getLengthByIPhone7(30),
			paddingTop: Common.getLengthByIPhone7(20),
			paddingLeft: Common.getLengthByIPhone7(20),
			paddingRight: Common.getLengthByIPhone7(20),
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
			borderTopColor: 'rgba(0, 0, 0, 0.33)',
			borderTopWidth: 1,
		}, style]}>
			<View>
				<Text style={{
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(18),
				}}>
					&#8381; {data?.price} <Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					opacity: 0.5,
				}}>
					/ ночь
				</Text>
				</Text>
				<Text style={{
					marginTop: Common.getLengthByIPhone7(8),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					textDecorationLine: 'underline',
				}}>
					26-29 апр
				</Text>
			</View>
			<GradientButton
				title={'Забронировать'}
				style={{
					width: Common.getLengthByIPhone7(154),
					height: Common.getLengthByIPhone7(50),
				}}
				onPress={() => {
					if (onClick) {
						onClick();
					}
				}}
			/>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(BuyView);