import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const CommentsView = ({style, data}) => {

	const navigation = useNavigation();

	const [lat, setLat] = React.useState(0);

	useEffect(() => {
		
	}, [data]);

	const renderView = (style) => {
		return (<View style={[{
			width: Common.getLengthByIPhone7(320),
			height: Common.getLengthByIPhone7(230),
			borderRadius: Common.getLengthByIPhone7(10),
			borderColor: 'rgba(0, 0, 0, 0.33)',
			borderWidth: 1,
			paddingLeft: Common.getLengthByIPhone7(10),
			paddingRight: Common.getLengthByIPhone7(10),
			paddingTop: Common.getLengthByIPhone7(20),
			paddingBottom: Common.getLengthByIPhone7(20),
		}, style]}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				marginBottom: Common.getLengthByIPhone7(15),
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(40),
					borderRadius: Common.getLengthByIPhone7(20),
					backgroundColor: 'rgba(0, 0, 0, 0.33)',
				}}>

				</View>
				<View style={{
					marginLeft: Common.getLengthByIPhone7(10),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
						marginBottom: Common.getLengthByIPhone7(4),
					}}>
						Арсений
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(12),
						opacity: 0.5,
					}}>
						2 месяца назад
					</Text>
				</View>
			</View>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				marginBottom: Common.getLengthByIPhone7(15),
			}}>
				Дом — результат строительства, представляющий в себя помещения, предназначенные для грязи и грязных просто так, проститутка, предназначенные для грязи...
			</Text>
			<TouchableOpacity>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					textDecorationLine: 'underline',
				}}>
					Подробнее
				</Text>
			</TouchableOpacity>
		</View>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
			paddingBottom: Common.getLengthByIPhone7(30),
			paddingTop: Common.getLengthByIPhone7(15),
			alignItems: 'center',
		}, style]}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				marginBottom: Common.getLengthByIPhone7(20),
			}}>
				<Image
					source={require('./../../assets/ic-star.png')}
					style={{
						width: Common.getLengthByIPhone7(25),
						height: Common.getLengthByIPhone7(25),
						resizeMode: 'contain',
					}}
				/>
				<Text style={{
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(16),
					marginLeft: Common.getLengthByIPhone7(5),
				}}>
					5.0
				</Text>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					marginLeft: Common.getLengthByIPhone7(5),
				}}>
					(74 отзыва)
				</Text>
			</View>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(230),
			}}
			horizontal={true}
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				{renderView({
					marginLeft: Common.getLengthByIPhone7(20),
				})}
				{renderView({
					marginLeft: Common.getLengthByIPhone7(15),
				})}
				{renderView({
					marginLeft: Common.getLengthByIPhone7(15),
					marginRight: Common.getLengthByIPhone7(20),
				})}
			</ScrollView>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(44),
				borderRadius: Common.getLengthByIPhone7(8),
				marginTop: Common.getLengthByIPhone7(30),
				alignItems: 'center',
				justifyContent: 'center',
				borderColor: '#222222',
				borderWidth: 1,
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}>
					Показать все 24 отзыва
				</Text>
			</TouchableOpacity>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(CommentsView);