import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const FeaturesView = ({style, data}) => {

	const navigation = useNavigation();

	const [body, setBody] = React.useState(null);

	useEffect(() => {
		if (data) {
			let array = [];

			if (data.facilities) {
				for (let i = 0; i < data.facilities.length; i++) {
					array.push(renderView(data.facilities[i]));
				}
			}

			if (data.schticks) {
				for (let i = 0; i < data.schticks.length; i++) {
					array.push(renderView(data.schticks[i]));
				}
			}

			if (data.additional_tags) {
				for (let i = 0; i < data.additional_tags.length; i++) {
					array.push(renderView(data.additional_tags[i]));
				}
			}

			if (data.popular_facilities) {
				for (let i = 0; i < data.popular_facilities.length; i++) {
					array.push(renderView(data.popular_facilities[i]));
				}
			}

			if (data.safety_facilities) {
				for (let i = 0; i < data.safety_facilities.length; i++) {
					array.push(renderView(data.safety_facilities[i]));
				}
			}

			setBody(array);
		}
	}, [data]);

	const renderView = (obj) => {
		return (<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			flexDirection: 'row',
			alignItems: 'center',
			marginTop: Common.getLengthByIPhone7(20),
		}}>
			<Image
				source={{uri: BASE_URL + obj.icon}}
				style={{
					width: Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(20),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				marginLeft: Common.getLengthByIPhone7(20),
			}}>
				{obj.name}
			</Text>
		</View>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
			paddingBottom: Common.getLengthByIPhone7(20),
		}, style]}>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(20),
			}}>
				Удобства
			</Text>
			{body}
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(FeaturesView);