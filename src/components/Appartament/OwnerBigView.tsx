import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const OwnerBigView = ({style, data}) => {

	const navigation = useNavigation();

	const [lat, setLat] = React.useState(0);

	useEffect(() => {
		
	}, [data]);

	const renderView = (title, icon, style) => {
		return (<View style={[{
			flexDirection: 'row',
			alignItems: 'center',
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
		}, style]}>
			<Image
				source={icon}
				style={{
					width: Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(20),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				marginLeft: Common.getLengthByIPhone7(10),
			}}>
				{title}
			</Text>
		</View>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			paddingBottom: Common.getLengthByIPhone7(25),
			paddingTop: Common.getLengthByIPhone7(39),
			alignItems: 'center',
		}, style]}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
				marginBottom: Common.getLengthByIPhone7(20),
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(20),
				}}>
					Хозяин: {data?.owner?.first_name} {data?.owner?.last_name}
				</Text>
				<View style={{
					width: Common.getLengthByIPhone7(51),
					height: Common.getLengthByIPhone7(51),
					borderRadius: Common.getLengthByIPhone7(51) / 2,
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: 'gray',
				}}>

				</View>
			</View>
			{renderView('159 отзывов', require('./../../assets/ic-star.png'), {
				marginTop: Common.getLengthByIPhone7(5),
			})}
			{renderView('Личность подтверждена', require('./../../assets/ic-approval.png'), {
				marginTop: Common.getLengthByIPhone7(10),
			})}
			{renderView('Мега хозяин', require('./../../assets/ic-medal.png'), {
				marginTop: Common.getLengthByIPhone7(10),
			})}
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				marginTop: Common.getLengthByIPhone7(30),
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
			}}>
				Дом — результат строительства, представляющий в себя помещения, предназначенные для грязи и грязных просто так, проститутка, предназначен для грязи...
			</Text>
			<Text style={{
				marginTop: Common.getLengthByIPhone7(20),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontFamily: 'SFProDisplay-Medium',
				fontSize: Common.getLengthByIPhone7(16),
			}}>
				{data?.owner?.first_name} {data?.owner?.last_name} - мегахозяин
			</Text>
			<Text style={{
				marginTop: Common.getLengthByIPhone7(20),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
			}}>
				Мегахозяин – это опытный хозяин с высокими оценками, который делает все для комфорта гостей
			</Text>
			<Text style={{
				marginTop: Common.getLengthByIPhone7(30),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
			}}>
				Время ответа – в течении часа
			</Text>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(44),
				borderRadius: Common.getLengthByIPhone7(8),
				marginTop: Common.getLengthByIPhone7(30),
				alignItems: 'center',
				justifyContent: 'center',
				borderColor: '#222222',
				borderWidth: 1,
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}>
					Связаться с хозяином
				</Text>
			</TouchableOpacity>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				justifyContent: 'space-between',
				marginTop: Common.getLengthByIPhone7(25),
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(12),
				}}>
					В целях безопасности не переводите деньги и не общайтесь за пределами сайта или приложения Centurion
				</Text>
				<Image
					source={require('./../../assets/ic-secure.png')}
					style={{
						width: Common.getLengthByIPhone7(25),
						height: Common.getLengthByIPhone7(25),
						resizeMode: 'contain',
					}}
				/>
			</View>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(OwnerBigView);