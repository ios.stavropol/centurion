import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const OwnerView = ({style, data, onStartChat}) => {

	const navigation = useNavigation();

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			borderTopColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderTopWidth: 1,
			borderBottomWidth: 1,
			paddingTop: Common.getLengthByIPhone7(20),
			paddingBottom: Common.getLengthByIPhone7(20),
		}, style]}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(51),
					height: Common.getLengthByIPhone7(51),
					borderRadius: Common.getLengthByIPhone7(51) / 2,
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: 'gray',
				}}>

				</View>
				<View style={{
					marginLeft: Common.getLengthByIPhone7(10),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
					}}>
						{data?.owner?.first_name} {data?.owner?.last_name}
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						opacity: 0.5,
						marginTop: Common.getLengthByIPhone7(7),
					}}>
						Хозяин жилья
					</Text>
				</View>
			</View>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(20),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: '#FFC453',
			}}
			onPress={() => {
				if (onStartChat) {
					onStartChat();
				}
			}}>
				<Image
					source={require('./../../assets/ic-bubble.png')}
					style={{
						width: Common.getLengthByIPhone7(20),
						height: Common.getLengthByIPhone7(20),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(OwnerView);