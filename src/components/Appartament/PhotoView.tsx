import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { BASE_URL } from '../../constants';
import GradientButton from '../GradientButton';

const { width } = Dimensions.get('window');

const PhotoView = ({style, data, onClose, onShare, onFavor}) => {

	const scroll = useRef(null);
	const [body, setBody] = React.useState(null);
	const [page, setPage] = React.useState(0);
	const [count, setCount] = React.useState(0);

	scrollX = new Animated.Value(0)

	useEffect(() => {
		console.warn('useEffect data');
		if (data) {
			let array = [];
			let cc = 0;
			if (data?.cover) {
				array.push(<Image
					source={{uri: BASE_URL + data?.cover.photo}}
					style={{
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(420),
						resizeMode: 'cover',
					}}
				/>);
				cc++;
			}
			for (let i = 0; i < data?.photos.length; i++) {
				array.push(<Image
					source={{uri: BASE_URL + data?.photos[i].photo}}
					style={{
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(420),
						resizeMode: 'cover',
					}}
				/>);
				cc++;
			}
			setBody(array);
			setCount(cc);
		}
	}, [data]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			height: Common.getLengthByIPhone7(420),
			borderBottomLeftRadius: Common.getLengthByIPhone7(50),
			borderBottomRightRadius: Common.getLengthByIPhone7(50),
			backgroundColor: 'gray',
			alignItems: 'center',
			overflow: 'hidden',
		}, style]}>
			<ScrollView style={[{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(420),
				overflow: 'hidden',
				borderBottomLeftRadius: Common.getLengthByIPhone7(50),
				borderBottomRightRadius: Common.getLengthByIPhone7(50),
			}, style]}
			ref={scroll}
			horizontal={true}
			pagingEnabled={true}
			showsHorizontalScrollIndicator={false}
			onScroll={Animated.event(
				[{ nativeEvent: { contentOffset: { x: scrollX } } }],
				{
					listener: event => {
						let page = Math.round(parseFloat(event.nativeEvent.contentOffset.x/Dimensions.get('window').width));
						setPage(page);
					},
				},)
			}
			scrollEventThrottle={16}
			contentContainerStyle={{
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				{body}
			</ScrollView>
			<View style={{
				position: 'absolute',
				bottom: Common.getLengthByIPhone7(37),
				right: Common.getLengthByIPhone7(20),
				width: Common.getLengthByIPhone7(58),
				height: Common.getLengthByIPhone7(25),
				borderRadius: Common.getLengthByIPhone7(40),
				backgroundColor: 'white',
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}>
					{page + 1} / {count}
				</Text>
			</View>
			<View style={{
				position: 'absolute',
				top: Common.getLengthByIPhone7(60),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<TouchableOpacity
					style={{

					}}
					onPress={() => {
						if (onClose) {
							onClose();
						}
					}}
				>
					<Image
						source={require('./../../assets/ic-close.png')}
						style={{
							width: Common.getLengthByIPhone7(51),
							height: Common.getLengthByIPhone7(51),
							resizeMode: 'contain',
						}}
					/>
				</TouchableOpacity>
				<View style={{
					flexDirection: 'row',
				}}>
					<TouchableOpacity
						style={{

						}}
						onPress={() => {
							if (onShare) {
								onShare();
							}
						}}
					>
						<Image
							source={require('./../../assets/ic-share.png')}
							style={{
								width: Common.getLengthByIPhone7(51),
								height: Common.getLengthByIPhone7(51),
								resizeMode: 'contain',
							}}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						style={{
							marginLeft: Common.getLengthByIPhone7(20),
							width: Common.getLengthByIPhone7(51),
							height: Common.getLengthByIPhone7(51),
							borderRadius: Common.getLengthByIPhone7(51) / 2,
							backgroundColor: 'white',
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							if (onFavor) {
								onFavor();
							}
						}}
					>
						{data?.is_favourite ? (<Image
							source={require('./../../assets/ic-like.png')}
							style={{
								width: Common.getLengthByIPhone7(25),
								height: Common.getLengthByIPhone7(25),
								resizeMode: 'contain',
							}}
						/>) : (<Image
							source={require('./../../assets/ic-unlike.png')}
							style={{
								width: Common.getLengthByIPhone7(25),
								height: Common.getLengthByIPhone7(25),
								resizeMode: 'contain',
							}}
						/>)}
					</TouchableOpacity>
				</View>
			</View>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(PhotoView);