import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const RoomsView = ({style, data}) => {

	const navigation = useNavigation();

	const [body, setBody] = React.useState(null);

	const renderView = (icon, text, style) => {
		return (<View style={[{
			flexDirection: 'row',
			alignItems: 'center',
		}, style]}>
			<Image
				source={icon}
				style={{
					width: Common.getLengthByIPhone7(30),
					height: Common.getLengthByIPhone7(30),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				textDecorationLine: 'underline',
				marginLeft: Common.getLengthByIPhone7(10),
			}}>
				{text}
			</Text>
		</View>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
			paddingBottom: Common.getLengthByIPhone7(30),
			paddingTop: Common.getLengthByIPhone7(30),
		}, style]}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
			}}>
				{renderView(require('./../../assets/ic-room.png'), Common.getDigitStr(data?.rooms, {
					d1: ' спальня',
					d2_d4: ' спальни',
					d5_d10: ' спален',
					d11_d19: ' спален',
				}), {
					width: Common.getLengthByIPhone7(170),
				})}
				{renderView(require('./../../assets/ic-bed2.png'), Common.getDigitStr(data?.beds, {
					d1: ' кровать',
					d2_d4: ' кровати',
					d5_d10: ' кроватей',
					d11_d19: ' кроватей',
				}), {
					// width: Common.getLengthByIPhone7(170),
				})}
			</View>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				marginTop: Common.getLengthByIPhone7(20),
			}}>
				{renderView(require('./../../assets/ic-bath.png'), Common.getDigitStr(data?.bathrooms, {
					d1: ' ванная',
					d2_d4: ' ванные',
					d5_d10: ' ванных',
					d11_d19: ' ванных',
				}), {
					width: Common.getLengthByIPhone7(170),
				})}
				{renderView(require('./../../assets/ic-guest.png'), Common.getDigitStr(data?.guest, {
					d1: ' гость',
					d2_d4: ' гостя',
					d5_d10: ' гостей',
					d11_d19: ' гостей',
				}), {
					// width: Common.getLengthByIPhone7(170),
				})}
			</View>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(RoomsView);