import React, { useEffect } from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from '../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';

const BackButton = ({navigation, style}) => {
	// const navigation = useNavigation();

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(32),
			height: Common.getLengthByIPhone7(32),
			alignItems: 'flex-start',
			justifyContent: 'center',
		}, style]}
		onPress={() => {
			// if (setBackFun) {
			// 	setBackFun();
			// } else {
				navigation.goBack();
			// }
		}}>
			<Image
				source={require('./../assets/ic-arrow-back.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(24),
					height: Common.getLengthByIPhone7(24),
					// tintColor: 'black',
				}}
			/>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	setBackFun: payload => dispatch.buttons.setBackFun(payload),
});

export default connect(mstp, mdtp)(BackButton);