import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';

const TopView = ({onPress, data}) => {

	const navigation = useNavigation();
	const [profile, setProfile] = React.useState(null);

	useEffect(() => {
		console.warn(data);
		setProfile(data);
	}, [data]);

	return (
		<View style={{
			width: Common.getLengthByIPhone7(0),
			height: Common.getLengthByIPhone7(120),
			borderBottomColor: 'rgba(125, 127, 136, 0.46)',
			borderBottomWidth: 1,
			alignItems: 'flex-start',
			justifyContent: 'flex-end',
			padding: Common.getLengthByIPhone7(16),
		}}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
			}}>
				<TouchableOpacity style={{

				}}
				onPress={() => {
					navigation.goBack();
				}}>
					<Image
						source={require('./../../assets/ic-arrow-back.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(20),
						}}
					/>
				</TouchableOpacity>
				<View style={{
					flexDirection: 'row',
					alignItems: 'center',
					marginLeft: Common.getLengthByIPhone7(16),
				}}>
					<View style={{
						width: Common.getLengthByIPhone7(42),
						height: Common.getLengthByIPhone7(42),
						borderRadius: Common.getLengthByIPhone7(21),
						backgroundColor: '#c4c4c4',
						overflow: 'hidden',
					}}>
						<Image
							source={{uri: BASE_URL + profile?.cover?.photo}}
							style={{
								width: Common.getLengthByIPhone7(42),
								height: Common.getLengthByIPhone7(42),
							}}
						/>
					</View>
					<Text style={{
						fontSize: Common.getLengthByIPhone7(16),
						marginLeft: Common.getLengthByIPhone7(12),
						fontFamily: 'SFProDisplay-Bold',
						fontWeight: 'normal',
						color: '#141414',
					}}>
						{profile?.header}
					</Text>
				</View>
			</View>
		</View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	searchAddress: payload => dispatch.user.searchAddress(payload),
});

export default connect(mstp, mdtp)(TopView);