import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';

const SearchView = ({onPress, searchAddress}) => {

	const navigation = useNavigation();
	const [search, setSearch] = React.useState('');
	const [body, setBody] = React.useState(null);
	const [height, setHeight] = React.useState(Common.getLengthByIPhone7(60));

	const renderAddress = (data) => {
		return (<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			alignItems: 'center',
			justifyContent: 'center',
		}}
		onPress={() => {
			Alert.alert(
				APP_NAME,
				'Вы уверены в выборе?',
				[
				  {
					text: "Нет",
					onPress: () => console.log("Cancel Pressed"),
					style: "cancel"
				  },
				  { text: "Да", onPress: () => {
					  if (onPress) {
						onPress(data);
					  }
					//   navigation.navigate('ConfirmAddress', {data: data});
				  }}
				]
			);
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				paddingBottom: Common.getLengthByIPhone7(12),
				paddingTop: Common.getLengthByIPhone7(12),
				borderBottomColor: 'rgba(0, 0, 0, 0.33)',
				borderBottomWidth: 1,
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(43),
					height: Common.getLengthByIPhone7(43),
					borderRadius: Common.getLengthByIPhone7(6),
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: '#d8d8d8',
					marginRight: Common.getLengthByIPhone7(12),
				}}>
					<Image
						source={require('./../../assets/ic-geotag.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(20),
						}}
					/>
				</View>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					color: '#343434',
				}}>
					{data.value}
				</Text>
			</View>
		</TouchableOpacity>);
	}

	useEffect(() => {
		searchAddress(search)
		.then(resp => {
			let array = [];
			for (let i = 0; i < resp.length; i++) {
				array.push(renderAddress(resp[i]));
			}
			setBody(array);
			if (resp.length === 0) {
				setHeight(Common.getLengthByIPhone7(60));
			} else if (resp.length < 6) {
				setHeight(resp.length * Common.getLengthByIPhone7(60));
			} else {
				setHeight(6 * Common.getLengthByIPhone7(60));
			}
		})
		.catch(err => {

		});
	}, [search]);

	return (
		<View style={{
			marginTop: Common.getLengthByIPhone7(60),
			// width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			// height: Common.getLengthByIPhone7(48),
			// borderRadius: Common.getLengthByIPhone7(72),
		}}>
			<TextInput
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(48),
					borderRadius: Common.getLengthByIPhone7(72),
					backgroundColor: '#F2F2F3',
					borderColor: '#E3E3E7',
					borderWidth: 1,
					fontSize: Common.getLengthByIPhone7(16),
					paddingLeft: Common.getLengthByIPhone7(56),
					letterSpacing: -0.408,
					fontFamily: 'SFProDisplay-Regular',
					fontWeight: 'normal',
					color: '#141414',
					textAlign: 'left',
				}}
				placeholderTextColor={'#7D7F88'}
				placeholder={'Поиск по сообщениям'}
				contextMenuHidden={false}
				autoCorrect={false}
				autoCompleteType={'off'}
				returnKeyType={'done'}
				secureTextEntry={false}
				// keyboardType={'number-pad'}
				allowFontScaling={false}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
					// this.nextClick();
				}}
				onFocus={() => {}}
				onBlur={() => {
				
				}}
				onChangeText={code => {
					setSearch(code);
				}}
				value={search}
			/>
		</View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	searchAddress: payload => dispatch.user.searchAddress(payload),
});

export default connect(mstp, mdtp)(SearchView);