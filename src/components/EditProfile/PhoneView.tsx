import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';
import { FloatingLabelInput } from 'react-native-floating-label-input';

const PhoneView = ({style, value, onChange}) => {

	const navigation = useNavigation();
	const [cont, setCont] = React.useState('');

	useEffect(() => {
		setCont(value);
	}, [value]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			paddingTop: Common.getLengthByIPhone7(30),
			paddingBottom: Common.getLengthByIPhone7(30),
			borderTopColor: 'rgba(0, 0, 0, 0.2)',
			borderTopWidth: 1,
			borderBottomColor: 'rgba(0, 0, 0, 0.2)',
			borderBottomWidth: 1,
		}, style]}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(18),
				}}>
					Номер телефона
				</Text>
				<TouchableOpacity>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						textDecorationLine: 'underline',
					}}>
						Редактировать
					</Text>
				</TouchableOpacity>
			</View>
			<Text style={{
				marginTop: Common.getLengthByIPhone7(5),
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				color: '#717171',
			}}>
				{`Для уведомлений, напоминаний\nи помощи со входом`}
			</Text>
			<Text style={{
				marginTop: Common.getLengthByIPhone7(30),
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(18),
			}}>
				{Common.reformatPhone(cont)}
			</Text>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(PhoneView);