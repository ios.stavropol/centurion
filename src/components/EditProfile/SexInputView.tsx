import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import ActionSheet from 'react-native-actionsheet';

const sex = [
	{id: 'male', name: 'Мужской'},
	{id: 'female', name: 'Женский'},
];

const SexInputView = ({style, title, value, onChange}) => {

	const navigation = useNavigation();
	const [selected, setSelected] = React.useState('');
	const actionSheet = useRef(null);

	useEffect(() => {
		let sel = '';
		for (let i = 0; i < sex.length; i++) {
			if (sex[i].id === value) {
				sel = sex[i].name;
				break;
			}
		}
		setSelected(sel);
	}, [value]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
		}, style]}>
			<FloatingLabelInput
				label={title}
				value={selected}
				containerStyles={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(60),
					paddingHorizontal: Common.getLengthByIPhone7(17),
					backgroundColor: 'white',
					borderColor: 'rgba(0, 0, 0, 0.3)',
					borderWidth: 1,
					borderRadius: Common.getLengthByIPhone7(10),
				}}
				customLabelStyles={{
					leftFocused: -4,
					leftBlurred: 0,
					topFocused: -20,
					fontSizeFocused: Common.getLengthByIPhone7(16),
					fontSizeBlurred: Common.getLengthByIPhone7(18),
					colorFocused: '#717171',
					colorBlurred: '#717171',
				}}
				labelStyles={{
					color: '#717171',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'SFProRounded-Regular',
					fontWeight: '400',
				}}
				inputStyles={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					color: 'black',
					fontSize: Common.getLengthByIPhone7(18),
					fontFamily: 'SFProRounded-Regular',
					fontWeight: '400',
				}}
			/>
			<TouchableOpacity
				style={{
					position: 'absolute',
					left: 0,
					top: 0,
					right: 0,
					bottom: 0,
				}}
				onPress={() => {
					actionSheet.current.show();
				}}
			/>
			<ActionSheet
				ref={actionSheet}
				title={'Выберите пол'}
				options={['Мужской', 'Женский', 'Отмена']}
				cancelButtonIndex={2}
				onPress={index => {
					if (index === 0) {
						setSelected(sex[0].name);
						if (onChange) {
							onChange(sex[index].id);
						}
					} else if (index === 1) {
						setSelected(sex[1].name);
						if (onChange) {
							onChange(sex[index].id);
						}
					}
				}}
			/>
		</View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(SexInputView);