import React, { useCallback, useEffect, useRef } from 'react';
import { Image, StatusBar, View, TouchableOpacity, Linking, Text, Alert } from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Common from '../utilities/Common';
import { colors, typography } from './../styles';
import LinearGradient from 'react-native-linear-gradient';

const GradientButton = ({title, onPress, style, fontStyle}) => {

	const navigation = useNavigation();
	const [code, setCode] = React.useState('');

	return (
        <TouchableOpacity style={[{
            
        }, style]}
		onPress={() => {
			if (onPress) {
				onPress();
			}
		}}>
			<LinearGradient
				colors={['#FFC700', '#FFB800']}
				start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
				style={[{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(46),
					borderRadius: Common.getLengthByIPhone7(8),
					alignItems: 'center',
					justifyContent: 'center',
				}, style]}
			>
				<Text style={[{
					color: '#111111',
					fontFamily: 'SFProDisplay-Bold',
					fontSize: Common.getLengthByIPhone7(14),
				}, fontStyle]}
				allowFontScaling={false}>
					{title}
				</Text>
			</LinearGradient>
		</TouchableOpacity>
	);
};

export default GradientButton;
