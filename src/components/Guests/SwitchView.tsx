import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, ScrollView, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';

const SwitchView = ({title, subtitle, onChange, style}) => {

	const navigation = useNavigation();
	const [value, setValue] = React.useState(0);

	useEffect(() => {
		if (onChange) {
			onChange(value);
		}
	}, [value]);

	return (
    <View style={[{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
		height: Common.getLengthByIPhone7(60),
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
	}, style]}>
		<View>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(18),
				color: 'black',
			}}>
				{title}
			</Text>
			{subtitle ? (<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(12),
				color: 'black',
				opacity: 0.6,
				marginTop: Common.getLengthByIPhone7(7),
			}}>
				{subtitle}
			</Text>) : null}
		</View>
		<View style={{
			flexDirection: 'row',
			alignItems: 'center',
		}}>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(28),
				height: Common.getLengthByIPhone7(28),
				borderRadius: Common.getLengthByIPhone7(14),
				borderColor: '#dddddd',
				borderWidth: 1,
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				if (value > 0) {
					let newValue = value;
					newValue--;
					setValue(newValue);
				}
			}}>
				<View
					style={{
						width: Common.getLengthByIPhone7(12),
						height: 2,
						backgroundColor: value === 0 ? '#dddddd' : '#717171',
					}}
				/>
			</TouchableOpacity>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(14),
				color: '#222222',
				width: Common.getLengthByIPhone7(40),
				textAlign: 'center',
			}}>
				{value}
			</Text>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(28),
				height: Common.getLengthByIPhone7(28),
				borderRadius: Common.getLengthByIPhone7(14),
				borderColor: '#dddddd',
				borderWidth: 1,
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				let newValue = value;
				newValue++;
				setValue(newValue);
			}}>
				<View
					style={{
						width: Common.getLengthByIPhone7(12),
						height: 2,
						backgroundColor: '#717171',
					}}
				/>
			</TouchableOpacity>
		</View>
    </View>
  );
};

const mstp = (state: RootState) => ({
	// categoryList: state.user.categoryList,
});

const mdtp = (dispatch: Dispatch) => ({
	// searchAddress: payload => dispatch.user.searchAddress(payload),
	// getAddressFromCoords: payload => dispatch.user.getAddressFromCoords(payload),
});

export default connect(mstp, mdtp)(SwitchView);