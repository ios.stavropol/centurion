import React, { useEffect } from 'react';
import {View, Animated, Easing, TouchableOpacity, Image} from 'react-native';
import { colors } from '../styles';
import Common from './../utilities/Common';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

const LoadingView = ({endLoading, isRequestGoing}) => {

	return (
		<Spinner
			visible={isRequestGoing}
			textContent={'Загрузка...'}
			overlayColor={'rgba(32, 42, 91, 0.3)'}
			textStyle={{color: '#FFF'}}
		/>
	);
};

const mstp = (state: RootState) => ({
	isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	endLoading: () => dispatch.user.endLoading(),
});

export default connect(mstp, mdtp)(LoadingView);