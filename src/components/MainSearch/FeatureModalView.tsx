import React, { useEffect } from 'react';
import {Image, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { colors, typography } from './../../styles';
import Modal from "react-native-modal";
import { APP_NAME } from './../../constants';
import ScaleView from './ScaleView';

const FeatureModalView = ({getAdvantages2, searchObj, show, onClose, onSelect}) => {

	const [body, setBody] = React.useState(null);
	const [showModal, setShowModal] = React.useState(false);
	const [selected, setSelected] = React.useState([]);
	const [cats, setCats] = React.useState([]);

	const renderView = (data, action) => {
		return (<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
			flexDirection: 'row',
			alignItems: 'flex-start',
			justifyContent: 'space-between',
			paddingTop: Common.getLengthByIPhone7(13),
			paddingBottom: Common.getLengthByIPhone7(13),
			paddingLeft: Common.getLengthByIPhone7(20),
			paddingRight: Common.getLengthByIPhone7(36),
		}}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(120),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					color: '#313131',
				}}>
					{data.name}
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(120),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					color: '#313131',
					opacity: 0.55,
					marginTop: 5,
				}}>
					{data.description}
				</Text>
			</View>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(25),
				height: Common.getLengthByIPhone7(25),
				borderRadius: Common.getLengthByIPhone7(6),
				borderColor: '#dddddd',
				borderWidth: 1,
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: selected.includes(data.id) ? '#222222' : 'white',
			}}
			activeOpacity={1}
			onPress={() => {
				if (action) {
					action();
				}
			}}>
				<Image
					source={require('./../../assets/ic-check.png')}
					style={{
						width: Common.getLengthByIPhone7(14),
						height: Common.getLengthByIPhone7(9),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
		</View>);
	}

	useEffect(() => {
		getAdvantages2(searchObj.mainCat)
		.then(res => {
			setCats(res);
		})
		.catch(err => {

		});
	}, []);

	useEffect(() => {
		let array = [];
		for (let i = 0; i < cats.length; i++) {
			array.push(renderView(cats[i], () => {
				let sel = JSON.parse(JSON.stringify(selected));
				if (sel.includes(cats[i].id)) {
					for (let z = 0; z < sel.length; z++) {
						if (sel[z] === cats[i].id) {
							sel.splice(z, 1);
							break;
						}
					}
				} else {
					sel.push(cats[i].id);
				}
				setSelected(sel);
			}));
		}
		setBody(array);
	}, [selected, cats]);

	useEffect(() => {
		setShowModal(show);
	}, [show]);

    return (
		<Modal
			isVisible={showModal}
			backdropColor={'black'}
			backdropOpacity={0.7}
			animationIn={'fadeIn'}
			animationOut={'fadeOut'}
			style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}
		>
			<View style={{
				borderRadius: Common.getLengthByIPhone7(10),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
				marginTop: Common.getLengthByIPhone7(90),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: 'white',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(50),
					alignItems: 'center',
					justifyContent: 'center',
					borderBottomColor: 'rgba(0, 0, 0, 0.5)',
					borderBottomWidth: 1,
				}}>
					<TouchableOpacity style={{
						position: 'absolute',
						left: Common.getLengthByIPhone7(10),
						width: Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(40),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						if (onClose) {
							onClose();
						}
					}}>
						<Image
							source={require('./../../assets/ic-close2.png')}
							style={{
								width: Common.getLengthByIPhone7(12),
								height: Common.getLengthByIPhone7(12),
								resizeMode: 'contain',
							}}
						/>
					</TouchableOpacity>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
					}}>
						Удобства
					</Text>
				</View>
				{body}
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(60),
					paddingLeft: Common.getLengthByIPhone7(14),
					paddingRight: Common.getLengthByIPhone7(14),
					alignItems: 'center',
					justifyContent: 'space-between',
					borderTopColor: 'rgba(0, 0, 0, 0.5)',
					borderTopWidth: 1,
					flexDirection: 'row',
				}}>
					<TouchableOpacity style={{
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						setSelected([]);
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							opacity: 0.5,
						}}>
							Очистить все
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						alignItems: 'center',
						justifyContent: 'center',
						width: Common.getLengthByIPhone7(202),
						height: Common.getLengthByIPhone7(36),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
					}}
					onPress={() => {
						if (onSelect) {
							onSelect(selected);
						}
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							color: 'white',
						}}>
							Показать больше 300 вариантов
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		</Modal>
	);
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	getAdvantages2: (payload) => dispatch.user.getAdvantages2(payload),
});

export default connect(mstp, mdtp)(FeatureModalView);