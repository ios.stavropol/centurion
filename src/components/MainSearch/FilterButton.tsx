import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const FilterButton = ({style, title, onClick}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
		<TouchableOpacity style={{
			height: Common.getLengthByIPhone7(32),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
			paddingLeft: Common.getLengthByIPhone7(10),
			paddingRight: Common.getLengthByIPhone7(10),
			borderRadius: Common.getLengthByIPhone7(100),
			borderColor: '#222222',
			borderWidth: 2,
		}}
		activeOpacity={1}
		onPress={() => {
			if (onClick) {
				onClick();
			}
		}}>
			<Text style={{
				color: '#222222',
				fontFamily: 'SFProRounded-Medium',
				fontSize: Common.getLengthByIPhone7(14),
			}}>
				{title}
			</Text>
			<Image
				source={require('./../../assets/ic-arrow-down.png')}
				style={{
					width: Common.getLengthByIPhone7(16),
					height: Common.getLengthByIPhone7(16),
					marginLeft: 4,
					resizeMode: 'contain',
				}}
			/>
		</TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(FilterButton);