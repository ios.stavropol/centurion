import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import FilterButton from './FilterButton';
import PriceModalView from './PriceModdalView';
import CategoryModalView from './CategoryModalView';
import FeatureModalView from './FeatureModalView';

const FilterView = ({style, searchObj, setSearchObj}) => {

	const navigation = useNavigation();
	const [showPrice, setShowPrice] = React.useState(false);
	const [showCategory, setShowCategory] = React.useState(false);
	const [showFeature, setShowFeature] = React.useState(false);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			// backgroundColor: 'red',
		}, style]}>
			<FilterButton
				title={'Цена'}
				onClick={() => {
					// setShowPrice(true);
				}}
			/>
			<FilterButton
				title={'Тип жилья'}
				onClick={() => {
					setShowCategory(true);
				}}
			/>
			<FilterButton
				title={'Удобства'}
				onClick={() => {
					setShowFeature(true);
				}}
			/>
			<PriceModalView
				show={showPrice}
				onClose={() => {
					setShowPrice(false);
				}}
			/>
			<CategoryModalView
				show={showCategory}
				onClose={() => {
					setShowCategory(false);
				}}
				onSelect={array => {
					let obj = JSON.parse(JSON.stringify(searchObj));
					let cats = [obj.mainCat];
					cats = cats.concat(array);
					obj.categories = cats;
					setSearchObj(obj);
					setShowCategory(false);
				}}
			/>
			<FeatureModalView
				show={showFeature}
				onClose={() => {
					setShowFeature(false);
				}}
				onSelect={array => {
					let obj = JSON.parse(JSON.stringify(searchObj));
					obj.features = array;
					setSearchObj(obj);
					setShowFeature(false);
				}}
			/>
		</View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
});

export default connect(mstp, mdtp)(FilterView);