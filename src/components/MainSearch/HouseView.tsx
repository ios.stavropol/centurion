import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from '../../constants';

const HouseView = ({style, data, onClick, setFavor}) => {

	const navigation = useNavigation();
	const [data2, setData2] = React.useState(null);

	useEffect(() => {
		setData2(data);
	}, data);

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			alignItems: 'center',
			justifyContent: 'center',
			borderTopLeftRadius: Common.getLengthByIPhone7(20),
			borderTopRightRadius: Common.getLengthByIPhone7(20),
			backgroundColor: 'white',
			marginBottom: Common.getLengthByIPhone7(30),
		}, style]}
		activeOpacity={1}
		onPress={() => {
			if (onClick) {
				onClick();
			}
		}}>
			<View style={{
				borderRadius: Common.getLengthByIPhone7(20),
				overflow: 'hidden',
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(210),
				backgroundColor: 'gray',
			}}>
				{data2?.cover?.photo ? (<Image
					source={{uri: BASE_URL + data2?.cover?.photo}}
					style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(210),
						resizeMode: 'cover',
					}}
				/>) : null}
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				marginTop: Common.getLengthByIPhone7(10),
				flexDirection: 'row',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					color: '#1A1E25',
				}}>
					{data2?.header}
				</Text>
				<TouchableOpacity style={{

				}}
				onPress={() => {
					setFavor(data2?.id)
					.then(() => {
						let d = JSON.parse(JSON.stringify(data2));
						d.is_favourite = !d.is_favourite;
						setData2(d);
					})
					.catch(err => {

					});
				}}>
					{data2?.is_favourite ? (<Image
						source={require('./../../assets/ic-like.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(18),
							resizeMode: 'contain',
						}}
					/>) : (<Image
						source={require('./../../assets/ic-unlike.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(18),
							resizeMode: 'contain',
						}}
					/>)}
				</TouchableOpacity>
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				marginTop: Common.getLengthByIPhone7(10),
				flexDirection: 'row',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Bold',
					fontSize: Common.getLengthByIPhone7(18),
					color: '#1A1E25',
				}}>
					&#8381; {data2?.price} <Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(12),
						color: '#7D7F88',
					}}>
						/ сутки
					</Text>
				</Text>
				<View style={{
					flexDirection: 'row',
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-bed.png')}
						style={{
							width: Common.getLengthByIPhone7(14),
							height: Common.getLengthByIPhone7(14),
							resizeMode: 'contain',
							marginRight: 6,
						}}
					/>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(13),
						color: '#7D7F88',
					}}>
						{Common.getDigitStr(data2?.rooms, {
							d1: ' комната',
							d2_d4: ' комнаты',
							d5_d10: ' комнат',
							d11_d19: ' комнат',
						})}
					</Text>
				</View>
			</View>
		</TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	setFavor: payload => dispatch.user.setFavor(payload),
});

export default connect(mstp, mdtp)(HouseView);