import React, { useEffect } from 'react';
import {View, Text} from 'react-native';
import Common from '../../utilities/Common';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from '../../styles';
import { typography } from '../../styles';
import { isIphoneX } from 'react-native-iphone-x-helper';

const months = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];

const NavBar = ({headerLeft, headerRight, searchObj}) => {

	const [title, setTitle] = React.useState('');

	useEffect(() => {
		let str = searchObj.city;
		if (searchObj.date_from) {
			let start = new Date(searchObj.date_from);
			let d1 = start.getDate();
			d1 = d1 < 10 ? '0' + d1 : d1;
			let m1 = start.getMonth();

			let end = new Date(searchObj.date_to);
			let d2 = end.getDate();
			d2 = d2 < 10 ? '0' + d2 : d2;
			let m2 = end.getMonth();

			if (m1 === m2) {
				str = str + ', ' + d1 + '-' + d2 + ' ' + months[m1];
			} else {
				str = str + ', ' + d1 + ' ' + months[m1] + '-' + d2 + ' ' + months[m2];
			}
		}
		setTitle(str);

	}, [searchObj]);

	return (
		<View style={{
			width: Common.getLengthByIPhone7(0),
			height: isIphoneX() ? 100 : 50,
			alignItems: 'flex-end',
			justifyContent: 'space-between',
			flexDirection: 'row',
			backgroundColor: 'white',
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'center',
				height: Common.getLengthByIPhone7(48),
			}}>
				<View style={{
					position: 'absolute',
					left: 0,
				}}>
					{headerLeft}
				</View>
				<View style={{
					height: Common.getLengthByIPhone7(48),
					maxWidth: Common.getLengthByIPhone7(250),
					borderRadius: Common.getLengthByIPhone7(26),
					backgroundColor: '#1f1f1f',
					alignItems: 'center',
					justifyContent: 'center',
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
				}}>
					<Text style={{
						color: 'white',
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
					}}
					allowFontScaling={false}>
						{title}
					</Text>
				</View>
				<View style={{
					position: 'absolute',
					right: 0,
				}}>
					{headerRight}
				</View>
			</View>
		</View>
	);
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(NavBar);