import React, { useEffect } from 'react';
import {Image, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { colors, typography } from './../../styles';
import Modal from "react-native-modal";
import { APP_NAME } from './../../constants';
import ScaleView from './ScaleView';

const PriceModalView = ({title, data, show, onClose, onSelect}) => {

	const [body, setBody] = React.useState(null);
	const [showModal, setShowModal] = React.useState(false);

	useEffect(() => {
		setShowModal(show);
	}, [show]);

    return (
		<Modal
			isVisible={showModal}
			backdropColor={'black'}
			backdropOpacity={0.7}
			animationIn={'fadeIn'}
			animationOut={'fadeOut'}
			style={{
				flex: 1,
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}
		>
			<View style={{
				borderRadius: Common.getLengthByIPhone7(10),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
				marginTop: Common.getLengthByIPhone7(90),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: 'white',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(50),
					alignItems: 'center',
					justifyContent: 'center',
					borderBottomColor: 'rgba(0, 0, 0, 0.5)',
					borderBottomWidth: 1,
				}}>
					<TouchableOpacity style={{
						position: 'absolute',
						left: Common.getLengthByIPhone7(10),
						width: Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(40),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						if (onClose) {
							onClose();
						}
					}}>
						<Image
							source={require('./../../assets/ic-close2.png')}
							style={{
								width: Common.getLengthByIPhone7(12),
								height: Common.getLengthByIPhone7(12),
								resizeMode: 'contain',
							}}
						/>
					</TouchableOpacity>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
					}}>
						Ценовой диапозон
					</Text>
				</View>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(200),
					padding: Common.getLengthByIPhone7(20),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
					}}>
						990₽ - 100 000₽ +
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						opacity: 0.5,
						marginTop: 5,
					}}>
						Средняя цена за ночь: 13 728₽
					</Text>
					<ScaleView/>
				</View>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
					height: Common.getLengthByIPhone7(60),
					paddingLeft: Common.getLengthByIPhone7(14),
					paddingRight: Common.getLengthByIPhone7(14),
					alignItems: 'center',
					justifyContent: 'space-between',
					borderTopColor: 'rgba(0, 0, 0, 0.5)',
					borderTopWidth: 1,
					flexDirection: 'row',
				}}>
					<TouchableOpacity style={{
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						if (onClose) {
							onClose();
						}
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							opacity: 0.5,
						}}>
							Очистить все
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						alignItems: 'center',
						justifyContent: 'center',
						width: Common.getLengthByIPhone7(202),
						height: Common.getLengthByIPhone7(36),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
					}}
					onPress={() => {
						if (onClose) {
							onClose();
						}
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							color: 'white',
						}}>
							Показать больше 300 вариантов
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		</Modal>
	);
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	logout: () => dispatch.user.logout(),
});

export default connect(mstp, mdtp)(PriceModalView);