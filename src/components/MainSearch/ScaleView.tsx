import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, Animated, Easing, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { typography, colors } from '../../styles';
import { useNavigation } from '@react-navigation/native';
import {map} from './../../constants/map';
import SliderText from './Slider';

const ScaleView = ({last, index, onChange}) => {

	const [sliderValue, setSliderValue] = React.useState(0);

	useEffect(() => {
		if (onChange) {
			onChange(sliderValue);
		}
	}, [sliderValue]);

	return (
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(64),
			height: Common.getLengthByIPhone7(117),
			borderRadius: Common.getLengthByIPhone7(16),
			marginTop: Common.getLengthByIPhone7(32),
			marginBottom: last ? Common.getLengthByIPhone7(32) : 0,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}
		key={index}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(112),
				marginTop: Common.getLengthByIPhone7(24),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				
				<Text style={[typography.H4_R16_140, {
					textAlign: 'left',
				}]}
				allowFontScaling={false}>
					{sliderValue}/10
				</Text>
			</View>
        </View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(ScaleView);
