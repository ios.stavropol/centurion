import React, { useEffect } from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from '../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../../store';
import { connect, useDispatch, useSelector } from 'react-redux';

const SettingButton = ({navigation, style}) => {

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(32),
			height: Common.getLengthByIPhone7(32),
			alignItems: 'flex-end',
			justifyContent: 'center',
		}, style]}
		onPress={() => {
			
		}}>
			<Image
				source={require('./../../assets/ic-setting.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(15),
					height: Common.getLengthByIPhone7(17),
					// tintColor: 'black',
				}}
			/>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	setBackFun: payload => dispatch.buttons.setBackFun(payload),
});

export default connect(mstp, mdtp)(SettingButton);