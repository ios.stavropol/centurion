import React, { useState, useEffect } from 'react';
import { View, Text, Platform, StyleSheet } from 'react-native';
import Slider from '@react-native-community/slider';
import Common from '../../utilities/Common';
import LinearGradient from 'react-native-linear-gradient';
import { colors } from '../../styles';

const width = Common.getLengthByIPhone7(280);

const SliderText = (props) => {
  const multiplier = props.multiplier || 1.15;
  const maximumValue = props.maximumValue || 1;
  const stepValue = props.stepValue || 1;
  const value = props.value || 0;
  const logic = maximumValue * multiplier;
  const [ sliderValue, setSliderValue ] = useState(props.sliderValue || 0);

  const left = sliderValue >= 100000000 ? sliderValue * width / logic - 40 : sliderValue * width / logic;

  const sendSliderValue = (slider) => {
    setSliderValue(slider);
    props.onValueChange(slider);
  };

  return (
    <View style={[styles.slider, props.containerStyle]}>
		<LinearGradient
			colors={['#83DC11', '#FF3D00']}
			start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
			style={{
				width: width - 20,
				height: Common.getLengthByIPhone7(5),
				borderRadius: Common.getLengthByIPhone7(24),
				alignItems: 'center',
				justifyContent: 'center',
				position: 'absolute',
				left: 10,
				bottom: Platform.OS === 'ios' ? Common.getLengthByIPhone7(52) : Common.getLengthByIPhone7(43),
			}}
		/>
      <Slider
        style={[styles.slider, props.sliderStyle]}
        minimumValue={props.minimumValue || 0}
        maximumValue={maximumValue}
        step={stepValue}
        minimumTrackTintColor={'transparent'}
        thumbTintColor={props.thumbTintColor || 'red'}
        maximumTrackTintColor={'transparent'}
        onValueChange={(e) => sendSliderValue(e)}
        onSlidingComplete={props.onSlidingComplete}
        value={value}
      />
      <View
        style={{
          height: Common.getLengthByIPhone7(36),
          width: Common.getLengthByIPhone7(30),
          borderRadius: Common.getLengthByIPhone7(16),
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
          transform: [{translateX: left}],
          marginTop: -20,
          shadowColor: "#14458D",
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.10,
          shadowRadius: 10.00,
          elevation: 1,
        }}
      >
        <Text allowFontScaling={false} style={[ styles.text, props.customCountStyle ]}>{Math.floor(sliderValue)}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000'
  },
  slider: {
    width: width,
    marginVertical: 20,
    alignSelf: 'center'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  customLabel: {
    fontSize: 20
  }
});

export default SliderText;
