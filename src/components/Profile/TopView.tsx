import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';

const TopView = ({style, onClick, userProfile}) => {

	const navigation = useNavigation();
	const [name, setName] = React.useState('Ваш профиль');

	useEffect(() => {
		if (userProfile?.first_name?.length) {
			setName(userProfile?.first_name + ' ' + userProfile?.last_name);
		} else {
			setName('Ваш профиль');
		}
	}, [userProfile]);

	return (
		<View style={{
			zIndex: 100,
			backgroundColor: 'white',
			shadowColor: "black",
			shadowOffset: {
				width: 0,
				height: 2,
			},
			shadowOpacity: 0.25,
			shadowRadius: 5.00,
			elevation: 1,
			borderBottomLeftRadius: Common.getLengthByIPhone7(50),
			borderBottomRightRadius: Common.getLengthByIPhone7(50),
		}}>
			<LinearGradient
				colors={['#FFB459', '#FFC453']}
				start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
				style={{
					alignItems: 'center',
					justifyContent: 'center',
					width: Common.getLengthByIPhone7(0),
					borderBottomLeftRadius: Common.getLengthByIPhone7(50),
					borderBottomRightRadius: Common.getLengthByIPhone7(50),
					paddingBottom: Common.getLengthByIPhone7(46),
					paddingTop: Common.getLengthByIPhone7(80),
				}}
			>
				<View style={{
					width: Common.getLengthByIPhone7(108),
					height: Common.getLengthByIPhone7(108),
					borderRadius: Common.getLengthByIPhone7(108) / 2,
					overflow: 'hidden',
				}}>
					<Image
						source={require('./../../assets/ic-anonimus.png')}
						style={{
							width: Common.getLengthByIPhone7(108),
							height: Common.getLengthByIPhone7(108),
							resizeMode: 'contain',
						}}
					/>
				</View>
				<Text style={{
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(20),
					color: '#222222',
					marginTop: Common.getLengthByIPhone7(15),
					marginBottom: Common.getLengthByIPhone7(12),
				}}
				allowFontScaling={false}>
					{name}
				</Text>
				<TouchableOpacity style={{
					flexDirection: 'row',
					alignItems: 'center',
				}}
				onPress={() => {
					if (onClick) {
						onClick();
					}
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						color: '#222222',
						textDecorationLine: 'underline',
					}}
					allowFontScaling={false}>
						Редактировать информацию
					</Text>
					<Image
						source={require('./../../assets/ic-pen.png')}
						style={{
							width: Common.getLengthByIPhone7(13),
							height: Common.getLengthByIPhone7(13),
							resizeMode: 'contain',
							marginLeft: Common.getLengthByIPhone7(3),
						}}
					/>
				</TouchableOpacity>
			</LinearGradient>
		</View>
  );
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(TopView);