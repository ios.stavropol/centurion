import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const VerifyView = ({style, title, icon, onClick}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(80),
			borderRadius: Common.getLengthByIPhone7(10),
			backgroundColor: '#F4F4F4',
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}
		onPress={() => {
			if (onClick) {
				onClick();
			}
		}}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
			}}>
				<Image
					source={icon}
					style={{
						width: Common.getLengthByIPhone7(25),
						height: Common.getLengthByIPhone7(25),
						marginLeft: Common.getLengthByIPhone7(20),
						resizeMode: 'contain',
					}}
				/>
				<View style={{
					marginLeft: Common.getLengthByIPhone7(15),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'black',
					}}
					allowFontScaling={false}>
						{title}
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
						marginTop: Common.getLengthByIPhone7(8),
						color: 'black',
						textDecorationLine: 'underline',
					}}
					allowFontScaling={false}>
						Подробнее
					</Text>
				</View>
			</View>
			<Image
				source={require('./../../assets/ic-arrow-right.png')}
				style={{
					width: Common.getLengthByIPhone7(7),
					height: Common.getLengthByIPhone7(14),
					marginRight: Common.getLengthByIPhone7(15),
					resizeMode: 'contain',
				}}
			/>
		</TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(VerifyView);