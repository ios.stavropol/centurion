import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from './../../constants';

const TagView = ({data, onSelect}) => {

	const navigation = useNavigation();
	const [selected, setSelected] = React.useState(false);

	useEffect(() => {
		if (onSelect) {
			onSelect(selected)
		}
	}, [selected]);

	return (
        <TouchableOpacity style={{
			height: Common.getLengthByIPhone7(46),
			borderRadius: Common.getLengthByIPhone7(22),
			paddingLeft: Common.getLengthByIPhone7(12),
			paddingRight: Common.getLengthByIPhone7(12),
			borderColor: '#b0b0b0',
			borderWidth: 1,
			backgroundColor: selected ? 'black' : 'white',
			marginRight: Common.getLengthByIPhone7(9),
			marginBottom: Common.getLengthByIPhone7(11),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
		}}
		onPress={() => {
			setSelected(!selected);
		}}>
			<Image
				source={{uri: BASE_URL + data.icon}}
				style={{
					height: Common.getLengthByIPhone7(20),
					width: Common.getLengthByIPhone7(20),
					resizeMode: 'contain',
					marginRight: Common.getLengthByIPhone7(9),
					tintColor: selected ? 'white' : 'black',
				}}
			/>
			<Text style={{
				fontFamily: 'SFProDisplay-Medium',
				fontSize: Common.getLengthByIPhone7(12),
				color: selected ? 'white' : 'black',
				opacity: 0.9,
			}}>
				{data.name}
			</Text>
		</TouchableOpacity>
	);
};

export default TagView;