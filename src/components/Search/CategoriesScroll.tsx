import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import CategoryView from './CategoryView';

const CategoriesScroll = ({style}) => {

	const navigation = useNavigation();
	const scroll = useRef(null);

	return (
        <ScrollView style={[{
            width: Common.getLengthByIPhone7(0),
			height: Common.getLengthByIPhone7(340),
        }, style]}
		ref={scroll}
		horizontal={true}
		pagingEnabled={true}
		showsHorizontalScrollIndicator={false}
		contentContainerStyle={{
			alignItems: 'center',
			justifyContent: 'center',
		}}>
			<CategoryView
				title={'Жилье'}
				image={require('./../../assets/ic-homes.png')}
				first
				onClick={() => {
					navigation.navigate('SelectCategory');
				}}
				onRight={() => {
					scroll.current.scrollTo({
						x: Common.getLengthByIPhone7(0),
						animated: true,
					});
				}}
				style={{
					// marginLeft: Common.getLengthByIPhone7(20),
				}}
			/>
			<CategoryView
				title={'Авто'}
				image={require('./../../assets/ic-avto.png')}
				onClick={() => {
					navigation.navigate('SelectCategory');
				}}
				onLeft={() => {
					scroll.current.scrollTo({
						x: 0,
						animated: true,
					});
				}}
				onRight={() => {
					scroll.current.scrollTo({
						x: 2*Common.getLengthByIPhone7(0),
						animated: true,
					});
				}}
				style={{
					// marginLeft: Common.getLengthByIPhone7(20),
				}}
			/>
			<CategoryView
				title={'Яхты'}
				image={require('./../../assets/ic-yaht.png')}
				onClick={() => {
					navigation.navigate('SelectCategory');
				}}
				onLeft={() => {
					scroll.current.scrollTo({
						x: Common.getLengthByIPhone7(0),
						animated: true,
					});
				}}
				onRight={() => {
					scroll.current.scrollTo({
						x: 3*Common.getLengthByIPhone7(0),
						animated: true,
					});
				}}
				style={{
					// marginLeft: Common.getLengthByIPhone7(20),
				}}
			/>
			<CategoryView
				title={'Самолеты'}
				image={require('./../../assets/ic-avia.png')}
				last
				onClick={() => {
					navigation.navigate('SelectCategory');
				}}
				onLeft={() => {
					scroll.current.scrollTo({
						x: 2*Common.getLengthByIPhone7(0),
						animated: true,
					});
				}}
				style={{
					// marginLeft: Common.getLengthByIPhone7(20),
					// marginRight: Common.getLengthByIPhone7(20),
				}}
			/>
        </ScrollView>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(CategoriesScroll);