import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const CategoryView = ({style, title, image, onClick, onLeft, onRight, first, last}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
		<View style={{
			width: Common.getLengthByIPhone7(0),
			alignItems: 'center',
			justifyContent: 'center',
		}}>
			<ImageBackground source={image} style={[{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(340),
				borderRadius: Common.getLengthByIPhone7(15),
				backgroundColor: 'white',
				alignItems: 'center',
				justifyContent: 'center',
			}, style]}>
				{!first ? (<TouchableOpacity style={{
					position: 'absolute',
					left: Common.getLengthByIPhone7(10),
				}}
				onPress={() => {
					if (onLeft) {
						onLeft();
					}
				}}>
					<Image source={require('./../../assets/ic-circle-left.png')}
						style={{
							width: Common.getLengthByIPhone7(33),
							height: Common.getLengthByIPhone7(33),
							resizeMode: 'contain',
						}}
					/>
				</TouchableOpacity>) : null}
				{!last ? (<TouchableOpacity style={{
					position: 'absolute',
					right: Common.getLengthByIPhone7(10),
				}}
				onPress={() => {
					if (onRight) {
						onRight();
					}
				}}>
					<Image source={require('./../../assets/ic-circle-right.png')}
						style={{
							width: Common.getLengthByIPhone7(33),
							height: Common.getLengthByIPhone7(33),
							resizeMode: 'contain',
						}}
					/>
				</TouchableOpacity>) : null}
				<View style={{
					position: 'absolute',
					bottom: Common.getLengthByIPhone7(50),
					alignItems: 'center',
				}}>
					<Text style={{
						color: '#FFF667',
						fontFamily: 'SFProRounded-Regular',
						fontSize: Common.getLengthByIPhone7(30),
						marginBottom: Common.getLengthByIPhone7(17),
					}}>
						{title}
					</Text>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(120),
						height: Common.getLengthByIPhone7(38),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'white',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						if (onClick) {
							onClick();
						}
					}}>
						<Text style={{
							fontSize: Common.getLengthByIPhone7(14),
							color: 'black',
						}}>
							Забронировать
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
		</View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(CategoryView);