import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const Centurion1View = ({style, onClick}) => {

	const navigation = useNavigation();

	return (
		<ImageBackground source={require('./../../assets/ic-centurion1.png')} style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(340),
			overflow: 'hidden',
			borderRadius: Common.getLengthByIPhone7(15),
			backgroundColor: 'white',
			alignItems: 'flex-start',
			justifyContent: 'center',
			paddingLeft: Common.getLengthByIPhone7(20),
		}, style]}>
			<Text style={{
				width: Common.getLengthByIPhone7(250),
				color: 'white',
				fontFamily: 'SFProRounded-Regular',
				fontSize: Common.getLengthByIPhone7(26),
				marginBottom: Common.getLengthByIPhone7(22),
			}}>
				Личное обращение к вам от основателя Centurion
			</Text>
			<TouchableOpacity style={{
				alignItems: 'center',
				justifyContent: 'center',
				width: Common.getLengthByIPhone7(120),
				height: Common.getLengthByIPhone7(38),
				borderRadius: Common.getLengthByIPhone7(8),
				backgroundColor: 'white',
			}}
			onPress={() => {
				if (onClick) {
					onClick();
				}
			}}>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
					color: 'black',
				}}
				allowFontScaling={false}>
					Познакомиться
				</Text>
			</TouchableOpacity>
		</ImageBackground>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(Centurion1View);