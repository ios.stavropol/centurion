import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import MapView from 'react-native-maps';

const MapCircleView = ({style}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
        <View style={[{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
			height: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            // backgroundColor: 'black',
            alignItems: 'center',
			justifyContent: 'center',
			position: 'absolute',
			top: Common.getLengthByIPhone7(140),
        }, style]}>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
				height: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
				borderRadius: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50)) / 2,
				// overflow: 'hidden',
				shadowColor: "black",
				shadowOffset: {
					width: 3,
					height: 4,
				},
				shadowOpacity: 0.26,
				shadowRadius: 25.00,
				elevation: 1,
				zIndex: -1000,
			}}
			zIndex={-1000}>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
					height: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
					borderRadius: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50)) / 2,
					overflow: 'hidden',
					borderColor: 'black',
					borderWidth: 4,
				}}>
					<MapView
						initialRegion={{
							latitude: 37.78825,
							longitude: -122.4324,
							latitudeDelta: 0.0922,
							longitudeDelta: 0.0421,
						}}
						scrollEnabled = {false}
						showsUserLocation={true}
						followsUserLocation={true}
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
							height: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(50),
						}}
						urlTemplate={'http://c.tile.openstreetmap.org/{z}/{x}/{y}.png'}
					/>
				</View>
			</View>
			<Text style={{
				color: '#EE544A',
				fontFamily: 'SFProRounded-Medium',
				fontSize: Common.getLengthByIPhone7(20),
				position: 'absolute',
				left: 0,
			}}>
				N
			</Text>
			<Text style={{
				color: '#EE544A',
				fontFamily: 'SFProRounded-Medium',
				fontSize: Common.getLengthByIPhone7(20),
				position: 'absolute',
				top: 0,
			}}>
				S
			</Text>
			<Text style={{
				color: '#EE544A',
				fontFamily: 'SFProRounded-Medium',
				fontSize: Common.getLengthByIPhone7(20),
				position: 'absolute',
				right: 0,
			}}>
				W
			</Text>
			<Text style={{
				color: '#EE544A',
				fontFamily: 'SFProRounded-Medium',
				fontSize: Common.getLengthByIPhone7(20),
				position: 'absolute',
				bottom: 0,
			}}>
				E
			</Text>
			<View
				style={{
					position: 'absolute',
					left: 0,
					top: 0,
					right: 0,
					bottom: 0,
					// zIndex: 100,
					// backgroundColor: 'red',
					// opacity: 0.3,
				}}
			/>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(MapCircleView);