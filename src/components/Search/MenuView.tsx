import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const MenuView = ({style}) => {

	const navigation = useNavigation();
	const scroll = useRef(null);

	const renderMenu = (title, style, onPress) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(290),
			height: Common.getLengthByIPhone7(70),
			borderBottomColor: 'rgba(0, 0, 0, 0.1)',
			borderBottomWidth: 1,
			justifyContent: 'center',
		}, style]}>
			<Text
				style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}
			>
				{title}
			</Text>
		</TouchableOpacity>);
	}

	return (
        <ScrollView style={[{
            width: Common.getLengthByIPhone7(0),
			height: Common.getLengthByIPhone7(370),
        }, style]}
		ref={scroll}
		horizontal={true}
		pagingEnabled={true}
		showsHorizontalScrollIndicator={false}
		contentContainerStyle={{
			alignItems: 'flex-start',
			justifyContent: 'center',
		}}>
			<View style={{
				marginLeft: Common.getLengthByIPhone7(20),
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(290),
					height: Common.getLengthByIPhone7(70),
					borderBottomColor: 'rgba(0, 0, 0, 0.1)',
					borderBottomWidth: 1,
					justifyContent: 'center',
				}}>
					<Text
						style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}
					>
						Поддержка
					</Text>
				</View>
				{renderMenu('Центр поддержки', {}, () => {

				})}
				{renderMenu('Сообщить о проблеме на сервисе', {}, () => {
					
				})}
				{renderMenu('Правила сообщества', {}, () => {
					
				})}
				{renderMenu('Советы и инструкции по безопасности', {
					borderBottomWidth: 0,
				}, () => {
					
				})}
			</View>
			<View style={{
				marginLeft: Common.getLengthByIPhone7(10),
				marginRight: Common.getLengthByIPhone7(20),
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(290),
					height: Common.getLengthByIPhone7(70),
					borderBottomColor: 'rgba(0, 0, 0, 0.1)',
					borderBottomWidth: 1,
					justifyContent: 'center',
				}}>
					<Text
						style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}
					>
						Сообщество
					</Text>
				</View>
				{renderMenu('Путешествия с командой Centurion', {}, () => {

				})}
				{renderMenu('Наши социальные сети', {}, () => {
					
				})}
			</View>
        </ScrollView>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(MenuView);