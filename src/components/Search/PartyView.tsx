import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';

const PartyView = ({style, onClick}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
		<ImageBackground source={require('./../../assets/ic-party.png')} style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(583),
			overflow: 'hidden',
			borderRadius: Common.getLengthByIPhone7(15),
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'center',
		}, style]}>
			<Text style={{
				color: '#F66F63',
				fontFamily: 'SFProRounded-Bold',
				fontSize: Common.getLengthByIPhone7(40),
				marginBottom: Common.getLengthByIPhone7(37),
			}}>
				X<Text style={{
				color: 'white',
				fontFamily: 'SFProRounded-Regular',
				fontSize: Common.getLengthByIPhone7(30),
			}}>
				-тусовки
			</Text>
			</Text>
			<TouchableOpacity style={{
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				if (onClick) {
					onClick();
				}
			}}>
				<LinearGradient
					colors={['#FFC453', '#FFB459']}
					start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
					style={{
						width: Common.getLengthByIPhone7(136),
						height: Common.getLengthByIPhone7(44),
						borderRadius: Common.getLengthByIPhone7(8),
						alignItems: 'center',
						justifyContent: 'center',
					}}
				>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'black',
					}}
					allowFontScaling={false}>
						Подробнее
					</Text>
				</LinearGradient>
			</TouchableOpacity>
		</ImageBackground>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(PartyView);