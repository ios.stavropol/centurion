import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const SearchView = ({onPress}) => {

	const navigation = useNavigation();

	return (
        <TouchableOpacity style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(48),
			borderRadius: Common.getLengthByIPhone7(300),
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'center',
			marginTop: Common.getLengthByIPhone7(50),
        }}
		activeOpacity={1}
		onPress={() => {
			if (onPress) {
				onPress();
			}
		}}>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(14),
			}}>
				Куда едете?
			</Text>
			<View style={{
				position: 'absolute',
				right: Common.getLengthByIPhone7(6),
			}}>
				<Image source={require('./../../assets/ic-search.png')}
					style={{
						width: Common.getLengthByIPhone7(37),
						height: Common.getLengthByIPhone7(37),
						resizeMode: 'contain',
					}}
				/>
			</View>
        </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(SearchView);