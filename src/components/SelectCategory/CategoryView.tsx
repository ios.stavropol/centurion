import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const CategoryView = ({style, title, subTitle, image, onClick}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(70),
			borderRadius: Common.getLengthByIPhone7(9),
			borderColor: 'rgba(0, 0, 0, 0.3)',
			borderWidth: 1,
			alignItems: 'center',
			justifyContent: 'space-between',
			flexDirection: 'row',
			paddingLeft: Common.getLengthByIPhone7(20),
			paddingRight: Common.getLengthByIPhone7(10),
		}, style]}
		onPress={() => {
			if (onClick) {
				onClick();
			}
		}}>
			<View style={{
				alignItems: 'flex-start',
			}}>
				<Text style={{
					color: '#343434',
					fontFamily: 'SFProRounded-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					marginBottom: Common.getLengthByIPhone7(7),
				}}>
					{title}
				</Text>
				<Text style={{
					fontSize: Common.getLengthByIPhone7(12),
					color: '#343434',
					opacity: 0.61,
				}}>
					{subTitle}
				</Text>
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(50),
				height: Common.getLengthByIPhone7(50),
				borderRadius: Common.getLengthByIPhone7(7),
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				<Image source={image} style={{
					width: Common.getLengthByIPhone7(50),
					height: Common.getLengthByIPhone7(50),
					borderRadius: Common.getLengthByIPhone7(7),
					resizeMode: 'contain',
				}}/>
			</View>
		</TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(CategoryView);