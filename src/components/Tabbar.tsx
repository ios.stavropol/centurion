import React, { useCallback, useEffect, useRef } from 'react';
import { Image, ImageBackground, View, TouchableOpacity, TextInput, Linking, Text, Alert, Dimensions } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Common from './../utilities/Common';
import { colors } from './../../styles';
import { APP_NAME } from './../constants';
import { typography, shadows } from './../styles';

const images = [require('./../assets/ic-tab1.png'), require('./../assets/ic-tab2.png'), require('./../assets/ic-tab3.png'), require('./../assets/ic-tab4.png'), require('./../assets/ic-tab5.png')];
const labels = ['Поиск', 'Избранное', 'Поездки', 'Чаты', 'Профиль'];

const Tabbar = ({state, descriptors, navigation }) => {

	const [code, setCode] = React.useState('');

	return (
    <View style={{
      flexDirection: 'row',
      backgroundColor: 'transparent',
      backgroundColor: 'rgba(31, 31, 31, 0.75)',
      height: 95,
      width: Dimensions.get('screen').width,
      justifyContent: 'center',
      position: 'absolute',
      bottom: 0,
      left: 0,
    }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              // borderWidth: 1,
              // borderColor: 'red',
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Image source={images[index]}
              style={{
                width: 30,
                height: 30,
                resizeMode: 'contain',
                // tintColor: isFocused ? '#5669FF' : 'rgba(18, 13, 38, 0.3)',
              }}
            />
			<Text style={{
				color: 'white',
				fontFamily: 'SFProRounded-Light',
				fontSize: 10,
				marginTop: 4,
				marginBottom: 15,
			}}>
			  {labels[index]}
			</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
	// userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
    // sendCode: payload => dispatch.user.sendCode(payload),
	// getProfile: () => dispatch.user.getProfile(),
});

export default connect(mstp, mdtp)(Tabbar);