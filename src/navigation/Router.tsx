import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { Image, Platform } from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { colors } from '../styles';
import { typography } from '../styles';
import SplashScreen from '../screens/Splash';
import LoginScreen from '../screens/Login';
import CodeScreen from '../screens/Code';
import NotifyScreen from '../screens/Notify';
import IntroScreen from '../screens/Intro';
import SearchScreen from '../screens/Search';
import SelectCategoryScreen from '../screens/SelectCategory';
import FavorScreen from '../screens/Favor';
import TripsScreen from '../screens/Trips';
import ChatsScreen from '../screens/Chats';
import ChatScreen from '../screens/Chat';
import ProfileScreen from '../screens/Profile';
import EditProfileScreen from '../screens/EditProfile';
import Create1Screen from '../screens/Create1';
import Create2Screen from '../screens/Create2';
import Create3Screen from '../screens/Create3';
import Create4Screen from '../screens/Create4';
import Create5Screen from '../screens/Create5';
import SearchAddressScreen from '../screens/SearchAddress';
import ConfirmAddressScreen from '../screens/ConfirmAddress';
import GuestsScreen from '../screens/Guests';
import AdvantagesScreen from '../screens/Advantages';
import AddPhotoScreen from '../screens/AddPhoto';
import SetNameScreen from '../screens/SetName';
import SchtickScreen from '../screens/Schtick';
import AboutScreen from '../screens/About';
import PriceScreen from '../screens/Price';
import AdvEndScreen from '../screens/AdvEnd';
import AdvSuccessScreen from '../screens/AdvSuccess';
import SelectCityScreen from '../screens/SelectCity';
import SelectDatesScreen from '../screens/SelectDates';
import SelectPeopleScreen from '../screens/SelectPeople';
import MainSearchScreen from '../screens/MainSearch';
import AppartamentScreen from '../screens/Appartament';
import BuyInfoScreen from '../screens/BuyInfo';
import BuyScreen from '../screens/Buy';

import Tabbar from '../components/Tabbar';
import common from './../utilities/Common';
import NavBar from '../components/MainSearch/NavBar';

import BackButton from '../components/BackButton';
import SettingButton from '../components/MainSearch/SettingButton';

export const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const EnterStack = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen
            name={'Splash2'}
            component={SplashScreen}
            options={{
                headerShown: false,
            }}
          />
          <Stack.Screen
            name={'Login'}
            component={LoginScreen}
            options={{
              headerShown: false,
              animation: 'fade',
              presentation: 'card',
              gestureEnabled: false,
              headerBackVisible: false,
            }}
          />
          <Stack.Screen
            name={'Code'}
            component={CodeScreen}
            options={{
              headerShown: false,
              animation: 'slide_from_bottom',
              presentation: 'card',
              gestureEnabled: false,
              headerBackVisible: false,
            }}
          />
          <Stack.Screen
            name={'Notify'}
            component={NotifyScreen}
            options={{
              headerShown: false,
              animation: 'fade',
              presentation: 'card',
              gestureEnabled: false,
              headerBackVisible: false,
            }}
          />
          <Stack.Screen
            name={'Intro'}
            component={IntroScreen}
            options={{
              headerShown: false,
              gestureEnabled: false,
              headerBackVisible: false,
            }}
          />
      </Stack.Navigator>
  );
}

const SearchTab = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen name={'Search'} component={SearchScreen} options={{
          headerShown: false,
          gestureEnabled: false,
        }}/>
        <Stack.Screen name={'SelectCity'} component={SelectCityScreen} options={{
          headerShown: false,
          gestureEnabled: true,
        }}/>
        <Stack.Screen name={'SelectCategory'} component={SelectCategoryScreen} options={{
          headerShown: false,
          gestureEnabled: true,
        }}/>
        <Stack.Screen name={'SelectDates'} component={SelectDatesScreen} options={{
          headerShown: false,
          gestureEnabled: true,
        }}/>
        <Stack.Screen name={'SelectPeople'} component={SelectPeopleScreen} options={{
          headerShown: false,
          gestureEnabled: true,
        }}/>
        <Stack.Screen name={'MainSearch'} component={MainSearchScreen} options={{
          headerShown: true,
          gestureEnabled: true,
          headerBackVisible: false,
          headerShadowVisible: false,
          header: (props) => (
            <NavBar
              {...props}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(20),
              }}/>}
              headerRight={ <SettingButton {...props} style={{
                marginRight: common.getLengthByIPhone7(20),
              }}/>}
            />
          ),
        }}/>
        <Stack.Screen name={'Appartament'} component={AppartamentScreen} options={{
          headerShown: false,
          gestureEnabled: true,
        }}/>
        <Stack.Screen name={'BuyInfo'} component={BuyInfoScreen} options={{
          title: '',
          headerShadowVisible: false,
          gestureEnabled: true,
          headerBackTitleVisible: false,
          headerTintColor: 'black',
        }}/>
        <Stack.Screen name={'Buy'} component={BuyScreen} options={{
          title: 'Оплата',
          gestureEnabled: true,
          headerBackTitleVisible: false,
          headerTintColor: 'black',
        }}/>
      </Stack.Navigator>
  );
}

const FavorTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Favor'} component={FavorScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Chat2'} component={ChatScreen} options={{
            headerShown: false,
            gestureEnabled: true,
          }}/>
      </Stack.Navigator>
  );
}

const TripsTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Trips'} component={TripsScreen} options={{
            headerTransparent: false,
            gestureEnabled: false,
            // header: (props) => <MainHeader {...props} type={'small'} letter={'К'} title={'алендарь'} />,
            // headerRight: (props) => (
            //   <MapList
            //     {...props}
            //   />
            // ),
              // animation: 'slide_from_bottom',
              // presentation: 'card',
          }}/>
      </Stack.Navigator>
  );
}

const ChatTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Chats'} component={ChatsScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Chat'} component={ChatScreen} options={{
            headerShown: false,
            gestureEnabled: true,
          }}/>
      </Stack.Navigator>
  );
}

const ProfileTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Profile'} component={ProfileScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'EditProfile'} component={EditProfileScreen} options={{
            title: '',
            headerShadowVisible: false,
            gestureEnabled: true,
            headerBackTitleVisible: false,
            headerTintColor: 'black',
          }}/>
      </Stack.Navigator>
  );
}

const CreateTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Create1'} component={Create1Screen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Create2'} component={Create2Screen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Create3'} component={Create3Screen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Create4'} component={Create4Screen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Create5'} component={Create5Screen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'SearchAddress'} component={SearchAddressScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'ConfirmAddress'} component={ConfirmAddressScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Guests'} component={GuestsScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Advantages'} component={AdvantagesScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'AddPhoto'} component={AddPhotoScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'SetName'} component={SetNameScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Schtick'} component={SchtickScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'About'} component={AboutScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'Price'} component={PriceScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'AdvEnd'} component={AdvEndScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'AdvSuccess'} component={AdvSuccessScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
      </Stack.Navigator>
  );
}

const Tabs = () => {
  return (
      <Tab.Navigator 
        tabBar={props => <Tabbar {...props} />} 
        screenOptions={({ route }) => ({
          tabBarStyle: {
            backgroundColor: colors.TABBAR_COLOR,
            borderTopWidth: 0,
            shadowColor: '#CED6E8',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 1,
            shadowRadius: 10,
            elevation: 4,
          },
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'SearchTab') {
              iconName = require('./../assets/ic-tab1.png');
            } else if (route.name === 'FavorTab') {
              iconName = require('./../assets/ic-tab2.png');
            } else if (route.name === 'TripsTab') {
              iconName = require('./../assets/ic-tab3.png');
            } else if (route.name === 'ChatTab') {
              iconName = require('./../assets/ic-tab4.png');
            } else if (route.name === 'ProfileTab') {
              iconName = require('./../assets/ic-tab5.png');
            }

            // You can return any component that you like here!
            return (<Image
              source={iconName}
              style={{
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? -10 : 0) : 0,
                resizeMode: 'contain',
                width: 32, 
                height: 32,
                tintColor: color,
              }}
            />);
          },
          tabBarActiveTintColor: colors.ORANGE_COLOR,
          tabBarInactiveTintColor: colors.TABBAR_INACTIVECOLOR,
          
        })}>
          <Tab.Screen name={'SearchTab'} component={SearchTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'FavorTab'} component={FavorTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'TripsTab'} component={TripsTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'ChatTab'} component={ChatTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'ProfileTab'} component={ProfileTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
      </Tab.Navigator>
  );
}

export default function Router() {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator>
          <Stack.Screen
            name={'Splash'}
            component={EnterStack}
            options={{
                headerShown: false,
            }}
          />
          <Stack.Screen name={'LoginIn'} component={Tabs} options={{
            animation: 'slide_from_bottom',
            presentation: 'card',
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'CreateTab'} component={CreateTab} options={{
            animation: 'slide_from_bottom',
            presentation: 'card',
            headerShown: false,
            gestureEnabled: false,
          }}/>
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
