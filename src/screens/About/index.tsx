import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { TextInput, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import CategoryView from '../../components/Advantages/CategoryView';
import { BASE_URL, APP_NAME } from '../../constants';

const AboutScreen = ({rentUpdateObj, setRentUpdateObj}) => {

	const navigation = useNavigation();
	const [name, setName] = React.useState('');
	const [index, setIndex] = React.useState(0);
	const bottomSheetRef = useRef<BottomSheet>(null);
	const input = useRef(null);

	const snapPoints = useMemo(() => ['53%', '90%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		setIndex(index);
	}, []);

	const next = () => {
		if (name.length) {
			let obj = rentUpdateObj;
			obj.description = name;
			setRentUpdateObj(obj);
			navigation.navigate('Price');
		} else {
			Alert.alert(APP_NAME, 'Введите описание!');
		}
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-fon-about.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(28),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(40),
					textAlign: 'left',
					position: 'absolute',
					top: Common.getLengthByIPhone7(320),
				}}>
					{`Опишите жилье`}
				</Text>
				<BottomSheet
					style={{
						backgroundColor: 'white',
						shadowColor: "black",
						shadowOffset: {
							width: 0,
							height: -12,
						},
						shadowOpacity: 0.05,
						shadowRadius: 30.00,
						elevation: 4,
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
					}}
					handleStyle={{
						// borderTopLeftRadius: Common.getLengthByIPhone7(30),
					}}
					// containerHeight={100}
					handleComponent={() => {
						if (index === 0) {
							return (<View
								style={{
									width: Common.getLengthByIPhone7(0),
									height: Common.getLengthByIPhone7(50),
									justifyContent: 'center',
								}}
							>
								<Text style={{
									fontFamily: 'SFProDisplay-Regular',
									fontSize: Common.getLengthByIPhone7(16),
									color: 'black',
									marginLeft: Common.getLengthByIPhone7(20),
								}}>
									Описание
								</Text>
							</View>);
						} else {
							return (<View
								style={{
									width: Common.getLengthByIPhone7(0),
									height: Common.getLengthByIPhone7(75),
									flexDirection: 'row',
									alignItems: 'center',
									justifyContent: 'center',
								}}
							>
								<TouchableOpacity style={{
									position: 'absolute',
									left: Common.getLengthByIPhone7(20),
								}}
								onPress={() => {
									navigation.goBack();
								}}>
									<Image
										source={require('./../../assets/ic-arrow-back.png')}
										style={{
											width: Common.getLengthByIPhone7(16),
											height: Common.getLengthByIPhone7(16),
										}}
									/>
								</TouchableOpacity>
								<Text style={{
									fontFamily: 'SFProDisplay-Medium',
									fontSize: Common.getLengthByIPhone7(14),
									color: 'black',
								}}>
									Составьте описание
								</Text>
								<TouchableOpacity style={{
									position: 'absolute',
									right: Common.getLengthByIPhone7(20),
								}}
								onPress={() => {
									input.current.blur();
									setIndex(0);
								}}>
									<Text style={{
										fontFamily: 'SFProDisplay-Regular',
										fontSize: Common.getLengthByIPhone7(14),
										color: '#141414',
										textDecorationLine: 'underline',
									}}>
										Готово
									</Text>
								</TouchableOpacity>
							</View>);
						}
					}}
					backgroundStyle={{
						
					}}
					ref={bottomSheetRef}
					index={index}
					snapPoints={snapPoints}
					onChange={handleSheetChanges}
				>
					<View style={{
						zIndex: 100,
						flex: 1,
						alignItems: 'center',
						backgroundColor: 'white',
					}}>
						<View style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: index === 0 ? Common.getLengthByIPhone7(176) : Common.getLengthByIPhone7(300),
						}}>
							<TextInput
								style={{
									width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
									height: index === 0 ? Common.getLengthByIPhone7(176) : Common.getLengthByIPhone7(300),
									borderRadius: Common.getLengthByIPhone7(10),
									backgroundColor: 'white',
									borderColor: 'rgba(0, 0, 0)',
									borderWidth: index === 0 ? 1 : 0,
									paddingLeft: Common.getLengthByIPhone7(13),
									paddingRight: Common.getLengthByIPhone7(13),
									fontFamily: 'SFProDisplay-Regular',
									fontSize: Common.getLengthByIPhone7(14),
									lineHeight: Common.getLengthByIPhone7(20),
									color: '#717171',
								}}
								ref={input}
								maxLength={500}
								multiline={true}
								numberOfLines={4}
								contextMenuHidden={false}
								autoCorrect={false}
								autoCompleteType={'off'}
								returnKeyType={'done'}
								secureTextEntry={false}
								allowFontScaling={false}
								underlineColorAndroid={'transparent'}
								onFocus={() => {
									setIndex(1);
								}}
								onChangeText={code => {
									setName(code);
								}}
								value={name}
							/>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: '#141414',
								opacity: 0.52,
								position: 'absolute',
								left: Common.getLengthByIPhone7(13),
								bottom: Common.getLengthByIPhone7(7),
							}}>
								{name.length}/500
							</Text>
						</View>
					</View>
				</BottomSheet>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
					backgroundColor: 'white',
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						next();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// advantageList: state.user.advantageList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentUpdateObj: payload => dispatch.user.setRentUpdateObj(payload),
});

export default connect(mstp, mdtp)(AboutScreen);