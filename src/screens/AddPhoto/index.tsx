import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import CategoryView from '../../components/Advantages/CategoryView';
import { BASE_URL, APP_NAME } from '../../constants';
import HeaderView from '../../components/AddPhoto/HeaderView';
import MenuModalView from '../../components/AddPhoto/MenuModalView';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import PhotoView from '../../components/AddPhoto/PhotoView';

const AddPhotoScreen = ({uploadPhotoToAdv, rentUpdateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [showModal, setShowModal] = React.useState(false);
	const [photos, setPhotos] = React.useState([null, null, null, null, null]);
	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['53%', '90%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	useEffect(() => {
		renderPhotos();
	}, []);

	useEffect(() => {
		renderPhotos();
	}, [photos]);

	const renderPhotos = () => {
		console.warn('renderPhotos: ', photos);
		let array = [];
		let views = [];
		for (let i = 0; i < photos.length; i++) {
			if (i === 0) {
				array.push(<PhotoView
					type={'big'}
					photo={photos[i]}
				/>);
			} else {
				views.push(<PhotoView
					photo={photos[i]}
				/>);

				if (views.length === 2) {
					array.push(<View style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
						marginTop: Common.getLengthByIPhone7(15),
					}}>
						{views}
					</View>);
					views = [];
				}
			}
		}

		if (views.length) {
			array.push(<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
				marginTop: Common.getLengthByIPhone7(15),
			}}>
				{views}
			</View>);
		}

		array.push(<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(20),
		}}/>);

		setBody(array);
	}

	const next = () => {
		if (photos.length >= 5) {
			let count = 0;
			for (let i = 0; i < photos.length; i++) {
				if (photos[i] !== null) {
					console.warn('photos[i]: ', photos[i]);
					count++;
				}
			}
			if (count < 5) {
				Alert.alert(APP_NAME, 'Загрузите не  менее 5 фотографий!');
				return;
			}
			let array = [];
			for (let i = 0; i < photos.length; i++) {
				array.push(uploadPhotoToAdv({id: rentUpdateObj.id, photo: photos[i]}));
			}

			Promise.all([array]).then(values => {
				console.warn(values);
				navigation.navigate('SetName');
			});
		} else {
			Alert.alert(APP_NAME, 'Загрузите не  менее 5 фотографий!');
		}
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-addphoto.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(28),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(40),
					textAlign: 'left',
					position: 'absolute',
					top: Common.getLengthByIPhone7(320),
				}}>
					{`Добавьте фото жилье`}
				</Text>
				<BottomSheet
					style={{
						backgroundColor: 'white',
						shadowColor: "black",
						shadowOffset: {
							width: 0,
							height: -12,
						},
						shadowOpacity: 0.05,
						shadowRadius: 30.00,
						elevation: 4,
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
					}}
					handleStyle={{
						// borderTopLeftRadius: Common.getLengthByIPhone7(30),
					}}
					// containerHeight={100}
					handleComponent={() => {
						return (<HeaderView
							onSelect={() => {
								setShowModal(true);
							}}
						/>);
					}}
					backgroundStyle={{
						
					}}
					ref={bottomSheetRef}
					index={0}
					snapPoints={snapPoints}
					onChange={handleSheetChanges}
				>
					<View style={{
						zIndex: 100,
						flex: 1,
						alignItems: 'center',
						backgroundColor: 'white',
					}}>
						<ScrollView style={{
							width: Common.getLengthByIPhone7(0),
							flex: 1,
							marginBottom: Common.getLengthByIPhone7(102),
						}}
						contentContainerStyle={{
							alignItems: 'center',
						}}>
							{body}
						</ScrollView>
					</View>
				</BottomSheet>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
					backgroundColor: 'white',
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						next();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
			<MenuModalView
				show={showModal}
				onClose={() => {
					setShowModal(false);
				}}
				onSelect={id => {
					setShowModal(false);
					if (id === 1) {
						setTimeout(() => {
							launchCamera({
								cameraType: 'back',
							})
							.then(resp => {
								console.warn('launchCamera: ', resp);
								if (resp.didCancel) {

								} else {
									if (resp.assets?.length) {
										ImageResizer.createResizedImage(resp.assets[0].uri, resp.assets[0].width / 2, resp.assets[0].height / 2, "JPEG", 100, 0)
										.then(response => {
											// console.warn('createResizedImage: ', response);
											let array = JSON.parse(JSON.stringify(photos));
											let index = -1;
											for (let i = 0; i < array.length; i++) {
												if (array[i] == null) {
													index = i;
													break;
												}
											}
											console.warn('index: ', index);
											if (index === -1) {
												array.push(response.uri);
											} else {
												array[index] = response.uri;
											}
											
											setPhotos(array);
										})
										.catch(err => {
											console.warn('createResizedImage err: ', err);
										});
									}
								}
							})
							.catch(err => {
								console.warn('launchCamera err: ', err);
							});
							// setShowCamera(true);
						}, 1000);
					} else {
						setTimeout(() => {
							launchImageLibrary({
								mediaType: 'photo',
							})
							.then(response => {
								console.warn('response: ', response);
								if (response.assets?.length) {
									ImageResizer.createResizedImage(response.assets[0].uri, response.assets[0].width / 2, response.assets[0].height / 2, "JPEG", 100, 0)
									.then(response => {
										// console.warn('createResizedImage: ', response);
										let array = JSON.parse(JSON.stringify(photos));
										let index = -1;
										for (let i = 0; i < array.length; i++) {
											if (array[i] == null) {
												index = i;
												break;
											}
										}

										if (index === -1) {
											array.push(response.uri);
										} else {
											array[index] = response.uri;
										}
										setPhotos(array);
									})
									.catch(err => {
										console.warn('createResizedImage err: ', err);
									});
								}
							})
							.catch(err => {
								console.warn('response err: ', err);
							});
						}, 1000);
					}
				}}
			/>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// advantageList: state.user.advantageList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	uploadPhotoToAdv: payload => dispatch.user.uploadPhotoToAdv(payload),
});

export default connect(mstp, mdtp)(AddPhotoScreen);