import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import GradientButton from '../../components/GradientButton';
import { BASE_URL, APP_NAME } from '../../constants';

const AdvEndScreen = ({rentUpdateObj, updateAdv}) => {

	const navigation = useNavigation();
	const [value, setValue] = React.useState('0' + String.fromCharCode(0x20BD));
	const [image, setImage] = React.useState(null);
	const input = useRef(null);

	useEffect(() => {
		console.warn('rentUpdateObj: ', rentUpdateObj);
		if (rentUpdateObj.photos?.length) {
			console.warn('rentUpdateObj.photos: ', rentUpdateObj.photos);
			setImage(<Image
				source={{uri: BASE_URL + rentUpdateObj.photos[0].photo}}
				style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(205),
					resizeMode: 'cover',
				}}
			/>);
		}
	}, []);

	const next = () => {
		let obj = rentUpdateObj;
		let categories = obj.categories;
		let array = [];
		for (let i = 0; i < categories.length; i++) {
			array.push(categories[i].id);
		}
		obj.categories = array;

		let facilities = obj.facilities;
		array = [];
		for (let i = 0; i < facilities.length; i++) {
			array.push(facilities[i].id);
		}
		obj.facilities = array;
		console.warn('obj: ', obj);
		updateAdv(obj)
		.then(() => {
			navigation.navigate('AdvSuccess');
		})
		.catch(err => {
			Alert.alert(APP_NAME, err);
		});
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-fon-adv.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}
			>
				<ScrollView style={{
					width: Common.getLengthByIPhone7(0),
					height: Dimensions.get('screen').height,
					marginBottom: Common.getLengthByIPhone7(102),
				}}
				contentContainerStyle={{
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(28),
						color: 'white',
						marginTop: Common.getLengthByIPhone7(100),
						marginBottom: Common.getLengthByIPhone7(15),
						textAlign: 'left',
					}}>
						{`Взгляните на объявление!`}
					</Text>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'white',
						marginBottom: Common.getLengthByIPhone7(35),
						textAlign: 'left',
					}}>
						{`Гости увидят объявление через 24 часа после публикации. Вы в любой момент можете добавить дополнительную информацию или внести изменения.`}
					</Text>
					<View style={{
						backgroundColor: 'white',
						width: Common.getLengthByIPhone7(0),
						minHeight: Common.getLengthByIPhone7(430),
						borderTopLeftRadius: Common.getLengthByIPhone7(20),
						borderTopRightRadius: Common.getLengthByIPhone7(20),
						alignItems: 'center',
					}}>
						<View style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(205),
							borderRadius: Common.getLengthByIPhone7(11),
							backgroundColor: '#c4c4c4',
							marginTop: Common.getLengthByIPhone7(20),
							overflow: 'hidden',
						}}>
							{image}
						</View>
						<Text style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(26),
							color: '#171717',
							marginTop: Common.getLengthByIPhone7(25),
							textAlign: 'left',
						}}>
							{rentUpdateObj.header}
						</Text>
						<View
							style={{
								width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
								paddingTop: Common.getLengthByIPhone7(20),
								paddingBottom: Common.getLengthByIPhone7(20),
								borderTopColor: 'rgba(0, 0, 0, 0.17)',
								borderTopWidth: 1,
								borderBottomColor: 'rgba(0, 0, 0, 0.17)',
								borderBottomWidth: 1,
								marginTop: Common.getLengthByIPhone7(29),
								flexDirection: 'row',
								alignItems: 'center',
								justifyContent: 'space-between',
							}}
						>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(16),
								color: 'black',
								textAlign: 'left',
							}}>
								Домик целиком, хозяин: Maksim
							</Text>
							<View style={{
								width: Common.getLengthByIPhone7(43),
								height: Common.getLengthByIPhone7(43),
								borderRadius: Common.getLengthByIPhone7(43) / 2,
								backgroundColor: '#c4c4c4',
							}}>

							</View>
						</View>
						<View style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							flexDirection: 'row',
							alignItems: 'center',
							justifyContent: 'space-between',
							height: Common.getLengthByIPhone7(63),
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
								textAlign: 'left',
							}}>
								{Common.getDigitStr(rentUpdateObj.guest, {
									d1: ' гость',
									d2_d4: ' гостя',
									d5_d10: ' гостей',
									d11_d19: ' гостей',
								})}
							</Text>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
								textAlign: 'left',
							}}>
								{Common.getDigitStr(rentUpdateObj.rooms, {
									d1: ' спальня',
									d2_d4: ' спальни',
									d5_d10: ' спальней',
									d11_d19: ' спален',
								})}
							</Text>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
								textAlign: 'left',
							}}>
								{Common.getDigitStr(rentUpdateObj.beds, {
									d1: ' кровать',
									d2_d4: ' кровати',
									d5_d10: ' кроватей',
									d11_d19: ' кроватей',
								})}
							</Text>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
								textAlign: 'left',
							}}>
								{Common.getDigitStr(rentUpdateObj.bathrooms, {
									d1: ' ванная',
									d2_d4: ' ванных',
									d5_d10: ' ванных',
									d11_d19: ' вванных',
								})}
							</Text>
						</View>
					</View>
				</ScrollView>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
					backgroundColor: 'white',
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<GradientButton
						title={'Опубликовать объявление'}
						style={{
							width: Common.getLengthByIPhone7(221),
						}}
						onPress={() => {
							next();
						}}
					/>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// advantageList: state.user.advantageList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	updateAdv: payload => dispatch.user.updateAdv(payload),
});

export default connect(mstp, mdtp)(AdvEndScreen);