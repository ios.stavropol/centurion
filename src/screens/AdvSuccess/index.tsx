import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import GradientButton from '../../components/GradientButton';

const AdvSuccessScreen = ({rentUpdateObj, updateAdv}) => {

	const navigation = useNavigation();

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-fon-success.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(12),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(28),
					textAlign: 'left',
				}}>
					{`Основатель компании Максим Янушевич`}
				</Text>
				<View style={{
					backgroundColor: 'black',
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(430),
					borderTopLeftRadius: Common.getLengthByIPhone7(20),
					borderTopRightRadius: Common.getLengthByIPhone7(20),
					alignItems: 'center',
				}}>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(28),
						color: 'white',
						marginTop: Common.getLengthByIPhone7(32),
						marginBottom: Common.getLengthByIPhone7(15),
						textAlign: 'left',
					}}>
						{`Поздравляем, Мэри!`}
					</Text>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'white',
						marginBottom: Common.getLengthByIPhone7(35),
						textAlign: 'left',
					}}>
						{`Позвольте первым поприветствовать вас на Centurion. Клиентский сервис лежит в основе нашей работы, и мы рады приветствовать вас на нашей платформе. 
– Максим`}
					</Text>
					<GradientButton
						title={'Вперед'}
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							position: 'absolute',
							bottom: Common.getLengthByIPhone7(20),
						}}
						onPress={() => {
							navigation.navigate('LoginIn');
						}}
					/>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// advantageList: state.user.advantageList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	updateAdv: payload => dispatch.user.updateAdv(payload),
});

export default connect(mstp, mdtp)(AdvSuccessScreen);