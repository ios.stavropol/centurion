import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import CategoryView from '../../components/Advantages/CategoryView';
import { BASE_URL } from '../../constants';

const AdvantagesScreen = ({advantageList, createAdv, rentCreateObj, setRentCreateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);

	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['53%', '90%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	useEffect(() => {
		console.warn(advantageList);
		if (advantageList) {
			let array = [];
			let view = [];
			for (let i = 0; i < advantageList.length; i++) {
				// console.warn(BASE_URL + advantageList[i].icon);
				if (i % 2 === 1) {
					view.push(<CategoryView
						title={advantageList[i].name}
						icon={BASE_URL + advantageList[i].icon}
						onSelect={selected => {
							obj = rentCreateObj;
							let array = obj.facilities;
							if (selected) {
								array.push(advantageList[i].id);
							} else {
								for (let i = 0; i < array.length; i++) {
									if (array[i] === advantageList[i].id) {
										array.splice(i, 1);
										break;
									}
								}
							}
							obj.facilities = array;
							setRentCreateObj(obj);
						}}
					/>);
					array.push(<View style={{
						marginTop: Common.getLengthByIPhone7(13),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					}}>
						{view}
					</View>);
					view = [];
				} else {
					view.push(<CategoryView
						title={advantageList[i].name}
						icon={BASE_URL + advantageList[i].icon}
						onSelect={selected => {
							obj = rentCreateObj;
							let array = obj.facilities;
							if (selected) {
								array.push(advantageList[i].id);
							} else {
								for (let i = 0; i < array.length; i++) {
									if (array[i] === advantageList[i].id) {
										array.splice(i, 1);
										break;
									}
								}
							}
							obj.facilities = array;
							setRentCreateObj(obj);
						}}
					/>);
				}
			}

			if (view.length) {
				array.push(<View style={{
					marginTop: Common.getLengthByIPhone7(13),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				}}>
					{view}
				</View>);
			}

			setBody(array);
		}
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-advantage.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(28),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(40),
					textAlign: 'left',
					position: 'absolute',
					top: Common.getLengthByIPhone7(270),
				}}>
					{`Расскажите гостям\nо преимуществах\nвашего жилья`}
				</Text>
				<BottomSheet
					style={{
						backgroundColor: 'white',
						shadowColor: "black",
						shadowOffset: {
							width: 0,
							height: -12,
						},
						shadowOpacity: 0.05,
						shadowRadius: 30.00,
						elevation: 4,
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
					}}
					handleStyle={{
						// borderTopLeftRadius: Common.getLengthByIPhone7(30),
					}}
					// containerHeight={100}
					handleComponent={() => {
						return (<View style={{
							height: Common.getLengthByIPhone7(60),
							backgroundColor: 'white',
							borderTopLeftRadius: Common.getLengthByIPhone7(30),
							borderTopRightRadius: Common.getLengthByIPhone7(30),
							paddingLeft: Common.getLengthByIPhone7(20),
							overflow: 'hidden',
							alignItems: 'flex-start',
							justifyContent: 'center',
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(18),
								color: 'black',
							}}>
								Есть ли у вас особые удобства?
							</Text>
						</View>);
					}}
					backgroundStyle={{
						
					}}
					ref={bottomSheetRef}
					index={0}
					snapPoints={snapPoints}
					onChange={handleSheetChanges}
				>
					<View style={{
						zIndex: 100,
						flex: 1,
						alignItems: 'center',
						backgroundColor: 'white',
					}}>
						<ScrollView style={{

						}}
						contentContainerStyle={{

						}}>
							{body}
						</ScrollView>
					</View>
				</BottomSheet>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						createAdv(rentCreateObj)
						.then(() => {
							navigation.navigate('AddPhoto');
						})
						.catch(err => {

						});
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	advantageList: state.user.advantageList,
	rentCreateObj: state.user.rentCreateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentCreateObj: payload => dispatch.user.setRentCreateObj(payload),
	createAdv: payload => dispatch.user.createAdv(payload),
});

export default connect(mstp, mdtp)(AdvantagesScreen);