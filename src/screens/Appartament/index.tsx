import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from '../../constants';
import OwnerView from '../../components/Appartament/OwnerView';
import FeaturesView from '../../components/Appartament/FeaturesView';
import RoomsView from '../../components/Appartament/RoomsView';
import AboutView from '../../components/Appartament/AboutView';
import CommentsView from '../../components/Appartament/CommentsView';
import OwnerBigView from '../../components/Appartament/OwnerBigView';
import ButtonView from '../../components/Appartament/ButtonView';
import BuyView from '../../components/Appartament/BuyView';
import PhotoView from '../../components/Appartament/PhotoView';

const AppartamentScreen = ({route, getObject, setFavor, startChat}) => {

	const navigation = useNavigation();
	const [data, setData] = React.useState(null);

	useEffect(() => {
		console.warn(route);
		getObject(route?.params?.data.id)
		.then(res => {
			setData(res);
		})
		.catch(err => {

		});
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				marginBottom: Common.getLengthByIPhone7(100),
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				<PhotoView
					data={data}
					onClose={() => {
						navigation.goBack();
					}}
					onShare={() => {

					}}
					onFavor={() => {
						setFavor(data?.id)
						.then(() => {
							let d = JSON.parse(JSON.stringify(data));
							d.is_favourite = !d.is_favourite;
							setData(d);
						})
						.catch(err => {

						});
					}}
				/>
				<View style={{
					marginTop: Common.getLengthByIPhone7(26),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(24),
					}}>
						{data?.header}
					</Text>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(24),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-star.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(20),
							resizeMode: 'contain',
						}}
					/>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						marginLeft: Common.getLengthByIPhone7(10),
					}}>
						5.0
					</Text>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						opacity: 0.3,
						marginLeft: Common.getLengthByIPhone7(5),
					}}>
						(74 отзыва)
					</Text>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-geotag.png')}
						style={{
							width: Common.getLengthByIPhone7(20),
							height: Common.getLengthByIPhone7(20),
							resizeMode: 'contain',
						}}
					/>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(14),
						marginLeft: Common.getLengthByIPhone7(10),
						opacity: 0.5,
					}}>
						{data?.region}
					</Text>
				</View>
				<OwnerView
					style={{
						marginTop: Common.getLengthByIPhone7(20),
						marginBottom: Common.getLengthByIPhone7(20),
					}}
					onStartChat={() => {
						startChat(data?.id)
						.then(() => {
							navigation.navigate('Chat2', {data: data});
						})
						.catch(err => {

						});
					}}
					data={data}
				/>
				<FeaturesView
					style={{
						
					}}
					data={data}
				/>
				<RoomsView
					style={{
						
					}}
					data={data}
				/>
				<AboutView
					style={{
						
					}}
					data={data}
				/>
				<CommentsView
					style={{
						
					}}
					data={data}
				/>
				<OwnerBigView
					style={{
						
					}}
					data={data}
				/>
				<ButtonView
					style={{
						
					}}
					title={'Свободные даты'}
					subtitle={'Укажите даты поездки'}
				/>
				<ButtonView
					style={{
						borderTopWidth: 0,
					}}
					title={'Правила дома'}
					subtitle={'Прибытие: После 15:00'}
				/>
				<ButtonView
					style={{
						borderTopWidth: 0,
						borderBottomWidth: 0,
					}}
					title={'Правила отмены'}
				/>
				<BuyView
					style={{
						
					}}
					data={data}
					onClick={() => {
						navigation.navigate('BuyInfo', {data: data});
					}}
				/>
			</ScrollView>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	getObject: payload => dispatch.user.getObject(payload),
	setFavor: payload => dispatch.user.setFavor(payload),
	startChat: payload => dispatch.user.startChat(payload),
});

export default connect(mstp, mdtp)(AppartamentScreen);