import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from '../../constants';
import GradientButton from '../../components/GradientButton';

const BuyScreen = ({route, getObject}) => {

	const navigation = useNavigation();
	const [data, setData] = React.useState(null);

	useEffect(() => {
		setData(route?.params.data);
	}, []);

	const renderView = (title, icon, style, action) => {
		return (<View style={[{
			marginTop: Common.getLengthByIPhone7(20),
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
			}}>
				<Image
					source={icon}
					style={{
						width: Common.getLengthByIPhone7(24),
						height: Common.getLengthByIPhone7(24),
						resizeMode: 'contain',
					}}
				/>
				<Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					marginLeft: Common.getLengthByIPhone7(15),
				}}>
					{title}
				</Text>
			</View>
			<TouchableOpacity>
				<Image
					source={require('./../../assets/ic-add-circle.png')}
					style={{
						width: Common.getLengthByIPhone7(24),
						height: Common.getLengthByIPhone7(24),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
		</View>);
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				marginBottom: Common.getLengthByIPhone7(100),
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				<View style={{
					marginTop: Common.getLengthByIPhone7(25),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					paddingBottom: Common.getLengthByIPhone7(20),
				}}>
					<View style={{
						width: Common.getLengthByIPhone7(142),
						height: Common.getLengthByIPhone7(109),
						borderRadius: Common.getLengthByIPhone7(10),
						overflow: 'hidden',
						backgroundColor: 'gray',
					}}>
						<Image
							source={{uri: BASE_URL + data?.cover.photo}}
							style={{
								width: Common.getLengthByIPhone7(142),
								height: Common.getLengthByIPhone7(109),
								resizeMode: 'cover',
							}}
						/>
					</View>
					<View style={{
						width: Common.getLengthByIPhone7(175),
						marginLeft: Common.getLengthByIPhone7(18),
					}}>
						<Text style={{
							width: Common.getLengthByIPhone7(175),
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							{data?.header}
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(18),
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							&#8381; {data?.price} <Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							color: '#7D7F88',
						}}>
							/ 24 дня
						</Text>
						</Text>
						<View style={{
							flexDirection: 'row',
							alignItems: 'center',
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							<Image
								source={require('./../../assets/ic-star.png')}
								style={{
									width: Common.getLengthByIPhone7(15),
									height: Common.getLengthByIPhone7(15),
									resizeMode: 'contain',
								}}
							/>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(12),
								marginLeft: Common.getLengthByIPhone7(4),
							}}>
								5.0
							</Text>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(12),
								marginLeft: Common.getLengthByIPhone7(2),
								color: '#7D7F88',
							}}>
								(74)
							</Text>
						</View>
					</View>
				</View>
				<View
					style={{
						backgroundColor: '#F3F3F3',
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(10),
					}}
				/>
				<View style={{
					marginTop: Common.getLengthByIPhone7(22),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}>
					<View style={{

					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
						}}>
							Прибытие
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							11 ноя.
						</Text>
					</View>
					<View style={{
						alignItems: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
						}}>
							Выезд
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							5 дек.
						</Text>
					</View>
					<View style={{
						alignItems: 'flex-end',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
						}}>
							Гости
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							1 гость
						</Text>
					</View>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(34),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(20),
					}}>
						Оплатить с помощью
					</Text>
				</View>
				{renderView('Дебетовая или кредитная карта', require('./../../assets/ic-pay-card.png'), {

				}, () => {

				})}
				{renderView('Google Pay', require('./../../assets/ic-pay-google.png'), {
					
				}, () => {

				})}
				{renderView('Apple Pay', require('./../../assets/ic-pay-apple.png'), {
					
				}, () => {

				})}
				{renderView('PayPal', require('./../../assets/ic-pay-paypal.png'), {
					marginBottom: Common.getLengthByIPhone7(30),
				}, () => {
					
				})}
				<View
					style={{
						backgroundColor: '#F3F3F3',
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(10),
					}}
				/>
				<View style={{
					marginTop: Common.getLengthByIPhone7(30),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(20),
					}}>
						Правила отмены
					</Text>
				</View>
				<Text style={{
					marginTop: Common.getLengthByIPhone7(19),
					marginBottom: Common.getLengthByIPhone7(40),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
				}}>
					Плата за это бронирование не возращается. <Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					textDecorationLine: 'underline',
				}}
				onPress={() => {

				}}>
					Подробнее.
				</Text>
				</Text>
				<View
					style={{
						backgroundColor: '#F3F3F3',
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(10),
					}}
				/>
				<TouchableOpacity style={{
					alignItems: 'center',
					justifyContent: 'center',
					flexDirection: 'row',
					backgroundColor: 'black',
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(50),
					borderRadius: Common.getLengthByIPhone7(10),
					marginTop: Common.getLengthByIPhone7(22),
					marginBottom: Common.getLengthByIPhone7(18),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: 'white',
					}}>
						Оплатить с помощью карты
					</Text>
					<Image
						source={require('./../../assets/ic-pay-card.png')}
						style={{
							width: Common.getLengthByIPhone7(24),
							height: Common.getLengthByIPhone7(24),
							resizeMode: 'contain',
							tintColor: 'white',
							marginLeft: Common.getLengthByIPhone7(10),
						}}
					/>
				</TouchableOpacity>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(12),
					marginBottom: Common.getLengthByIPhone7(50),
				}}>
					Нажимая на кнопку ниже, я принимаю условия следующих документов: <Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(12),
					textDecorationLine: 'underline',
				}}
				onPress={() => {

				}}>
					Правила дома, установленные хозяином
				</Text>, и <Text style={{
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(12),
					textDecorationLine: 'underline',
				}}
				onPress={() => {
					
				}}>
					Правила компенсации гостю
				</Text>
				</Text>
			</ScrollView>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	getObject: payload => dispatch.user.getObject(payload),
});

export default connect(mstp, mdtp)(BuyScreen);