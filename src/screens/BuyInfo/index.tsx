import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from '../../constants';
import GradientButton from '../../components/GradientButton';

const BuyInfoScreen = ({route, getObject}) => {

	const navigation = useNavigation();
	const [data, setData] = React.useState(null);

	useEffect(() => {
		setData(route?.params.data);
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				marginBottom: Common.getLengthByIPhone7(100),
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				<View style={{
					marginTop: Common.getLengthByIPhone7(25),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
				}}>
					<View style={{
						width: Common.getLengthByIPhone7(142),
						height: Common.getLengthByIPhone7(109),
						borderRadius: Common.getLengthByIPhone7(10),
						overflow: 'hidden',
						backgroundColor: 'gray',
					}}>
						<Image
							source={{uri: BASE_URL + data?.cover.photo}}
							style={{
								width: Common.getLengthByIPhone7(142),
								height: Common.getLengthByIPhone7(109),
								resizeMode: 'cover',
							}}
						/>
					</View>
					<View style={{
						width: Common.getLengthByIPhone7(175),
						marginLeft: Common.getLengthByIPhone7(18),
					}}>
						<Text style={{
							width: Common.getLengthByIPhone7(175),
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							{data?.header}
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(18),
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							&#8381; {data?.price} <Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(12),
							color: '#7D7F88',
						}}>
							/ сутки
						</Text>
						</Text>
						<View style={{
							flexDirection: 'row',
							alignItems: 'center',
							marginTop: Common.getLengthByIPhone7(8),
						}}>
							<Image
								source={require('./../../assets/ic-star.png')}
								style={{
									width: Common.getLengthByIPhone7(15),
									height: Common.getLengthByIPhone7(15),
									resizeMode: 'contain',
								}}
							/>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(12),
								marginLeft: Common.getLengthByIPhone7(4),
							}}>
								5.0
							</Text>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(12),
								marginLeft: Common.getLengthByIPhone7(2),
								color: '#7D7F88',
							}}>
								(74)
							</Text>
						</View>
					</View>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(40),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(20),
					}}>
						Детали бронирования
					</Text>
					<TouchableOpacity>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							opacity: 0.21,
						}}>
							Изменить
						</Text>
					</TouchableOpacity>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(20),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					paddingBottom: Common.getLengthByIPhone7(32),
					borderBottomColor: 'rgba(0, 0, 0, 0.33)',
					borderBottomWidth: 1,
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(16),
					}}>
						Даты
					</Text>
					<Text style={{
						marginTop: Common.getLengthByIPhone7(12),
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(18),
						color: '#7D7F88',
						textDecorationLine: 'underline',
					}}>
						11 Ноя - 5 Дек
					</Text>
					<Text style={{
						marginTop: Common.getLengthByIPhone7(26),
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(16),
					}}>
						Кол-во гостей
					</Text>
					<Text style={{
						marginTop: Common.getLengthByIPhone7(12),
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(18),
						color: '#7D7F88',
						textDecorationLine: 'underline',
					}}>
						3 Гостя
					</Text>
				</View>
				<View style={{
					marginTop: Common.getLengthByIPhone7(30),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					paddingBottom: Common.getLengthByIPhone7(39),
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(20),
					}}>
						Налоги и сборы
					</Text>
					<View style={{
						marginTop: Common.getLengthByIPhone7(20),
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#7D7F88',
						}}>
							17 950₽ х 24 дня
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							₽12360
						</Text>
					</View>
					<View style={{
						marginTop: Common.getLengthByIPhone7(16),
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#7D7F88',
						}}>
							Плата за уборку
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							₽200
						</Text>
					</View>
					<View style={{
						marginTop: Common.getLengthByIPhone7(16),
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Bold',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							Всего
						</Text>
						<Text style={{
							fontFamily: 'SFProDisplay-Bold',
							fontSize: Common.getLengthByIPhone7(16),
						}}>
							₽12560
						</Text>
					</View>
				</View>
				<GradientButton
					title={'Забронировать'}
					style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(50),
					}}
					onPress={() => {
						navigation.navigate('Buy', {data: data});
					}}
				/>
			</ScrollView>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	getObject: payload => dispatch.user.getObject(payload),
});

export default connect(mstp, mdtp)(BuyInfoScreen);