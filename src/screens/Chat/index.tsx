import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import TopView from '../../components/Chat/TopView';
import { GiftedChat, InputToolbar, Composer, Send, Bubble } from 'react-native-gifted-chat';
import ru from 'dayjs/locale/ru';

const ChatScreen = ({route, getChat, sendMessage, userProfile}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);
	const [messages, setMessages] = React.useState([]);

	useEffect(() => {
		getChat(route?.params?.data?.id)
		.then(res => {
			let array = [];
			res = res.sort((a, b) => {
				return a.send_at < b.send_at;
			});
			for (let i = 0; i < res.length; i++) {
				array.push({
					_id: res[i].send_at,
					text: res[i].text,
					createdAt: new Date(Math.round(res[i].send_at)*1000),
					user: {
					  _id: res[i].sender_id,
					},
				  },);
			}

			setMessages(array);
		})
		.catch(err => {

		});
	}, [])

	const onSend = useCallback((messages = []) => {
		console.warn(messages);
		if (messages?.length) {
			sendMessage({
				object_id: route?.params?.data?.id,
				message_text: messages[0].text,
			})
			.then(() => {
				setMessages(previousMessages => GiftedChat.append(previousMessages, messages));
			})
			.catch(err => {

			});
		}
	}, [])

	return (
    <View style={{
		width: Common.getLengthByIPhone7(0),
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'flex-start',
    }}>
		<TopView
			data={route?.params?.data}
		/>
		<View style={{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			marginBottom: Common.getLengthByIPhone7(100),
		}}>
			<GiftedChat
				messages={messages}
				locale={ru}
				dateFormat='DD.MM.YYYY'
				onSend={messages => onSend(messages)}
				showUserAvatar={false}
				renderAvatar={() => {
					return null;
				}}
				renderBubble={props => {
					let date = new Date(props.currentMessage.createdAt);
					let dd = date.getHours();
					dd = dd < 10 ? '0' + dd : dd;
					let mm = date.getMinutes();
					mm = mm < 10 ? '0' + mm : mm;
					let time = dd + ':' + mm;

					let backgroundColor = '#fdfdfd';
					let borderTopLeftRadius = Common.getLengthByIPhone7(10);
					let borderBottomLeftRadius = 0;
					let borderTopRightRadius = Common.getLengthByIPhone7(10);
					let borderBottomRightRadius = Common.getLengthByIPhone7(10);
					let marginLeft = 0;
					let marginRight = Common.getLengthByIPhone7(8);
					let timeLeft = null;
					let timeRight = (<Text style={{
						color: '#7D7F88',
						fontFamily: 'SFProDisplay-Regular',
						fontWeight: 'normal',
						fontSize: Common.getLengthByIPhone7(13),
						// lineHeight: Common.getLengthByIPhone7(12),
					}}>
						{time}
					</Text>);
					let color = '#1A1E25';

					if (userProfile?.id == props.currentMessage.user._id) {
						backgroundColor = '#212121';
						borderTopLeftRadius = Common.getLengthByIPhone7(10);
						borderBottomLeftRadius = Common.getLengthByIPhone7(10);
						borderTopRightRadius = Common.getLengthByIPhone7(10);
						borderBottomRightRadius = 0;
						marginLeft = Common.getLengthByIPhone7(8);
						marginRight = 0;
						timeRight = null;
						timeLeft = (<Text style={{
							color: '#7D7F88',
							fontFamily: 'SFProDisplay-Regular',
							fontWeight: 'normal',
							fontSize: Common.getLengthByIPhone7(13),
							// lineHeight: Common.getLengthByIPhone7(12),
						}}>
							{time}
						</Text>);
						color = 'white';
					}
					
					return (<View style={{
						flexDirection: 'row',
						alignItems: 'flex-end',
					}}>
						{timeLeft}
						<View style={{
							paddingLeft: Common.getLengthByIPhone7(20),
							paddingRight: Common.getLengthByIPhone7(20),
							paddingTop: Common.getLengthByIPhone7(10),
							paddingBottom: Common.getLengthByIPhone7(10),
							backgroundColor,
							borderColor: '#E3E3E7',
							borderWidth: 1,
							borderTopLeftRadius,
							borderBottomLeftRadius,
							borderTopRightRadius,
							borderBottomRightRadius,
							marginLeft,
							marginRight,
						}}>
							<Text style={{
								color: '#1A1E25',
								fontFamily: 'SFProDisplay-Regular',
								fontWeight: 'normal',
								fontSize: Common.getLengthByIPhone7(16),
								lineHeight: Common.getLengthByIPhone7(21),
								color,
							}}>
								{props.currentMessage.text}
							</Text>
						</View>
						{timeRight}
					</View>);
				}}
				user={{
					_id: userProfile?.id,
				}}
			/>
		</View>
    </View>
  );
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	getChat: payload => dispatch.user.getChat(payload),
	sendMessage: payload => dispatch.user.sendMessage(payload),
});

export default connect(mstp, mdtp)(ChatScreen);