import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import SearchView from '../../components/Chats/SearchView';
import { BASE_URL } from '../../constants';

const ChatsScreen = ({getAllChats, chatList}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState([]);

  useEffect(() => {
    getAllChats()
    .then(() => {

    })
    .catch(err => {

    });
  }, []);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < chatList.length; i++) {
      array.push(renderView(chatList[i]));
    }
    setBody(array);
  }, [chatList]);

  const renderView = (data) => {

    let date = new Date(data?.last_message_date*1000);
    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let hh = date.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = date.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    return (<TouchableOpacity style={{
      width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingTop: Common.getLengthByIPhone7(10),
      paddingBottom: Common.getLengthByIPhone7(10),
    }}
    onPress={() => {
      navigation.navigate('Chat', {data: {
        id: data?.booking_id,
        header: data?.booking_name,
        cover: data?.photo,
      }});
    }}>
      <View style={{
        flexDirection: 'row',
        alignItems: 'center',
      }}>
        <View style={{
          width: Common.getLengthByIPhone7(52),
          height: Common.getLengthByIPhone7(52),
          borderRadius: Common.getLengthByIPhone7(26),
          backgroundColor: '#c4c4c4',
          overflow: 'hidden',
        }}>
          <Image
            source={{uri: BASE_URL + data?.photo?.photo}}
            style={{
              width: Common.getLengthByIPhone7(52),
              height: Common.getLengthByIPhone7(52),
              resizeMode: 'cover',
            }}
          />
        </View>
        <View style={{
          marginLeft: Common.getLengthByIPhone7(16),
        }}>
          <Text style={{
            fontFamily: 'SFProDisplay-Bold',
            fontSize: Common.getLengthByIPhone7(16),
          }}>
            {data?.booking_name}
          </Text>
          <Text style={{
            marginTop: Common.getLengthByIPhone7(8),
            fontFamily: 'SFProDisplay-Regular',
            fontSize: Common.getLengthByIPhone7(14),
            color: '#7D7F88',
          }}>
            {data?.last_message_text}
          </Text>
        </View>
      </View>
      <View style={{
        alignItems: 'flex-end',
        height: Common.getLengthByIPhone7(40),
      }}>
        <Text style={{
          fontFamily: 'SFProDisplay-Regular',
          fontSize: Common.getLengthByIPhone7(12),
          color: '#7D7F88',
        }}>
          {dd}.{mm}.{yyyy} {hh}:{ii}
        </Text>
      </View>
    </TouchableOpacity>);
  }

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
      <SearchView/>
      <ScrollView style={{
        width: Common.getLengthByIPhone7(0),
        flex: 1,
        marginBottom: Common.getLengthByIPhone7(100),
        marginTop: Common.getLengthByIPhone7(20),
      }}
      contentContainerStyle={{
        alignItems: 'center',
      }}>
        {body}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
	chatList: state.user.chatList,
});

const mdtp = (dispatch: Dispatch) => ({
	getAllChats: () => dispatch.user.getAllChats(),
});

export default connect(mstp, mdtp)(ChatsScreen);