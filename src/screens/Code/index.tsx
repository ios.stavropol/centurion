import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, ImageBackground, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import {
	CodeField,
	Cursor,
	useBlurOnFulfill,
	useClearByFocusCell,
} from 'react-native-confirmation-code-field';

const {Value, Text: AnimatedText} = Animated;

const CELL_HEIGHT = Common.getLengthByIPhone7(55);
const CELL_WIDTH = Common.getLengthByIPhone7(47);
const CELL_BORDER_RADIUS = Common.getLengthByIPhone7(8);
const NOT_EMPTY_CELL_BG_COLOR = '#E5EAEF';
const ACTIVE_CELL_BG_COLOR = 'transparent';
const CELL_COUNT = 6;

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({hasValue, index, isFocused}) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const CodeScreen = ({route, sendCode, getCode}) => {

  const navigation = useNavigation();
  const [value, setValue] = React.useState('');

	const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
	const [props, getCellOnLayoutHandler] = useClearByFocusCell({
		value,
		setValue,
	});

	const renderCell = ({index, symbol, isFocused}) => {
		const hasValue = Boolean(symbol);
		const animatedCellStyle = {
			backgroundColor: hasValue
				? animationsScale[index].interpolate({
					inputRange: [0, 1],
					outputRange: ['transparent', ACTIVE_CELL_BG_COLOR],
				})
				: animationsColor[index].interpolate({
					inputRange: [0, 1],
					outputRange: ['transparent', ACTIVE_CELL_BG_COLOR],
				}),
			borderRadius: CELL_BORDER_RADIUS,
			borderWidth: 2,
			borderColor: isFocused ? 'black' : (hasValue ? 'black' : '#b0b0b0'),
		};

		// Run animation on next event loop tik
		// Because we need first return new style prop and then animate this value
		setTimeout(() => {
		animateCell({hasValue, index, isFocused});
		}, 0);

		return (
			<View style={{
				height: CELL_HEIGHT,
				width: CELL_WIDTH,
				borderRadius: CELL_BORDER_RADIUS,
				overflow: 'hidden',
				marginHorizontal: Common.getLengthByIPhone7(4),
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				<AnimatedText
					key={index}
					style={[typography.H1_M24_140, {
						// fontSize: 16,
						height: CELL_HEIGHT,
						width: CELL_WIDTH,
						lineHeight: CELL_HEIGHT - 5,
						// ...Platform.select({web: {lineHeight: 65}}),
						textAlign: 'center',
						color: colors.BLACK_COLOR,
						// color: 'transparent'
					}, animatedCellStyle]}
					onLayout={getCellOnLayoutHandler(index)}>
					{symbol || (isFocused ? <Cursor /> : null)}
				</AnimatedText>
			</View>
		);
	};

	useEffect(() => {
		if (value.length === 6) {
			sendCode({phone: route.params?.phone, code: value})
			.then(() => {
				navigation.navigate('Notify');
			})
			.catch(err => {

			});
		}
	}, [value]);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
        }}>
			<ImageBackground source={require('./../../assets/splash2.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}
			>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					marginTop: Common.getLengthByIPhone7(72),
					flex: 1,
					backgroundColor: 'white',
					borderTopLeftRadius: Common.getLengthByIPhone7(25),
					borderTopRightRadius: Common.getLengthByIPhone7(25),
				}}>
					<View style={{
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(53),
						alignItems: 'center',
						justifyContent: 'center',
						borderBottomColor: 'rgba(0, 0, 0, 0.1)',
						borderBottomWidth: 1,
					}}>
						<Text style={{
							fontFamily: 'SFProRounded-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#141414',
							opacity: 0.86,
						}}
						allowFontScaling={false}>
							Подтвердите номер
						</Text>
						<TouchableOpacity style={{
							position: 'absolute',
							left: Common.getLengthByIPhone7(25),
						}}
						onPress={() => {
							navigation.goBack();
						}}>
							<Image source={require('./../../assets/ic-arrow-back.png')}
								style={{
									width: Common.getLengthByIPhone7(20),
									height: Common.getLengthByIPhone7(20),
								}}
							/>
						</TouchableOpacity>
					</View>
					<View style={{
						width: Common.getLengthByIPhone7(0),
						paddingLeft: Common.getLengthByIPhone7(25),
						marginTop: Common.getLengthByIPhone7(34),
					}}>
						<Text style={{
							fontFamily: 'SFProRounded-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#141414',
							opacity: 0.86,
						}}
						allowFontScaling={false}>
							Введите код, отправленный на {route.params?.format}
						</Text>
					</View>
					<CodeField
						ref={ref}
						{...props}
						value={value}
						autoFocus={true}
						onChangeText={text => {
							setValue(text);
						}}
						cellCount={CELL_COUNT}
						rootStyle={{
							height: CELL_HEIGHT,
							marginTop: Common.getLengthByIPhone7(33),
							paddingHorizontal: Common.getLengthByIPhone7(24),
							justifyContent: 'center',
						}}
						keyboardType="number-pad"
						textContentType="oneTimeCode"
						renderCell={renderCell}
					/>
					<View style={{
						width: Common.getLengthByIPhone7(0),
						paddingLeft: Common.getLengthByIPhone7(25),
						marginTop: Common.getLengthByIPhone7(26),
						flexDirection: 'row',
						alignItems: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProRounded-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#141414',
							opacity: 0.86,
						}}
						allowFontScaling={false}>
							SMS не пришло?
						</Text>
						<TouchableOpacity style={{
							marginLeft: 4,
						}}
						onPress={() => {
							getCode(route.params?.phone)
							.then(() => {
								Alert.alert(APP_NAME, 'СМС с кодом отправлено!');
							})
							.catch(err => {
								Alert.alert(APP_NAME, err);
							});
						}}>
							<Text style={{
								fontFamily: 'SFProRounded-Regular',
								fontSize: Common.getLengthByIPhone7(16),
								color: '#141414',
								opacity: 0.86,
								textDecorationLine: 'underline',
							}}
							allowFontScaling={false}>
								Отправить еще раз
							</Text>
						</TouchableOpacity>
					</View>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(0),
						paddingLeft: Common.getLengthByIPhone7(25),
						marginTop: Common.getLengthByIPhone7(20),
						flexDirection: 'row',
						alignItems: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProRounded-Regular',
							fontSize: Common.getLengthByIPhone7(16),
							color: '#141414',
							opacity: 0.86,
							textDecorationLine: 'underline',
						}}
						allowFontScaling={false}>
							Больше вариантов
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	sendCode: payload => dispatch.user.sendCode(payload),
	getCode: payload => dispatch.user.getCode(payload),
});

export default connect(mstp, mdtp)(CodeScreen);

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.backgroundColor,
  },
});
