import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, ScrollView, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';
import { useEvent } from 'react-native-reanimated';
import { FloatingLabelInput } from 'react-native-floating-label-input';

const ConfirmAddressScreen = ({route, rentCreateObj, setRentCreateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [street, setStreet] = React.useState('');
	const [flat, setFlat] = React.useState('');
	const [city, setCity] = React.useState('');
	const [region, setRegion] = React.useState('');
	const [index, setIndex] = React.useState('');
	const [country, setCountry] = React.useState('');
	const [geo, setGeo] = React.useState(null);

	useEffect(() => {
		console.warn(route.params);
		if (route?.params?.data) {
			setStreet(route.params.data.data.street_with_type + ' ' + route.params.data.data.house_type + ' ' + route.params.data.data.house);
			setCity(route.params.data.data.city_with_type);
			setIndex(route.params.data.data.postal_code);
			setRegion(route.params.data.data.region_with_type);
			setCountry(route.params.data.data.country);
			setGeo([parseFloat(route.params.data.data.geo_lat), parseFloat(route.params.data.data.geo_lon)]);
		}
	}, []);

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
		<ImageBackground
			source={require('./../../assets/ic-create2.png')}
			style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				resizeMode: 'cover',
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height - Common.getLengthByIPhone7(60),
				borderTopLeftRadius: Common.getLengthByIPhone7(20),
				borderTopRightRadius: Common.getLengthByIPhone7(20),
				backgroundColor: 'white',
				alignItems: 'center',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(80),
					alignItems: 'center',
					justifyContent: 'center',
				}}>
					<TouchableOpacity style={{
						position: 'absolute',
						left: Common.getLengthByIPhone7(20),
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Image
							source={require('./../../assets/ic-arrow-back.png')}
							style={{
								width: Common.getLengthByIPhone7(16),
								height: Common.getLengthByIPhone7(16),
							}}
						/>
					</TouchableOpacity>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: '#000000',
					}}>
						Подтвердите адрес
					</Text>
				</View>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					// height: Common.getLengthByIPhone7(46),
					borderRadius: Common.getLengthByIPhone7(8),
					borderColor: '#C4C4C4',
					borderWidth: 1,
					overflow: 'hidden',
				}}>
					<FloatingLabelInput
						label={'Улица'}
						value={street}
						onChangeText={value => setStreet(value)}
						// mask="9 (999) 999-99-99"
						// keyboardType={'phone-pad'}
						containerStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(60),
							paddingHorizontal: Common.getLengthByIPhone7(17),
							backgroundColor: 'white',
							// borderColor: 'blue',
							borderTopLeftRadius: Common.getLengthByIPhone7(8),
							borderTopRightRadius: Common.getLengthByIPhone7(8),
							borderBottomColor: '#C4C4C4',
							borderBottomWidth: 1,
						}}
						customLabelStyles={{
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(10),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.62,
						}}
						labelStyles={{
							color: '#676767',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
						}}
						inputStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.8,
						}}
					/>
					<FloatingLabelInput
						label={'Квартира (необязательно)'}
						value={flat}
						onChangeText={value => setFlat(value)}
						// mask="9 (999) 999-99-99"
						// keyboardType={'phone-pad'}
						containerStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(60),
							paddingHorizontal: Common.getLengthByIPhone7(17),
							backgroundColor: 'white',
							borderBottomColor: '#C4C4C4',
							borderBottomWidth: 1,
						}}
						customLabelStyles={{
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(10),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.62,
						}}
						labelStyles={{
							color: '#676767',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
						}}
						inputStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.8,
						}}
					/>
					<FloatingLabelInput
						label={'Город'}
						value={city}
						onChangeText={value => setCity(value)}
						// mask="9 (999) 999-99-99"
						// keyboardType={'phone-pad'}
						containerStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(60),
							paddingHorizontal: Common.getLengthByIPhone7(17),
							backgroundColor: 'white',
							borderBottomColor: '#C4C4C4',
							borderBottomWidth: 1,
						}}
						customLabelStyles={{
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(10),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.62,
						}}
						labelStyles={{
							color: '#676767',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
						}}
						inputStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.8,
						}}
					/>
					<View style={{
						flexDirection: 'row',
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						alignItems: 'center',
						justifyContent: 'space-between',
					}}>
						<View style={{
							width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
						}}>
							<FloatingLabelInput
								label={'Регион'}
								value={region}
								onChangeText={value => setRegion(value)}
								containerStyles={{
									width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
									height: Common.getLengthByIPhone7(60),
									paddingHorizontal: Common.getLengthByIPhone7(17),
									backgroundColor: 'white',
									borderBottomColor: '#C4C4C4',
									borderBottomWidth: 1,
									borderRightColor: '#C4C4C4',
									borderRightWidth: 1,
								}}
								customLabelStyles={{
									color: '#141414',
									fontSize: Common.getLengthByIPhone7(10),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
									opacity: 0.62,
								}}
								labelStyles={{
									color: '#676767',
									fontSize: Common.getLengthByIPhone7(16),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
								}}
								inputStyles={{
									width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
									color: '#141414',
									fontSize: Common.getLengthByIPhone7(16),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
									opacity: 0.8,
								}}
							/>
						</View>
						<View style={{
							width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
						}}>
							<FloatingLabelInput
								label={'Индекс'}
								value={index}
								onChangeText={value => setIndex(value)}
								containerStyles={{
									width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
									height: Common.getLengthByIPhone7(60),
									paddingHorizontal: Common.getLengthByIPhone7(17),
									backgroundColor: 'white',
									borderBottomColor: '#C4C4C4',
									borderBottomWidth: 1,
								}}
								customLabelStyles={{
									color: '#141414',
									fontSize: Common.getLengthByIPhone7(10),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
									opacity: 0.62,
								}}
								labelStyles={{
									color: '#676767',
									fontSize: Common.getLengthByIPhone7(16),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
								}}
								inputStyles={{
									width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)) / 2,
									color: '#141414',
									fontSize: Common.getLengthByIPhone7(16),
									fontFamily: 'SFProRounded-Regular',
									fontWeight: '400',
									opacity: 0.8,
								}}
							/>
						</View>
					</View>
					<FloatingLabelInput
						label={'Страна'}
						value={country}
						onChangeText={value => setCountry(value)}
						// mask="9 (999) 999-99-99"
						// keyboardType={'phone-pad'}
						containerStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(60),
							paddingHorizontal: Common.getLengthByIPhone7(17),
							backgroundColor: 'white',
						}}
						customLabelStyles={{
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(10),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.62,
						}}
						labelStyles={{
							color: '#676767',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
						}}
						inputStyles={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							color: '#141414',
							fontSize: Common.getLengthByIPhone7(16),
							fontFamily: 'SFProRounded-Regular',
							fontWeight: '400',
							opacity: 0.8,
						}}
					/>
				</View>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(46),
					borderRadius: Common.getLengthByIPhone7(8),
					backgroundColor: 'black',
					alignItems: 'center',
					justifyContent: 'center',
					position: 'absolute',
					bottom: Common.getLengthByIPhone7(51),
				}}
				onPress={() => {
					let obj = rentCreateObj;
					obj.appartament_number = flat;
					obj.country = country;
					obj.zip_number = index;
					obj.street = street;
					obj.region = region;
					obj.geo = geo;
					setRentCreateObj(obj);
					console.warn(obj);
					navigation.navigate('Guests');
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
						color: 'white',
					}}>
						Всё верно
					</Text>
				</TouchableOpacity>
			</View>
		</ImageBackground>
    </View>
  );
};

const mstp = (state: RootState) => ({
	rentCreateObj: state.user.rentCreateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentCreateObj: payload => dispatch.user.setRentCreateObj(payload),
});

export default connect(mstp, mdtp)(ConfirmAddressScreen);