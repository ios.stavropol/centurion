import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const Create1Screen = ({getCategoryList, categoryList}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	useEffect(() => {
		getCategoryList()
		.then(resp => {

		})
		.catch(err => {
			
		});
	}, []);

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
		<ImageBackground
			source={require('./../../assets/ic-create1.png')}
			style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				resizeMode: 'cover',
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<TouchableOpacity style={{
				position: 'absolute',
				left: Common.getLengthByIPhone7(30),
				top: Common.getLengthByIPhone7(50),
			}}
			onPress={() => {
				navigation.goBack();
			}}>
				<Image
					source={require('./../../assets/ic-close.png')}
					style={{
						width: Common.getLengthByIPhone7(30),
						height: Common.getLengthByIPhone7(30),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				borderTopLeftRadius: Common.getLengthByIPhone7(20),
				borderTopRightRadius: Common.getLengthByIPhone7(20),
				backgroundColor: 'black',
				paddingLeft: Common.getLengthByIPhone7(20),
				paddingRight: Common.getLengthByIPhone7(20),
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Bold',
					fontSize: Common.getLengthByIPhone7(26),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(16),
					marginTop: Common.getLengthByIPhone7(42),
				}}>
					{`Станьте хозяином\nвсего за 10 шагов`}
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(16),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(140),
				}}>
					{`Присоединяйтесь. Мы с радостью\nпоможем`}
				</Text>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: Common.getLengthByIPhone7(46),
					borderRadius: Common.getLengthByIPhone7(8),
					backgroundColor: 'white',
					alignItems: 'center',
					justifyContent: 'center',
					marginBottom: Common.getLengthByIPhone7(50),
				}}
				onPress={() => {
					navigation.navigate('Create2');
					// navigation.navigate('SetName');
				}}>
					<Text style={{
						fontFamily: 'SFProDisplay-Bold',
						fontSize: Common.getLengthByIPhone7(14),
						color: 'black',
					}}>
						Вперед
					</Text>
				</TouchableOpacity>
			</View>
		</ImageBackground>
    </View>
  );
};

const mstp = (state: RootState) => ({
	categoryList: state.user.categoryList,
});

const mdtp = (dispatch: Dispatch) => ({
	getCategoryList: () => dispatch.user.getCategoryList(),
});

export default connect(mstp, mdtp)(Create1Screen);