import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import GradientButton from '../../components/GradientButton';
import { BASE_URL, APP_NAME } from './../../constants';
import { useEvent } from 'react-native-reanimated';

const Create5Screen = ({route, setRentCreateObj, rentCreateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [selected, setSelected] = React.useState(null);
	const [data, setData] = React.useState(null);

	useEffect(() => {
		render();
	}, []);

	const render = () => {
		let array = [];

		for (let i = 0; i < route.params.data.items.length; i++) {
			array.push(renderView(route.params.data.items[i].name, BASE_URL + route.params.data.items[i].cover, route.params.data.items[i].id, {
				marginBottom: i + 1 === route.params.data.items.length ? Common.getLengthByIPhone7(40) : Common.getLengthByIPhone7(10),
				marginTop: i === 0 ? Common.getLengthByIPhone7(30) : 0,
			}, () => {
				setData(route.params.data.items[i]);
				setSelected(route.params.data.items[i].id);
			}));
		}
		setBody(array);
	}


	useEffect(() => {
		render();
	}, [selected]);

	const renderView = (title, image, index, style, action) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(60),
			borderRadius: Common.getLengthByIPhone7(10),
			paddingLeft: Common.getLengthByIPhone7(15),
			paddingRight: Common.getLengthByIPhone7(10),
			borderColor: selected == index ? 'black' : '#CBCBCB',
			borderWidth: 1,
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}
		onPress={() => {
			if (action) {
				action();
			}
		}}>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				color: 'black',
			}}>
				{title}
			</Text>
			<View style={{
				width: Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(5),
				overflow: 'hidden',
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: '#FFD441',
			}}>
				<Image
					source={{uri: image}}
					style={{
						width: Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(40),
						resizeMode: 'cover',
					}}
				/>
			</View>
		</TouchableOpacity>);
	}

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
		<ImageBackground
			source={{uri: BASE_URL + route.params.data.cover}}
			style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				resizeMode: 'cover',
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<TouchableOpacity style={{
				position: 'absolute',
				left: Common.getLengthByIPhone7(30),
				top: Common.getLengthByIPhone7(50),
			}}
			onPress={() => {
				navigation.goBack();
			}}>
				<Image
					source={require('./../../assets/ic-close.png')}
					style={{
						width: Common.getLengthByIPhone7(30),
						height: Common.getLengthByIPhone7(30),
						resizeMode: 'contain',
					}}
				/>
			</TouchableOpacity>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontFamily: 'SFProDisplay-Bold',
				fontSize: Common.getLengthByIPhone7(26),
				color: 'white',
				marginBottom: Common.getLengthByIPhone7(23),
			}}>
				{`Что вы предлагаете?`}
			</Text>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				borderTopLeftRadius: Common.getLengthByIPhone7(20),
				borderTopRightRadius: Common.getLengthByIPhone7(20),
				backgroundColor: 'white',
				paddingLeft: Common.getLengthByIPhone7(20),
				paddingRight: Common.getLengthByIPhone7(20),
			}}>
				{body}
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					marginLeft: -Common.getLengthByIPhone7(20),
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						if (selected !== null) {
							let obj = rentCreateObj;
							obj.categories.push(selected);
							setRentCreateObj(obj);
							navigation.navigate('SearchAddress');
						} else {
							Alert.alert(APP_NAME, 'Выберите категорию');
						}
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		</ImageBackground>
    </View>
  );
};

const mstp = (state: RootState) => ({
	rentCreateObj: state.user.rentCreateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentCreateObj: payload => dispatch.user.setRentCreateObj(payload),
});

export default connect(mstp, mdtp)(Create5Screen);