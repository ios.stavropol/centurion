import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import TextInputView from '../../components/EditProfile/TextInputView';
import SexInputView from '../../components/EditProfile/SexInputView';
import MailView from '../../components/EditProfile/MailView';
import PasportView from '../../components/EditProfile/PasportView';
import ContactView from '../../components/EditProfile/ContactView';
import PhoneView from '../../components/EditProfile/PhoneView';
import GradientButton from '../../components/GradientButton';
import { APP_NAME } from '../../constants';
import prompt from 'react-native-prompt-android';

const EditProfileScreen = ({editProfile, userProfile}) => {

	const navigation = useNavigation();
	const [firstname, setFirstname] = React.useState('');
	const [lastname, setLastname] = React.useState('');
	const [sex, setSex] = React.useState(null);
	const [email, setEmail] = React.useState('');
	const [phone, setPhone] = React.useState('');

	useEffect(() => {
		if (userProfile) {
			setFirstname(userProfile.first_name);
			setLastname(userProfile.last_name);
			setSex(userProfile.sex);
			setEmail(userProfile.email);
			setPhone(userProfile.phone);
		}
	}, []);

	useEffect(() => {
		if (userProfile) {
			setFirstname(userProfile.first_name);
			setLastname(userProfile.last_name);
			setSex(userProfile.sex);
			setEmail(userProfile.email);
			setPhone(userProfile.phone);
		}
	}, [userProfile]);

	const saveProfile = () => {
		if (firstname?.length && lastname?.length) {
			editProfile({
				first_name: firstname,
				last_name: lastname,
				sex: sex,
				email: userProfile.email,
			})
			.then(() => {
				Alert.alert(APP_NAME, 'Данные успешно сохранены!');
			})
			.catch(err => {
				Alert.alert(APP_NAME, err);
			});
		} else {
			Alert.alert(APP_NAME, 'Введите ФИО');
		}
	}

	return (
		<View style={{
		flex: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'flex-start',
		}}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				marginBottom: Common.getLengthByIPhone7(100),
				flex: 1,
			}}
			contentContainerStyle={{
				alignItems: 'center',
				justifyContent: 'flex-start'
			}}>
				<Text style={{
					marginTop: Common.getLengthByIPhone7(20),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(22),
					color: '#222222',
				}}>
					{`Измените личную\nинформацию`}
				</Text>
				<TextInputView
					style={{
						marginTop: Common.getLengthByIPhone7(30),
					}}
					title={'Имя'}
					value={firstname}
					onChange={text => {
						setFirstname(text);
					}}
				/>
				<TextInputView
					style={{
						marginTop: Common.getLengthByIPhone7(15),
					}}
					title={'Фамилия'}
					value={lastname}
					onChange={text => {
						setLastname(text);
					}}
				/>
				<SexInputView
					style={{
						marginTop: Common.getLengthByIPhone7(15),
					}}
					title={'Пол'}
					value={sex}
					onChange={text => {
						setSex(text);
					}}
				/>
				<GradientButton
					title={'Сохранить'}
					style={{
						marginTop: Common.getLengthByIPhone7(10),
					}}
					onPress={() => {
						saveProfile();
					}}
				/>
				<MailView
					style={{
						marginTop: Common.getLengthByIPhone7(20),
					}}
					value={email}
					onChange={() => {
						prompt(
							'Введите почту',
							'',
							[
							 {text: 'Отмена', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
							 {text: 'Сохранить', onPress: text => {
								if (text?.length) {
									editProfile({
										first_name: userProfile.first_name,
										last_name: userProfile.last_name,
										sex: userProfile.sex,
										email: text,
									})
									.then(() => {
										Alert.alert(APP_NAME, 'Данные успешно сохранены!');
									})
									.catch(err => {
										Alert.alert(APP_NAME, err);
									});
								} else {
									Alert.alert(APP_NAME, 'Введите почту');
								}
							 }},
							],
							{
								// type: 'email-address',
								cancelable: false,
								defaultValue: email,
								placeholder: 'E-mail',
							}
						);
					}}
				/>
				<PhoneView
					style={{
						borderTopWidth: 0,
					}}
					value={phone}
					onChange={text => {
						setPhone(text);
					}}
				/>
				<PasportView
					style={{
						borderTopWidth: 0,
					}}
					value={lastname}
					onChange={text => {
						setLastname(text);
					}}
				/>
				<ContactView
					style={{
						borderTopWidth: 0,
					}}
					value={lastname}
					onChange={text => {
						setLastname(text);
					}}
				/>
			</ScrollView>
		</View>
	);
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	editProfile: payload => dispatch.user.editProfile(payload),
});

export default connect(mstp, mdtp)(EditProfileScreen);