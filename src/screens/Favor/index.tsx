import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../constants';

const FavorScreen = ({getFavors}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
  const [data, setData] = React.useState([]);

  useEffect(() => {
    getFavor();
  }, []);

  const getFavor = () => {
    getFavors()
    .then(res => {
      setData(res);
    })
    .catch(err => {

    });
  }

  useEffect(() => {
    if (data) {
      let array = [];

      for (let i = 0; i < data.length; i++) {
        array.push(renderView(data[i]));
      }

      setBody(array);
    }
  }, [data]);

  const renderView = (data) => {
    return (<TouchableOpacity style={{
      width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
      paddingTop: Common.getLengthByIPhone7(10),
      paddingBottom: Common.getLengthByIPhone7(10),
      flexDirection: 'row',
      alignItems: 'center',
    }}
    onPress={() => {
      navigation.navigate('Appartament', {data: data});
    }}>
      <View style={{
        width: Common.getLengthByIPhone7(67),
        height: Common.getLengthByIPhone7(67),
        borderRadius: Common.getLengthByIPhone7(10),
        backgroundColor: '#c4c4c4',
        overflow: 'hidden',
      }}>
        <Image
          source={{uri: BASE_URL + data?.cover?.photo}}
          style={{
            width: Common.getLengthByIPhone7(67),
            height: Common.getLengthByIPhone7(67),
          }}
        />
      </View>
      <View style={{
        marginLeft: Common.getLengthByIPhone7(20),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(140),
      }}>
        <Text style={{
          marginBottom: Common.getLengthByIPhone7(7),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(140),
          fontFamily: 'SFProDisplay-Regular',
          fontSize: Common.getLengthByIPhone7(16),
          color: 'rgba(20, 20, 20, 0.86)',
        }}>
          {data?.name}
        </Text>
      </View>
    </TouchableOpacity>);
  }

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
      <Text style={{
        marginTop: Common.getLengthByIPhone7(90),
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        fontFamily: 'SFProDisplay-Medium',
        fontSize: Common.getLengthByIPhone7(30),
      }}>
        Избранное
      </Text>
      <ScrollView style={{
        marginTop: Common.getLengthByIPhone7(20),
        width: Common.getLengthByIPhone7(0),
        flex: 1,
        marginBottom: Common.getLengthByIPhone7(100),
      }}
      contentContainerStyle={{
        alignItems: 'center',
      }}>
        {body}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	getFavors: () => dispatch.user.getFavors(),
});

export default connect(mstp, mdtp)(FavorScreen);