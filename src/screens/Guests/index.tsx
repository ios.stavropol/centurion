import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, ScrollView, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from './../../constants';
import SwitchView from '../../components/Guests/SwitchView';

const GuestsScreen = ({route, rentCreateObj, setRentCreateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [guests, setGuests] = React.useState(0);
	const [beds, setBeds] = React.useState(0);
	const [rooms, setRooms] = React.useState(0);
	const [bathrooms, setBathrooms] = React.useState(0);

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
		<ImageBackground
			source={require('./../../assets/ic-guests.png')}
			style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				resizeMode: 'cover',
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontFamily: 'SFProDisplay-Medium',
				fontSize: Common.getLengthByIPhone7(28),
				color: 'white',
				marginBottom: Common.getLengthByIPhone7(40),
				textAlign: 'left',
			}}>
				{`Сколько гостей вы\nготовы принять?`}
			</Text>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(430),
				borderTopLeftRadius: Common.getLengthByIPhone7(20),
				borderTopRightRadius: Common.getLengthByIPhone7(20),
				backgroundColor: 'white',
				alignItems: 'center',
			}}>
				<SwitchView
					title={'Гости'}
					style={{
						marginTop: Common.getLengthByIPhone7(30),
					}}
					onChange={value => {
						setGuests(value);
					}}
				/>
				<SwitchView
					title={'Кровати'}
					onChange={value => {
						setBeds(value);
					}}
				/>
				<SwitchView
					title={'Спальни'}
					onChange={value => {
						setRooms(value);
					}}
				/>
				<SwitchView
					title={'Ванные'}
					onChange={value => {
						setBathrooms(value);
					}}
				/>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
					// marginLeft: -Common.getLengthByIPhone7(20),
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						let obj = rentCreateObj;
						obj.guest = guests;
						obj.beds = beds;
						obj.rooms = rooms;
						obj.bathrooms = bathrooms;
						setRentCreateObj(obj);
						navigation.navigate('Advantages');
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		</ImageBackground>
    </View>
  );
};

const mstp = (state: RootState) => ({
	rentCreateObj: state.user.rentCreateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentCreateObj: payload => dispatch.user.setRentCreateObj(payload),
});

export default connect(mstp, mdtp)(GuestsScreen);