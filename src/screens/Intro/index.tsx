import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import {CircularProgress} from 'react-native-svg-circular-progress'

const { width } = Dimensions.get('window');

const IntroScreen = ({}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	scrollX = new Animated.Value(0);

	const [body, setBody] = React.useState(null);

	useEffect(() => {
		let page = [];
		let pages = [];
		let position = Animated.divide(scrollX, width);

		page.push(renderPage1());
		page.push(renderPage2());
		page.push(renderPage3());
		page.push(renderPage4());

		pages.push('1');
		pages.push('2');
		pages.push('3');
		pages.push('4');

		setBody(<View style={{
			width: Common.getLengthByIPhone7(0),
			height: Dimensions.get('screen').height,
			alignItems: 'center',
			// backgroundColor: 'red'
		}}>
			<ScrollView style={{
			  width: Common.getLengthByIPhone7(0),
			  height: Dimensions.get('screen').height,
			}}
			horizontal={true}
			pagingEnabled={true}
			showsHorizontalScrollIndicator={false}
			// onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }])}
			// onScroll={event => {
			// 	scrollX.setOffset(event.nativeEvent.contentOffset.x);
			// }}
			onScroll={Animated.event(
				[{ nativeEvent: { contentOffset: { x: scrollX } } }],
				{
					listener: event => {
						let page = Math.round(parseFloat(event.nativeEvent.contentOffset.x/Dimensions.get('window').width));
						setPage(page);
					},
				},)
			}
			scrollEventThrottle={16}
			// ref={el => scroll = el}
			bounces={false}>
			  {page}
			</ScrollView>
			<View
			  style={{
				flexDirection: 'row',
				position: 'absolute',
				// bottom: Common.getLengthByIPhone7(33)+hh,
				bottom: Common.getLengthByIPhone7(42),
			  }}
			>
			  {pages.map((_, i) => {
				let backgroundColor = position.interpolate({
				  inputRange: [i - 1, i, i + 1],
				  outputRange: ['rgba(255, 255, 255, 0.1)', 'white', 'rgba(255, 255, 255, 0.1)'],
				  extrapolate: 'clamp'
				});
  
				return (
				  <Animated.View
					key={i}
					style={{
					  backgroundColor,
					  height: Common.getLengthByIPhone7(6),
					  width: Common.getLengthByIPhone7(6),
					  marginLeft: Common.getLengthByIPhone7(3),
					  marginRight: Common.getLengthByIPhone7(3),
					  borderRadius: Common.getLengthByIPhone7(3),
					}}
				  />
				);
			  })}
			</View>
			<View style={{
				position: 'absolute',
				bottom: Common.getLengthByIPhone7(42),
				right: Common.getLengthByIPhone7(20),
			}}>
				<CircularProgress
					percentage={100}
					progressWidth={Common.getLengthByIPhone7(22)}
					size={Common.getLengthByIPhone7(51)}
					fillColor={page === 2 ? 'yellow' : 'black'}
					blankColor={'rgba(255, 255, 255, 0.1)'}
					donutColor={'white'}
				>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(35),
						width: Common.getLengthByIPhone7(35),
						borderRadius: Common.getLengthByIPhone7(35) / 2,
						backgroundColor: 'white',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.navigate('LoginIn');
					}}>
						<Image source={require('./../../assets/ic-next.png')}
							style={{
								height: Common.getLengthByIPhone7(10),
								width: Common.getLengthByIPhone7(5),
								resizeMode: 'contain',
							}}
						/>
					</TouchableOpacity>
				</CircularProgress>
			</View>
		</View>);
	}, []);

	const renderPage1 = () => {
		return (<View style={{
		  width: Common.getLengthByIPhone7(0),
		  height: Dimensions.get('screen').height,
		  alignItems: 'center',
		  justifyContent: 'flex-end',
		}}>
			<Image source={require('./../../assets/ic-intro1.png')} style={{
				resizeMode: 'cover',
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height,
			}} />
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				alignItems: 'flex-start',
				position: 'absolute',
				bottom: Common.getLengthByIPhone7(106),
			}}>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'Montserrat',
					fontWeight: '700',
					fontSize: Common.getLengthByIPhone7(24),
				}}
				allowFontScaling={false}>
					{`Добро пожаловать\nв мир Centurion`.toUpperCase()}
				</Text>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'SFProDisplay-Regular',
					textAlign: 'left',
					fontSize: Common.getLengthByIPhone7(14),
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{`В нашем инновационом сервисе\nCENTURION, тут вы сможете в пару\nкликов арендовать частное жилье,\nавто, яхты, самолеты и найти крутую\nвечеринку`}
				</Text>
			</View>
		</View>);
	}

	const renderPage2 = () => {
		return (<View style={{
		  width: Common.getLengthByIPhone7(0),
		  height: Dimensions.get('screen').height,
		  alignItems: 'center',
		  justifyContent: 'flex-end',
		}}>
			<Image source={require('./../../assets/ic-intro2.png')} style={{
				resizeMode: 'cover',
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height,
			}} />
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				alignItems: 'flex-start',
				position: 'absolute',
				bottom: Common.getLengthByIPhone7(106),
			}}>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'Montserrat',
					fontWeight: '700',
					fontSize: Common.getLengthByIPhone7(24),
				}}
				allowFontScaling={false}>
					{`Путешествуйте`.toUpperCase()}
				</Text>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'SFProDisplay-Regular',
					textAlign: 'left',
					fontSize: Common.getLengthByIPhone7(14),
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{`Вы можете забронировать ЧТО\nугодно, ГДЕ угодно и КОГДА угодно,\nа мы позаботимся чтобы Ваш отдых\nпрошел комфортно`}
				</Text>
			</View>
		</View>);
	}

	const renderPage3 = () => {
		return (<View style={{
		  width: Common.getLengthByIPhone7(0),
		  height: Dimensions.get('screen').height,
		  alignItems: 'center',
		  justifyContent: 'flex-end',
		}}>
			<Image source={require('./../../assets/ic-intro3.png')} style={{
				resizeMode: 'cover',
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height,
			}} />
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				alignItems: 'center',
				position: 'absolute',
				top: Common.getLengthByIPhone7(88),
			}}>
				<Text style={{
					color: '#262626',
					fontFamily: 'Montserrat',
					fontWeight: '700',
					fontSize: Common.getLengthByIPhone7(34),
				}}
				allowFontScaling={false}>
					{`ПРОСТО`.toUpperCase()}
				</Text>
				<Text style={{
					color: '#262626',
					fontFamily: 'SFProDisplay-Regular',
					textAlign: 'center',
					fontSize: Common.getLengthByIPhone7(14),
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{`Вводите место, дату своего\nпутешествия и выбираете, какой\nраздел из ниже предствленных вам\nинтересен для бронирования`}
				</Text>
			</View>
		</View>);
	}

	const renderPage4 = () => {
		return (<View style={{
		  width: Common.getLengthByIPhone7(0),
		  height: Dimensions.get('screen').height,
		  alignItems: 'center',
		  justifyContent: 'flex-end',
		}}>
			<Image source={require('./../../assets/ic-intro4.png')} style={{
				resizeMode: 'cover',
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height,
			}} />
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				alignItems: 'flex-start',
				position: 'absolute',
				bottom: Common.getLengthByIPhone7(106),
			}}>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'Montserrat',
					fontWeight: '700',
					fontSize: Common.getLengthByIPhone7(24),
				}}
				allowFontScaling={false}>
					{`А еще...`.toUpperCase()}
				</Text>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'Montserrat',
					fontWeight: '700',
					fontSize: Common.getLengthByIPhone7(24),
					marginTop: Common.getLengthByIPhone7(120),
				}}
				allowFontScaling={false}>
					{`тут Вы можете разместить свою недвижимость`.toUpperCase()}
				</Text>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'SFProDisplay-Regular',
					textAlign: 'left',
					fontSize: Common.getLengthByIPhone7(14),
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					{`А также свое авто, яхту или\nустроить пятничную вечеринку`}
				</Text>
				<Text style={{
					color: colors.WHITE_COLOR,
					fontFamily: 'SFProDisplay-Regular',
					textAlign: 'left',
					fontSize: Common.getLengthByIPhone7(14),
					marginTop: 3,
				}}
				allowFontScaling={false}>
					{`И зарабатывать на этом`.toUpperCase()}
				</Text>
			</View>
		</View>);
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'flex-start',
			justifyContent: 'flex-start',
        }}>
			{body}
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(IntroScreen);