import React, { useCallback, useEffect, useRef, useState } from 'react';
import { StyleSheet, StatusBar, View, Image, TouchableOpacity, Text, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { StackNavigationProp } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import CountryPicker from 'react-native-country-picker-modal';
import {APP_NAME} from './../../constants';

const LoginScreen = ({getCode}) => {

  const navigation = useNavigation();
  const [cont, setCont] = useState('7');
  const [countryName, setCountryName] = useState('Россия');
  const [visible, setVisible] = useState(false);

  const [countryCode, setCountryCode] = useState<CountryCode>('RU')
  const [country, setCountry] = useState<Country>(null)
  const [withCountryNameButton, setWithCountryNameButton] = useState<boolean>(
    false,
  )
  const [withFlag, setWithFlag] = useState<boolean>(true)
  const [withEmoji, setWithEmoji] = useState<boolean>(true)
  const [withFilter, setWithFilter] = useState<boolean>(true)
  const [withAlphaFilter, setWithAlphaFilter] = useState<boolean>(true)
  const [withCallingCode, setWithCallingCode] = useState<boolean>(true)
//   const [withCallingCode, setWithCallingCode] = useState<boolean>(true)
  const onSelect = (country: Country) => {
	console.warn(country);
	setCountryName(country.name);
    setCountryCode(country.cca2);
    setCountry(country);
	setVisible(false);
	if (country.callingCode?.length) {
		setCont(country.callingCode[0]);
	}
  }

  useEffect(() => {
    // navigation.navigate('LoginIn');
  }, []);

  const renderButton = (title, icon, style, textStyle, imageStyle, action) => {
	  return (<TouchableOpacity style={[{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
		height: Common.getLengthByIPhone7(44),
		borderRadius: Common.getLengthByIPhone7(9),
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
	}, style]}
	onPress={() => {
		if (action) {
			action();
		}
	}}>
		<Image source={icon}
			style={[{
				position: 'absolute',
				left: Common.getLengthByIPhone7(16),
				width: Common.getLengthByIPhone7(24),
				height: Common.getLengthByIPhone7(24),
				resizeMode: 'contain',
			}, imageStyle]}
		/>
		<Text style={[{
			color: 'white',
			fontSize: Common.getLengthByIPhone7(13),
			fontFamily: 'SFProDisplay-Regular',
			fontWeight: '400',
			letterSpacing: 0.08,
		}, textStyle]}
		allowFontScaling={false}>
			{title}
		</Text>
	</TouchableOpacity>);
  }

	const next = () => {
		let phone = cont.replace(/[^\d]/g, '');
		if (phone.length) {
			getCode(phone)
			.then(() => {
				navigation.navigate('Code', {phone: phone, format: cont});
			})
			.catch(err => {
				Alert.alert(APP_NAME, err);
			});
		} else {
			Alert.alert(APP_NAME, 'Введите номер телефона!');
		}
	}

  return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
        }}>
			<Text style={{
				color: '#1A1E25',
				marginTop: Common.getLengthByIPhone7(55),
				marginBottom: Common.getLengthByIPhone7(30),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontSize: Common.getLengthByIPhone7(16),
				lineHeight: Common.getLengthByIPhone7(32),
				fontFamily: 'SFProDisplay-Bold',
			}}
			allowFontScaling={false}>
				Войдите или зарегистрируйтесь
			</Text>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(44),
				paddingHorizontal: Common.getLengthByIPhone7(17),
				backgroundColor: '#eeeeee',
				borderColor: 'blue',
				borderRadius: Common.getLengthByIPhone7(8),
				justifyContent: 'flex-start',
				alignItems: 'center',
				flexDirection: 'row',
			}}
			activeOpacity={1}
			onPress={() => {
				setVisible(true);
			}}>
				<CountryPicker
					{...{
						countryCode,
						withFilter,
						withFlag,
						withCountryNameButton,
						withAlphaFilter,
						withCallingCode,
						withEmoji,
						onSelect,
					}}
					translation={'rus'}
					filterProps={{
						placeholder: 'Введите название страны'
					}}
					visible={visible}
				/>
				<Text style={{
					color: '#141414',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'SFProRounded-Regular',
					fontWeight: '400',
					opacity: 0.8,
				}}>
					{countryName}
				</Text>
			</TouchableOpacity>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				marginTop: Common.getLengthByIPhone7(7),
			}}>
				<FloatingLabelInput
					label={'Номер телефона'}
					value={cont}
					onChangeText={value => setCont(value)}
					mask="9 (999) 999-99-99"
					keyboardType={'phone-pad'}
					containerStyles={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(44),
						paddingHorizontal: Common.getLengthByIPhone7(17),
						backgroundColor: '#eeeeee',
						borderColor: 'blue',
						borderRadius: Common.getLengthByIPhone7(8),
					}}
					customLabelStyles={{
						color: '#141414',
						fontSize: Common.getLengthByIPhone7(10),
						fontFamily: 'SFProRounded-Regular',
						fontWeight: '400',
						opacity: 0.62,
					}}
					labelStyles={{
						color: '#676767',
						fontSize: Common.getLengthByIPhone7(16),
						fontFamily: 'SFProRounded-Regular',
						fontWeight: '400',
					}}
					inputStyles={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						color: '#141414',
						fontSize: Common.getLengthByIPhone7(16),
						fontFamily: 'SFProRounded-Regular',
						fontWeight: '400',
						opacity: 0.8,
					}}
				/>
			</View>
			<Text style={{
				color: '#1A1E25',
				marginTop: Common.getLengthByIPhone7(14),
				marginBottom: Common.getLengthByIPhone7(22),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				fontSize: Common.getLengthByIPhone7(9),
				fontFamily: 'SFProDisplay-Regular',
				fontWeight: '400',
			}}
			allowFontScaling={false}>
				{`Мы позвоним вам или отправим SMS, чтобы\nподтвердить правильность введенных вами данных`}
			</Text>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(44),
				borderRadius: Common.getLengthByIPhone7(9),
				backgroundColor: '#262626',
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				next();
			}}>
				<Text style={{
					color: 'white',
					fontSize: Common.getLengthByIPhone7(13),
					fontFamily: 'SFProDisplay-Regular',
					fontWeight: '400',
					letterSpacing: 0.08,
				}}
				allowFontScaling={false}>
					ПРОДОЛЖИТЬ
				</Text>
			</TouchableOpacity>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
				alignItems: 'center',
				justifyContent: 'center',
				marginTop: Common.getLengthByIPhone7(33),
				marginBottom: Common.getLengthByIPhone7(37),
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					height: 1,
					backgroundColor: 'rgba(0, 0, 0, 0.72)',
				}}/>
				<View style={{
					backgroundColor: '#FFC453',
					position: 'absolute',
					width: Common.getLengthByIPhone7(57),
					height: Common.getLengthByIPhone7(26),
					borderRadius: Common.getLengthByIPhone7(24),
					alignItems: 'center',
					justifyContent: 'center',
				}}>
					<Text style={{
						color: '#1A1A1A',
						fontSize: Common.getLengthByIPhone7(12),
						fontFamily: 'Inter-Medium',
						letterSpacing: 0.08,
					}}
					allowFontScaling={false}>
						ИЛИ
					</Text>
				</View>
			</View>
			{renderButton('Продолжить с Apple', require('./../../assets/ic-apple.png'), {backgroundColor: '#262626'}, {}, {}, () => {
				
			})}
			{renderButton('Продолжить с Google', require('./../../assets/ic-google.png'), {
				borderWidth: 1,
				borderColor: '#E2E8F0',
				marginTop: Common.getLengthByIPhone7(12),
			},
			{
				color: '#475569',
			},
			{},
			() => {
				
			})}
			{renderButton('С помощью эл.почты', require('./../../assets/ic-mail.png'), {
				borderWidth: 1,
				borderColor: '#E2E8F0',
				marginTop: Common.getLengthByIPhone7(12),
			},
			{
				color: '#475569',
			},
			{},
			() => {
				
			})}
			{renderButton('Продолжить через VK', require('./../../assets/ic-vk.png'), {
				borderWidth: 1,
				borderColor: '#E2E8F0',
				marginTop: Common.getLengthByIPhone7(12),
			},
			{
				color: '#475569',
			},
			{
				width: Common.getLengthByIPhone7(32),
				height: Common.getLengthByIPhone7(32),
				left: Common.getLengthByIPhone7(13),
			},
			() => {
				
			})}
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	getCode: payload => dispatch.user.getCode(payload),
});

export default connect(mstp, mdtp)(LoginScreen);

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.backgroundColor,
  },
});
