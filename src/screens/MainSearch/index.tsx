import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import MapCircleView from '../../components/Search/MapCircleView';
import { BASE_URL, APP_NAME } from '../../constants';
import FilterView from '../../components/MainSearch/FilterView';
import HouseView from '../../components/MainSearch/HouseView';

const MainSearchScreen = ({setSearchObj, searchObj, searchAdv, searchList}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);

	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['50%', '90%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	useEffect(() => {
		searchAdv(searchObj)
		.then(() => {

		}) 
		.catch(err => {

		});
	}, []);

	useEffect(() => {
		searchAdv(searchObj)
		.then(() => {

		})
		.catch(err => {

		});
	}, [searchObj]);

	useEffect(() => {
		let array = [];

		for (let i = 0; i < searchList?.length; i++) {
			array.push(<HouseView
				data={searchList[i]}
				style={{
					marginTop: i === 0 ? Common.getLengthByIPhone7(20) : 0,
				}}
				onClick={() => {
					navigation.navigate('Appartament', {data: searchList[i]});
				}}
			/>);
		}

		setBody(array);
	}, [searchList]);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<FilterView
				style={{
					marginTop: Common.getLengthByIPhone7(20),
				}}
			/>
			<MapCircleView
				style={{
					top: Common.getLengthByIPhone7(70),
				}}
			/>
			<BottomSheet
				style={{
					backgroundColor: 'white',
					shadowColor: "black",
					shadowOffset: {
						width: 0,
						height: -12,
					},
					shadowOpacity: 0.05,
					shadowRadius: 30.00,
					elevation: 4,
					borderTopLeftRadius: Common.getLengthByIPhone7(30),
					borderTopRightRadius: Common.getLengthByIPhone7(30),
				}}
				handleStyle={{
					// borderTopLeftRadius: Common.getLengthByIPhone7(30),
				}}
				// containerHeight={100}
				handleComponent={() => {
					return (<View style={{
						height: Common.getLengthByIPhone7(60),
						backgroundColor: 'white',
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
						overflow: 'hidden',
						alignItems: 'center',
						justifyContent: 'center',
					}}>
						<View style={{
							borderBottomColor: 'rgba(0, 0, 0, 0.33)',
							borderBottomWidth: 1,
							height: Common.getLengthByIPhone7(60),
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							alignItems: 'center',
							justifyContent: 'center',
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
							}}>
								{Common.getDigitStr(searchList?.length, {
									d1: ' вариант',
									d2_d4: ' варианта',
									d5_d10: ' вариантов',
									d11_d19: ' вариантов',
								})}
							</Text>
						</View>
					</View>);
				}}
				backgroundStyle={{
					
				}}
				ref={bottomSheetRef}
				index={0}
				snapPoints={snapPoints}
				onChange={handleSheetChanges}
			>
				<View style={{
					zIndex: 100,
					flex: 1,
					alignItems: 'center',
					backgroundColor: 'white',
				}}>
					<ScrollView style={{
						width: Common.getLengthByIPhone7(0),
						flex: 1,
					}}
					contentContainerStyle={{
						alignItems: 'center',
					}}>
						{body}
						<View
							style={{
								width: 200,
								height: Common.getLengthByIPhone7(100),
							}}
						/>
					</ScrollView>
				</View>
			</BottomSheet>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
	searchList: state.user.searchList,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
	searchAdv: payload => dispatch.user.searchAdv(payload),
});

export default connect(mstp, mdtp)(MainSearchScreen);