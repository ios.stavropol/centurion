import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, ImageBackground, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import {checkNotifications, requestNotifications, PERMISSIONS, RESULTS} from 'react-native-permissions';

const NotifyScreen = ({}) => {

	const navigation = useNavigation();
	const [value, setValue] = React.useState('');

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'flex-start',
			justifyContent: 'center',
			paddingLeft: Common.getLengthByIPhone7(25),
        }}>
			<Image source={require('./../../assets/ic-clock.png')}
				style={{
					width: Common.getLengthByIPhone7(65),
					height: Common.getLengthByIPhone7(65),
					// marginTop: Common.getLengthByIPhone7(110),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				color: '#1A1E25',
				marginTop: Common.getLengthByIPhone7(13),
				fontFamily: 'SFProDisplay-Bold',
				fontSize: Common.getLengthByIPhone7(30),
			}}>
				Включить уведомления?
			</Text>
			<Text style={{
				color: '#141414',
				marginTop: Common.getLengthByIPhone7(30),
				fontFamily: 'SFProRounded-Regular',
				fontSize: Common.getLengthByIPhone7(18),
			}}>
				Не пропустите важные сообщения, например информацию о прибытии и активности аккаунта
			</Text>
			<Text style={{
				color: '#141414',
				marginTop: Common.getLengthByIPhone7(23),
				fontFamily: 'SFProRounded-Regular',
				fontSize: Common.getLengthByIPhone7(18),
			}}>
				{`Получайте специальные\nпредложения,\nперсонализированные\nрекомендации и не только`}
			</Text>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(154),
				height: Common.getLengthByIPhone7(44),
				borderRadius: Common.getLengthByIPhone7(8),
				marginTop: Common.getLengthByIPhone7(56),
				backgroundColor: '#222222',
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				checkNotifications().then(({status, settings}) => {
					console.warn(status);
					if (status === RESULTS.DENIED) {
						requestNotifications(['alert', 'sound', 'badge']).then(({status, settings}) => {
							console.warn(status);
							navigation.navigate('Intro');
						});
					} else {
						navigation.navigate('Intro');
					}
				});
			}}>
				<Text style={{
					color: 'white',
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}>
					Да, сообщайте
				</Text>
			</TouchableOpacity>
			<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(136),
				height: Common.getLengthByIPhone7(44),
				borderRadius: Common.getLengthByIPhone7(8),
				marginTop: Common.getLengthByIPhone7(19),
				borderColor: '#222222',
				borderWidth: 1,
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				navigation.navigate('Intro');
			}}>
				<Text style={{
					color: '#222222',
					fontFamily: 'SFProDisplay-Regular',
					fontSize: Common.getLengthByIPhone7(14),
				}}>
					Пропустить
				</Text>
			</TouchableOpacity>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(NotifyScreen);