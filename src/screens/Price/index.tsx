import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { TextInput, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, APP_NAME } from '../../constants';

const PriceScreen = ({rentUpdateObj, setRentUpdateObj}) => {

	const navigation = useNavigation();
	const [value, setValue] = React.useState('0' + String.fromCharCode(0x20BD));
	const input = useRef(null);

	const next = () => {
		let str = value;
		str = str.replace(/[^\d]/g, '');
		if (str.length && str > 0) {
			let obj = rentUpdateObj;
			obj.price = str;
			setRentUpdateObj(obj);
			navigation.navigate('AdvEnd');
		} else {
			Alert.alert(APP_NAME, 'Введите цену!');
		}
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-fon-price.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(28),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(20),
					textAlign: 'left',
				}}>
					{`Теперь самое интересное: установите цену`}
				</Text>
				<View style={{
					backgroundColor: 'white',
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(430),
					borderTopLeftRadius: Common.getLengthByIPhone7(20),
					borderTopRightRadius: Common.getLengthByIPhone7(20),
					alignItems: 'center',
				}}>
					<View style={{
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center',
						marginTop: Common.getLengthByIPhone7(53),
					}}>
						<TouchableOpacity style={{
							width: Common.getLengthByIPhone7(30),
							height: Common.getLengthByIPhone7(30),
							borderRadius: Common.getLengthByIPhone7(15),
							alignItems: 'center',
							justifyContent: 'center',
							borderColor: 'rgba(0, 0, 0, 0.3)',
							borderWidth: 1,
						}}
						onPress={() => {
							let str = value;
							str = str.replace(/[^\d]/g, '');
							if (str > 0) {
								str--;
							}
							str = str + String.fromCharCode(0x20BD);
							setValue(str);
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(20),
								lineHeight: Common.getLengthByIPhone7(20),
							}}>
								-
							</Text>
						</TouchableOpacity>
						<TextInput
							style={{
								marginLeft: Common.getLengthByIPhone7(25),
								marginRight: Common.getLengthByIPhone7(25),
								width: Common.getLengthByIPhone7(227),
								height: Common.getLengthByIPhone7(68),
								borderRadius: Common.getLengthByIPhone7(12),
								backgroundColor: 'white',
								borderColor: 'rgba(0, 0, 0, 0.27)',
								borderWidth: 1,
								paddingLeft: Common.getLengthByIPhone7(13),
								paddingRight: Common.getLengthByIPhone7(13),
								fontFamily: 'SFProDisplay-Medium',
								fontSize: Common.getLengthByIPhone7(40),
								color: 'black',
								textAlign: 'center',
							}}
							ref={input}
							numberOfLines={4}
							contextMenuHidden={false}
							autoCorrect={false}
							autoCompleteType={'off'}
							keyboardType={'number-pad'}
							returnKeyType={'done'}
							secureTextEntry={false}
							allowFontScaling={false}
							underlineColorAndroid={'transparent'}
							onFocus={() => {
								let str = value;
								str = str.replace(/[^\d]/g, '');
								setValue(str);
							}}
							onBlur={() => {
								let str = value;
								str = str.replace(/[^\d]/g, '');
								str = str + String.fromCharCode(0x20BD);
								setValue(str);
							}}
							onChangeText={code => {
								setValue(code);
							}}
							value={value}
						/>
						<TouchableOpacity style={{
							width: Common.getLengthByIPhone7(30),
							height: Common.getLengthByIPhone7(30),
							borderRadius: Common.getLengthByIPhone7(15),
							alignItems: 'center',
							justifyContent: 'center',
							borderColor: 'rgba(0, 0, 0, 0.3)',
							borderWidth: 1,
						}}
						onPress={() => {
							let str = value;
							str = str.replace(/[^\d]/g, '');
							str++;
							str = str + String.fromCharCode(0x20BD);
							setValue(str);
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(20),
								lineHeight: Common.getLengthByIPhone7(20),
							}}>
								+
							</Text>
						</TouchableOpacity>
					</View>
					<View style={{
						borderTopColor: 'rgba(0, 0, 0, 0.16)',
						borderTopWidth: 1,
						width: Common.getLengthByIPhone7(0),
						height: Common.getLengthByIPhone7(102),
						flexDirection: 'row',
						alignItems: 'flex-start',
						justifyContent: 'space-between',
						paddingTop: Common.getLengthByIPhone7(10),
						paddingLeft: Common.getLengthByIPhone7(20),
						paddingRight: Common.getLengthByIPhone7(20),
						position: 'absolute',
						bottom: 0,
						backgroundColor: 'white',
					}}>
						<TouchableOpacity style={{
							height: Common.getLengthByIPhone7(46),
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							navigation.goBack();
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: '#141414',
								textDecorationLine: 'underline',
							}}>
								Назад
							</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{
							width: Common.getLengthByIPhone7(122),
							height: Common.getLengthByIPhone7(46),
							borderRadius: Common.getLengthByIPhone7(8),
							backgroundColor: 'black',
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							next();
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Medium',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'white',
							}}>
								Далее
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// advantageList: state.user.advantageList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	updateAdv: payload => dispatch.user.updateAdv(payload),
	setRentUpdateObj: payload => dispatch.user.setRentUpdateObj(payload),
});

export default connect(mstp, mdtp)(PriceScreen);