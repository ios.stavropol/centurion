import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import TopView from '../../components/Profile/TopView';
import MenuView from '../../components/Profile/MenuView';
import VerifyView from '../../components/Profile/VerifyView';

const ProfileScreen = ({}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
      <TopView
        onClick={() => {
          navigation.navigate('EditProfile');
        }}
      />
      <ScrollView style={{
        width: Common.getLengthByIPhone7(0),
        marginTop: -Common.getLengthByIPhone7(50),
        flex: 1,
      }}
      contentContainerStyle={{
        alignItems: 'center',
        justifyContent: 'flex-start'
      }}>
        <VerifyView
          style={{
            marginTop: Common.getLengthByIPhone7(80),
          }}
          title={'Пройти верификацию'}
          icon={require('./../../assets/ic-profile-scan.png')}
          onClick={() => {

          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Личная информация'}
          icon={require('./../../assets/ic-profile-info.png')}
          onClick={() => {

          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Платежи и выплаты'}
          icon={require('./../../assets/ic-profile-money.png')}
          onClick={() => {
            
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Уведомления'}
          icon={require('./../../assets/ic-profile-notify.png')}
          onClick={() => {
            
          }}
        />
        <Text style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(20),
					marginTop: Common.getLengthByIPhone7(30),
					color: 'black',
        }}>
          Стать Хозяином
        </Text>
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(30),
          }}
          title={'Разместить объявление'}
          icon={require('./../../assets/ic-profile-upload.png')}
          onClick={() => {
            navigation.navigate('CreateTab');
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Создать событие'}
          icon={require('./../../assets/ic-profile-event.png')}
          onClick={() => {
            
          }}
        />
        <Text style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(20),
					marginTop: Common.getLengthByIPhone7(30),
					color: 'black',
        }}>
          Поддержка
        </Text>
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(30),
          }}
          title={'Как все устроено в Centurion'}
          icon={require('./../../assets/ic-profile-compas.png')}
          onClick={() => {
            
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Центр безопасности'}
          icon={require('./../../assets/ic-profile-shield.png')}
          onClick={() => {
            
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Поддержка'}
          icon={require('./../../assets/ic-profile-chat.png')}
          onClick={() => {
            
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Обратная связь'}
          icon={require('./../../assets/ic-profile-feedback.png')}
          onClick={() => {
            
          }}
        />
        <Text style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(20),
					marginTop: Common.getLengthByIPhone7(30),
					color: 'black',
        }}>
          Юридическая информация
        </Text>
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(30),
          }}
          title={'Условия предоставления услуг'}
          icon={require('./../../assets/ic-profile-list.png')}
          onClick={() => {
            
          }}
        />
        <MenuView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          title={'Политика конфиденциальности'}
          icon={require('./../../assets/ic-profile-list.png')}
          onClick={() => {
            
          }}
        />
        <TouchableOpacity style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          marginTop: Common.getLengthByIPhone7(30),
          marginBottom: Common.getLengthByIPhone7(120),
        }}>
          <Text style={{
            fontFamily: 'SFProDisplay-Medium',
            fontSize: Common.getLengthByIPhone7(20),
            color: 'black',
          }}>
            Выйти
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	// setBiometryType: payload => dispatch.buttons.setBiometryType(payload),
});

export default connect(mstp, mdtp)(ProfileScreen);