import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { TextInput, ImageBackground, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import CategoryView from '../../components/Advantages/CategoryView';
import { BASE_URL, APP_NAME } from '../../constants';
import TagView from '../../components/Schtick/TagView';

const SchtickScreen = ({schtickList, rentUpdateObj, setRentUpdateObj}) => {

	const navigation = useNavigation();
	const [tags, setTags] = React.useState([]);
	const [selected, setSelected] = React.useState([]);
	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['53%', '90%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		
	}, []);

	useEffect(() => {

		let array = [];

		for (let i = 0; i < schtickList.length; i++) {
			array.push(<TagView
				data={schtickList[i]}
				onSelect={value => {
					let array = JSON.parse(JSON.stringify(selected));

					if (value) {
						array.push(schtickList[i].id);
					} else {
						for (let i = 0; i < array.length; i++) {
							if (array[i] == schtickList[i].id) {
								array.splice(i, 1);
								break;
							}
						}
					}

					setSelected(array);
				}}
			/>);
		}

		setTags(array);
	}, []);

	const next = () => {
		if (selected.length) {
			let obj = rentUpdateObj;
			obj.schticks = selected;
			setRentUpdateObj(obj);
			navigation.navigate('About');
		} else {
			Alert.alert(APP_NAME, 'Выберите одну-две отличительные черты!');
		}
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<ImageBackground
				source={require('./../../assets/ic-fon-about.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
				}}
			>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					fontFamily: 'SFProDisplay-Medium',
					fontSize: Common.getLengthByIPhone7(28),
					color: 'white',
					marginBottom: Common.getLengthByIPhone7(40),
					textAlign: 'left',
					position: 'absolute',
					top: Common.getLengthByIPhone7(320),
				}}>
					{`Опишите жилье`}
				</Text>
				<BottomSheet
					style={{
						backgroundColor: 'white',
						shadowColor: "black",
						shadowOffset: {
							width: 0,
							height: -12,
						},
						shadowOpacity: 0.05,
						shadowRadius: 30.00,
						elevation: 4,
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
					}}
					handleStyle={{
						// borderTopLeftRadius: Common.getLengthByIPhone7(30),
					}}
					// containerHeight={100}
					handleComponent={() => {
						return (<View
							style={{
								width: Common.getLengthByIPhone7(0),
								height: Common.getLengthByIPhone7(70),
								paddingLeft: Common.getLengthByIPhone7(20),
								alignItems: 'flex-start',
								justifyContent: 'center',
							}}
						>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'black',
							}}>
								Выберите одну-две отличительные черты
							</Text>
						</View>);
					}}
					backgroundStyle={{
						
					}}
					ref={bottomSheetRef}
					index={0}
					snapPoints={snapPoints}
					onChange={handleSheetChanges}
				>
					<View style={{
						width: Common.getLengthByIPhone7(0),
						paddingLeft: Common.getLengthByIPhone7(20),
						paddingRight: Common.getLengthByIPhone7(20),
						zIndex: 100,
						flex: 1,
						backgroundColor: 'white',
						flexDirection: "row",
						flexWrap: "wrap",
						alignItems: "center",
					}}>
						{tags}
					</View>
				</BottomSheet>
				<View style={{
					borderTopColor: 'rgba(0, 0, 0, 0.16)',
					borderTopWidth: 1,
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(102),
					flexDirection: 'row',
					alignItems: 'flex-start',
					justifyContent: 'space-between',
					paddingTop: Common.getLengthByIPhone7(10),
					paddingLeft: Common.getLengthByIPhone7(20),
					paddingRight: Common.getLengthByIPhone7(20),
					position: 'absolute',
					bottom: 0,
					backgroundColor: 'white',
				}}>
					<TouchableOpacity style={{
						height: Common.getLengthByIPhone7(46),
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: '#141414',
							textDecorationLine: 'underline',
						}}>
							Назад
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{
						width: Common.getLengthByIPhone7(122),
						height: Common.getLengthByIPhone7(46),
						borderRadius: Common.getLengthByIPhone7(8),
						backgroundColor: 'black',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						next();
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Medium',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'white',
						}}>
							Далее
						</Text>
					</TouchableOpacity>
				</View>
			</ImageBackground>
        </View>
  );
};

const mstp = (state: RootState) => ({
	schtickList: state.user.schtickList,
	rentUpdateObj: state.user.rentUpdateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setRentUpdateObj: payload => dispatch.user.setRentUpdateObj(payload),
});

export default connect(mstp, mdtp)(SchtickScreen);