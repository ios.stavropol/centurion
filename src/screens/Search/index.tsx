import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import MapCircleView from '../../components/Search/MapCircleView';
import SearchView from '../../components/Search/SearchView';
import CategoriesScroll from '../../components/Search/CategoriesScroll';
import PartyView from '../../components/Search/PartyView';
import Centurion1View from '../../components/Search/Centurion1View';
import Centurion2View from '../../components/Search/Centurion2View';
import MenuView from '../../components/Search/MenuView';
import {checkNotifications, requestNotifications, PERMISSIONS, RESULTS} from 'react-native-permissions';
import OneSignal from 'react-native-onesignal';

const SearchScreen = ({setPushId}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	useEffect(() => {
		checkNotifications().then(({status, settings}) => {
			console.warn(status);
			if (status === RESULTS.GRANTED) {
				/* O N E S I G N A L   S E T U P */
				OneSignal.setAppId("7b015d95-ffd5-4a4e-9aeb-94aabeb2a83f");
				OneSignal.setLogLevel(6, 0);
				OneSignal.setRequiresUserPrivacyConsent(false);
				OneSignal.promptForPushNotificationsWithUserResponse(response => {
				console.warn('promptForPushNotificationsWithUserResponse: '+JSON.stringify(response));
					// this.OSLog("Prompt response:", response);
				});
			
				/* O N E S I G N A L  H A N D L E R S */
				OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
					// getMessages();
				});
				OneSignal.setNotificationOpenedHandler(notification => {
					console.warn('setNotificationOpenedHandler: '+JSON.stringify(notification));
					// getMessages();
					if (notification.notification.additionalData.type === 'company') {
						// NavigationService.navigate('Company', {data: {id: notification.notification.additionalData.id}});
						// navigation.navigate('Company', {data: {id: notification.notification.additionalData.id}});
					} else if (notification.notification.additionalData.type === 'message') {
						// NavigationService.navigate('Chat');
						// navigation.navigate('Chat');
					} else if (notification.notification.additionalData.type === 'case') {
						// if (notification.notification.additionalData.is_sou) {
						// 	navigation.navigate('SouCase', {data: {id: notification.notification.additionalData.id}});
						// } else {
						// 	navigation.navigate('Case', {data: {id: notification.notification.additionalData.id}});
						// }
						// NavigationService.navigate('Item', {data: {id: notification.notification.additionalData.id}});
						
					} else if (notification.notification.additionalData.type === 'keyword') {
						// NavigationService.navigate('Word', {data: {id: notification.notification.additionalData.id}});
						// navigation.navigate('Keyword', {data: {id: notification.notification.additionalData.id}});
					}
				});
				OneSignal.setInAppMessageClickHandler(event => {
				console.warn('setInAppMessageClickHandler: '+JSON.stringify(event));
					// this.OSLog("OneSignal IAM clicked:", event);
				});
				OneSignal.addEmailSubscriptionObserver((event) => {
				console.warn('addEmailSubscriptionObserver: '+JSON.stringify(event));
					// this.OSLog("OneSignal: email subscription changed: ", event);
				});
				OneSignal.addSubscriptionObserver(event => {
				console.warn('addSubscriptionObserver: '+JSON.stringify(event));
					// this.OSLog("OneSignal: subscription changed:", event);
					// this.setState({ isSubscribed: event.to.isSubscribed})
				});
				OneSignal.addPermissionObserver(event => {
				console.warn('addPermissionObserver: '+JSON.stringify(event));
					// this.OSLog("OneSignal: permission changed:", event);
				});
			
				OneSignal.getDeviceState()
				.then(device => {
					console.warn('getDeviceState: '+JSON.stringify(device));
					setPushId(device.userId)
					.then(() => {
			
					})
					.catch(err => {
			
					});
				})
				.catch(err => {
			
				});
			}
		});
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'black',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<SearchView
				onPress={() => {
					navigation.navigate('SelectCity');
				}}
			/>
			<MapCircleView/>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				marginTop: Common.getLengthByIPhone7(42),
				paddingBottom: Common.getLengthByIPhone7(100),
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				<View
					style={{
						position: 'absolute',
						left: 0,
						bottom: 0,
						right: 0,
						top: Dimensions.get('screen').height - 100,
						backgroundColor: 'white',
					}}
				/>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					color: 'white',
					fontFamily: 'SFProRounded-Regular',
					fontSize: Common.getLengthByIPhone7(30),
					marginTop: Common.getLengthByIPhone7(430),
				}}>
					Арендуй
				</Text>
				<CategoriesScroll
					style={{
						marginTop: Common.getLengthByIPhone7(30),
						marginBottom: Common.getLengthByIPhone7(55),
					}}
				/>
				<PartyView
					onClick={() => {

					}}
				/>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					// color: 'white',
					color: 'black',
					fontFamily: 'SFProRounded-Regular',
					fontSize: Common.getLengthByIPhone7(26),
					marginTop: Common.getLengthByIPhone7(55),
					marginBottom: Common.getLengthByIPhone7(30),
				}}>
					Давайте больше времени проводить вместе
				</Text>
				<Centurion1View
					onClick={() => {

					}}
				/>
				<Centurion2View
					onClick={() => {

					}}
					style={{
						marginTop: Common.getLengthByIPhone7(30),
					}}
				/>
				<MenuView
					style={{
						marginTop: Common.getLengthByIPhone7(27),
						marginBottom: Common.getLengthByIPhone7(100),
					}}
				/>
			</ScrollView>
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	setPushId: payload => dispatch.user.setPushId(payload),
});

export default connect(mstp, mdtp)(SearchScreen);