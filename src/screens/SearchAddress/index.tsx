import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Dimensions, ScrollView, View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import GradientButton from '../../components/GradientButton';
import { BASE_URL, APP_NAME } from './../../constants';
import { useEvent } from 'react-native-reanimated';
import Geolocation from '@react-native-community/geolocation';

const SearchAddressScreen = ({route, searchAddress, getAddressFromCoords, rentCreateObj, setRentCreateObj}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [search, setSearch] = React.useState('');

	const renderAddress = (data) => {
		return (<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			paddingBottom: Common.getLengthByIPhone7(12),
			paddingTop: Common.getLengthByIPhone7(12),
			borderBottomColor: 'rgba(0, 0, 0, 0.33)',
			borderBottomWidth: 1,
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'flex-start',
		}}
		onPress={() => {
			Alert.alert(
				APP_NAME,
				'Вы уверены в выборе?',
				[
				  {
					text: "Нет",
					onPress: () => console.log("Cancel Pressed"),
					style: "cancel"
				  },
				  { text: "Да", onPress: () => {
					  navigation.navigate('ConfirmAddress', {data: data});
				  }}
				]
			);
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(43),
				height: Common.getLengthByIPhone7(43),
				borderRadius: Common.getLengthByIPhone7(6),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: '#d8d8d8',
				marginRight: Common.getLengthByIPhone7(12),
			}}>
				<Image
					source={require('./../../assets/ic-geotag.png')}
					style={{
						width: Common.getLengthByIPhone7(20),
						height: Common.getLengthByIPhone7(20),
					}}
				/>
			</View>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(14),
				color: '#343434',
			}}>
				{data.value}
			</Text>
		</TouchableOpacity>);
	}

	useEffect(() => {
		searchAddress(search)
		.then(resp => {
			let array = [];
			for (let i = 0; i < resp.length; i++) {
				array.push(renderAddress(resp[i]));
			}
			setBody(array);
		})
		.catch(err => {

		});
	}, [search]);

	return (
    <View style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
    }}>
		<ImageBackground
			source={require('./../../assets/ic-create2.png')}
			style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				resizeMode: 'cover',
				alignItems: 'center',
				justifyContent: 'flex-end',
			}}
		>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				height: Dimensions.get('screen').height - Common.getLengthByIPhone7(60),
				borderTopLeftRadius: Common.getLengthByIPhone7(20),
				borderTopRightRadius: Common.getLengthByIPhone7(20),
				backgroundColor: 'white',
				alignItems: 'center',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					height: Common.getLengthByIPhone7(80),
					alignItems: 'center',
					justifyContent: 'center',
				}}>
					<TouchableOpacity style={{
						position: 'absolute',
						left: Common.getLengthByIPhone7(20),
					}}
					onPress={() => {
						navigation.goBack();
					}}>
						<Image
							source={require('./../../assets/ic-arrow-back.png')}
							style={{
								width: Common.getLengthByIPhone7(16),
								height: Common.getLengthByIPhone7(16),
							}}
						/>
					</TouchableOpacity>
					<Text style={{
						fontFamily: 'SFProDisplay-Regular',
						fontSize: Common.getLengthByIPhone7(16),
						color: '#000000',
					}}>
						Введите адрес
					</Text>
				</View>
				<View style={{
					alignItems: 'center',
					justifyContent: 'center',
				}}>
					<TextInput
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
							height: Common.getLengthByIPhone7(45),
							borderRadius: Common.getLengthByIPhone7(37),
							backgroundColor: 'white',
							borderColor: 'rgba(0, 0, 0, 0.24)',
							borderWidth: 1,
							fontSize: Common.getLengthByIPhone7(14),
							// lineHeight: Common.getLengthByIPhone7(22),
							paddingLeft: Common.getLengthByIPhone7(37),
							letterSpacing: -0.408,
							fontFamily: 'SFProDisplay-Regular',
							fontWeight: 'normal',
							color: '#141414',
							textAlign: 'left',
						}}
						placeholderTextColor={'rgba(0, 0, 0, 0.28)'}
						placeholder={'Поиск'}
						contextMenuHidden={false}
						autoCorrect={false}
						autoCompleteType={'off'}
						returnKeyType={'done'}
						secureTextEntry={false}
						// keyboardType={'number-pad'}
						allowFontScaling={false}
						underlineColorAndroid={'transparent'}
						onSubmitEditing={() => {
							// this.nextClick();
						}}
						onFocus={() => {}}
						onBlur={() => {
						
						}}
						onChangeText={code => {
							setSearch(code);
						}}
						value={search}
					/>
					<Image
						source={require('./../../assets/ic-geotag.png')}
						style={{
							width: Common.getLengthByIPhone7(15),
							height: Common.getLengthByIPhone7(18),
							position: 'absolute',
							left: Common.getLengthByIPhone7(12),
						}}
					/>
				</View>
				<TouchableOpacity style={{
					marginTop: Common.getLengthByIPhone7(33),
					paddingBottom: Common.getLengthByIPhone7(12),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
					flexDirection: 'row',
					alignItems: 'center',
					borderBottomColor: 'rgba(0, 0, 0, 0.16)',
					borderBottomWidth: 1,
				}}
				onPress={() => {
					// Geolocation.requestAuthorization()
					// .then(resp => {
						Geolocation.getCurrentPosition(info => {
							console.warn(info);
							getAddressFromCoords({
								lat: info.coords.latitude,
								lon: info.coords.longitude,
							})
							.then(address => {
								if (address?.length) {
									navigation.navigate('ConfirmAddress', {data: address[0]});
								}
							})
							.catch(err => {

							});
						});
					// })
					// .catch(err => {

					// });
				}}>
					<Image
						source={require('./../../assets/ic-myplace.png')}
						style={{
							width: Common.getLengthByIPhone7(33),
							height: Common.getLengthByIPhone7(33),
							marginRight: Common.getLengthByIPhone7(9),
						}}
					/>
					<Text style={{
						fontFamily: 'SFProDisplay-Medium',
						fontSize: Common.getLengthByIPhone7(14),
						color: '#141414',
					}}>
						{`Использовать текущее\nместоположение`}
					</Text>
				</TouchableOpacity>
				<ScrollView style={{
					width: Common.getLengthByIPhone7(0),
					// marginBottom: Common.getLengthByIPhone7(50),
					flex: 1,
				}}
				contentContainerStyle={{
					alignItems: 'center',
				}}>
					{body}
				</ScrollView>
			</View>
		</ImageBackground>
    </View>
  );
};

const mstp = (state: RootState) => ({
	rentCreateObj: state.user.rentCreateObj,
});

const mdtp = (dispatch: Dispatch) => ({
	searchAddress: payload => dispatch.user.searchAddress(payload),
	getAddressFromCoords: payload => dispatch.user.getAddressFromCoords(payload),
	setRentCreateObj: payload => dispatch.user.setRentCreateObj(payload),
});

export default connect(mstp, mdtp)(SearchAddressScreen);