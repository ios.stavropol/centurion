import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import MapCircleView from '../../components/Search/MapCircleView';
import { BASE_URL, APP_NAME } from '../../constants';
import CategoryView from '../../components/SelectCategory/CategoryView';
import GradientButton from '../../components/GradientButton';

const SelectCategoryScreen = ({setSearchObj, searchObj, categoryList}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [selected, setSelected] = React.useState(null);
	const [data, setData] = React.useState(null);

	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['40%', '80%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	useEffect(() => {
		console.warn('categoryList: ', categoryList);
		render();
	}, []);

	const render = () => {
		let array = [];

		for (let i = 0; i < categoryList.length; i++) {
			array.push(renderView(categoryList[i].name, BASE_URL + categoryList[i].cover, categoryList[i].id, {
				marginBottom: i + 1 === categoryList.length ? Common.getLengthByIPhone7(40) : Common.getLengthByIPhone7(10),
				marginTop: i === 0 ? Common.getLengthByIPhone7(30) : 0,
			}, () => {
				console.warn(categoryList[i]);
				setData(categoryList[i]);
				setSelected(categoryList[i].id);
			}));
		}
		setBody(array);
	}


	useEffect(() => {
		render();
	}, [selected]);

	const renderView = (title, image, index, style, action) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
			height: Common.getLengthByIPhone7(60),
			borderRadius: Common.getLengthByIPhone7(10),
			paddingLeft: Common.getLengthByIPhone7(15),
			paddingRight: Common.getLengthByIPhone7(10),
			borderColor: selected == index ? 'black' : '#CBCBCB',
			borderWidth: 1,
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}
		onPress={() => {
			if (action) {
				action();
			}
		}}>
			<Text style={{
				fontFamily: 'SFProDisplay-Regular',
				fontSize: Common.getLengthByIPhone7(16),
				color: 'black',
			}}>
				{title}
			</Text>
			<View style={{
				width: Common.getLengthByIPhone7(40),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(5),
				overflow: 'hidden',
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: '#FFD441',
			}}>
				<Image
					source={{uri: image}}
					style={{
						width: Common.getLengthByIPhone7(40),
						height: Common.getLengthByIPhone7(40),
						resizeMode: 'cover',
					}}
				/>
			</View>
		</TouchableOpacity>);
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<MapCircleView
				style={{
					top: Common.getLengthByIPhone7(80),
				}}
			/>
			<BottomSheet
				style={{
					backgroundColor: 'white',
					shadowColor: "black",
					shadowOffset: {
						width: 0,
						height: -12,
					},
					shadowOpacity: 0.05,
					shadowRadius: 30.00,
					elevation: 4,
					borderTopLeftRadius: Common.getLengthByIPhone7(30),
					borderTopRightRadius: Common.getLengthByIPhone7(30),
				}}
				handleStyle={{
					// borderTopLeftRadius: Common.getLengthByIPhone7(30),
				}}
				// containerHeight={100}
				handleComponent={() => {
					return (<View style={{
						height: Common.getLengthByIPhone7(60),
						backgroundColor: 'white',
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
						overflow: 'hidden',
						alignItems: 'center',
						justifyContent: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'black',
						}}>
							Что ищите?
						</Text>
						<TouchableOpacity style={{
							position: 'absolute',
							left: Common.getLengthByIPhone7(20),
							// width: Common.getLengthByIPhone7(10),
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							navigation.goBack();
						}}>
							<Image
								source={require('./../../assets/ic-arrow-back.png')}
								style={{
									width: Common.getLengthByIPhone7(24),
									height: Common.getLengthByIPhone7(24),
									resizeMode: 'contain',
								}}
							/>
						</TouchableOpacity>
					</View>);
				}}
				backgroundStyle={{
					
				}}
				ref={bottomSheetRef}
				index={0}
				snapPoints={snapPoints}
				onChange={handleSheetChanges}
			>
				<View style={{
					zIndex: 100,
					flex: 1,
					alignItems: 'center',
					backgroundColor: 'white',
				}}>
					{body}
					<GradientButton
						title={'Вперед'}
						style={{
							marginBottom: Common.getLengthByIPhone7(50),
						}}
						onPress={() => {
							if (selected !== null) {
								let obj = JSON.parse(JSON.stringify(searchObj));
								obj.categories = [selected];
								obj.mainCat = data.id;
								obj.slug = data.slug;
								setSearchObj(obj);
								navigation.navigate('SelectDates');
							} else {
								Alert.alert(APP_NAME, 'Выберите категорию');
							}
						}}
					/>
				</View>
			</BottomSheet>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
	categoryList: state.user.categoryList,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
});

export default connect(mstp, mdtp)(SelectCategoryScreen);