import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import MapCircleView from '../../components/Search/MapCircleView';
import SearchView from '../../components/SelectCity/SearchView';

const SelectCityScreen = ({setSearchObj, searchObj}) => {

	const navigation = useNavigation();
	const [page, setPage] = React.useState(0);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<SearchView
				onPress={obj => {
					console.warn(obj);
					// let data = {};
					// data.street = obj.data.street_with_type + ' ' + obj.data.house_type + ' ' + obj.data.house;
					// data.city = obj.data.city_with_type;
					// data.zip_number = obj.data.postal_code;
					// data.region = obj.data.region_with_type;
					// data.country = obj.data.country;
					// data.geo = [parseFloat(obj.data.geo_lat), parseFloat(obj.data.geo_lon)];
					let data = JSON.parse(JSON.stringify(searchObj));
					data.city = obj.data.city_with_type;
					setSearchObj(data);
					navigation.navigate('SelectCategory');
				}}
			/>
			<MapCircleView
				style={{
					top: Common.getLengthByIPhone7(150),
				}}
			/>
			
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
});

export default connect(mstp, mdtp)(SelectCityScreen);