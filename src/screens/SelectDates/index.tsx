import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import MapCircleView from '../../components/Search/MapCircleView';
import { BASE_URL, APP_NAME } from '../../constants';
import CategoryView from '../../components/SelectCategory/CategoryView';
import GradientButton from '../../components/GradientButton';
import {LocaleConfig, CalendarList} from 'react-native-calendars';

LocaleConfig.locales['ru'] = {
	monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
	dayNamesShort: ['ВC','ПН','ВТ','СР','ЧТ','ПТ','СБ'],
	today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'ru';

const SelectDatesScreen = ({setSearchObj, searchObj, categoryList}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [start, setStart] = React.useState(null);
	const [end, setEnd] = React.useState(null);
	const [markedDates, setMarkedDates] = React.useState({});

	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['50%', '80%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	useEffect(() => {
		renderCalendar();
	}, []);

	useEffect(() => {
		console.warn('useEffect: ', start, ' ', end);
		if (start !== null) {
			if (end === null) {
				let period = {};
				period[start] = {startingDay: true, color: '#0a0a0a', textColor: 'white'};
				setMarkedDates(period);
			} else {
				let period = {};
				let s = new Date(start);
				let e = new Date(end);
				period[start] = {startingDay: true, color: '#0a0a0a', textColor: 'white'};
				while (e > s) {
					s = new Date(s.setDate(s.getDate() + 1));
					period[Common.getEngDate(s)] = {color: '#0a0a0a', textColor: 'white'};
				}
				period[Common.getEngDate(e)] = {endingDay: true, color: '#0a0a0a', textColor: 'white'};
				setMarkedDates(period);
			}
		}
	}, [start, end]);

	useEffect(() => {
		console.warn('markedDates: ', markedDates);
		renderCalendar();
	}, [markedDates]);

	const renderCalendar = () => {
		setBody(<CalendarList
			style={{
				width: Common.getLengthByIPhone7(0),
			}}
			markingType={'period'}
			markedDates={markedDates}
			pastScrollRange={0}
			futureScrollRange={3}
			current={Common.getEngDate(new Date())}
			minDate={Common.getEngDate(new Date())}
			// maxDate={Common.getEngDate()}
			onDayPress={day => {
				if (start === null) {
					setStart(day.dateString);
				} else {
					if (end === null) {
						let x = new Date(start);
						let y = new Date(day.dateString);
						if (x < y) {
							setEnd(day.dateString);
						} else {
							setEnd(start);
							setStart(day.dateString);
						}
					} else {
						setStart(day.dateString);
						setEnd(null);
					}
				}
				console.warn('selected day', day);
			}}
			onDayLongPress={day => {
				console.log('selected day', day);
			}}
			monthFormat={'MMM yyyy'}
			onMonthChange={month => {
				console.log('month changed', month);
			}}
			hideArrows={true}
			hideExtraDays={true}
			disableMonthChange={true}
			firstDay={1}
			hideDayNames={false}
			showWeekNumbers={false}
			onPressArrowLeft={subtractMonth => subtractMonth()}
			onPressArrowRight={addMonth => addMonth()}
			disableArrowLeft={true}
			disableArrowRight={true}
			disableAllTouchEventsForDisabledDays={true}
			enableSwipeMonths={false}
			theme={{
				backgroundColor: '#ffffff',
				calendarBackground: '#ffffff',
				textSectionTitleColor: '#b6c1cd',
				textSectionTitleDisabledColor: '#d9e1e8',
				selectedDayBackgroundColor: '#0a0a0a',
				selectedDayTextColor: '#ffffff',
				todayTextColor: '#333333',
				dayTextColor: '#333333',
				textDisabledColor: '#e0e0e0',
				dotColor: '#00adf5',
				selectedDotColor: '#ffffff',
				arrowColor: colors.ORANGE_COLOR,
				disabledArrowColor: '#d9e1e8',
				monthTextColor: colors.TEXT_COLOR,
				indicatorColor: 'red',
				textDayFontFamily: 'SFProDisplay-Regular',
				textMonthFontFamily: 'SFProDisplay-Regular',
				textDayHeaderFontFamily: 'SFProDisplay-Regular',
				textDayFontWeight: 'normal',
				textMonthFontWeight: 'bold',
				textDayHeaderFontWeight: 'bold',
				textDayFontSize: Common.getLengthByIPhone7(14),
				textMonthFontSize: Common.getLengthByIPhone7(14),
				textDayHeaderFontSize: Common.getLengthByIPhone7(10)
			}}
		/>);
	}

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<MapCircleView
				style={{
					top: Common.getLengthByIPhone7(80),
				}}
			/>
			<BottomSheet
				style={{
					backgroundColor: 'white',
					shadowColor: "black",
					shadowOffset: {
						width: 0,
						height: -12,
					},
					shadowOpacity: 0.05,
					shadowRadius: 30.00,
					elevation: 4,
					borderTopLeftRadius: Common.getLengthByIPhone7(30),
					borderTopRightRadius: Common.getLengthByIPhone7(30),
				}}
				handleStyle={{
					// borderTopLeftRadius: Common.getLengthByIPhone7(30),
				}}
				// containerHeight={100}
				handleComponent={() => {
					return (<View style={{
						height: Common.getLengthByIPhone7(60),
						backgroundColor: 'white',
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
						overflow: 'hidden',
						alignItems: 'center',
						justifyContent: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'black',
						}}>
							Какие даты вам нужны?
						</Text>
						<TouchableOpacity style={{
							position: 'absolute',
							left: Common.getLengthByIPhone7(20),
							// width: Common.getLengthByIPhone7(10),
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							navigation.goBack();
						}}>
							<Image
								source={require('./../../assets/ic-arrow-back.png')}
								style={{
									width: Common.getLengthByIPhone7(24),
									height: Common.getLengthByIPhone7(24),
									resizeMode: 'contain',
								}}
							/>
						</TouchableOpacity>
					</View>);
				}}
				backgroundStyle={{
					
				}}
				ref={bottomSheetRef}
				index={0}
				snapPoints={snapPoints}
				onChange={handleSheetChanges}
			>
				<View style={{
					zIndex: 100,
					flex: 1,
					alignItems: 'center',
					backgroundColor: 'white',
				}}>
					<View style={{
						flex: 1,
						marginBottom: 20,
						overflow: 'hidden',
					}}>
						{body}
					</View>
					<View style={{
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						marginBottom: Common.getLengthByIPhone7(100),
					}}>
						<TouchableOpacity style={{

						}}
						onPress={() => {
							let obj = JSON.parse(JSON.stringify(searchObj));
							obj.date_from = null;
							obj.date_to = null;
							setSearchObj(obj);
							navigation.navigate('SelectPeople');
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: '#141414',
								textDecorationLine: 'underline',
							}}>
								Пропустить
							</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{
							width: Common.getLengthByIPhone7(109),
							height: Common.getLengthByIPhone7(46),
							borderRadius: Common.getLengthByIPhone7(8),
							alignItems: 'center',
							justifyContent: 'center',
							backgroundColor: 'black',
						}}
						onPress={() => {
								let obj = JSON.parse(JSON.stringify(searchObj));
								obj.date_from = start;
								obj.date_to = end;
								setSearchObj(obj);
								navigation.navigate('SelectPeople');
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: 'white',
							}}>
								Далее
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</BottomSheet>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
	categoryList: state.user.categoryList,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
});

export default connect(mstp, mdtp)(SelectDatesScreen);