import React, { useCallback, useMemo, useRef, useEffect } from 'react';
import { ScrollView, Dimensions, View, Image, TouchableOpacity, Text, Animated, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import MapCircleView from '../../components/Search/MapCircleView';
import { BASE_URL, APP_NAME } from '../../constants';
import GradientButton from '../../components/GradientButton';
import SwitchView from '../../components/Guests/SwitchView';

const SelectPeopleScreen = ({setSearchObj, searchObj, categoryList}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);
	const [start, setStart] = React.useState(null);
	const [end, setEnd] = React.useState(null);
	const [adult, setAdult] = React.useState(0);
	const [children, setChildren] = React.useState(0);
	const [babies, setBabies] = React.useState(0);
	const [animals, setAnimals] = React.useState(0);

	const bottomSheetRef = useRef<BottomSheet>(null);

	const snapPoints = useMemo(() => ['50%', '70%'], []);

	const handleSheetChanges = useCallback((index: number) => {
		// console.warn('handleSheetChanges', index);
	}, []);

	return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
			justifyContent: 'flex-start',
        }}>
			<MapCircleView
				style={{
					top: Common.getLengthByIPhone7(80),
				}}
			/>
			<BottomSheet
				style={{
					backgroundColor: 'white',
					shadowColor: "black",
					shadowOffset: {
						width: 0,
						height: -12,
					},
					shadowOpacity: 0.05,
					shadowRadius: 30.00,
					elevation: 4,
					borderTopLeftRadius: Common.getLengthByIPhone7(30),
					borderTopRightRadius: Common.getLengthByIPhone7(30),
				}}
				handleStyle={{
					// borderTopLeftRadius: Common.getLengthByIPhone7(30),
				}}
				// containerHeight={100}
				handleComponent={() => {
					return (<View style={{
						height: Common.getLengthByIPhone7(60),
						backgroundColor: 'white',
						borderTopLeftRadius: Common.getLengthByIPhone7(30),
						borderTopRightRadius: Common.getLengthByIPhone7(30),
						overflow: 'hidden',
						alignItems: 'center',
						justifyContent: 'center',
					}}>
						<Text style={{
							fontFamily: 'SFProDisplay-Regular',
							fontSize: Common.getLengthByIPhone7(14),
							color: 'black',
						}}>
							Кто приедет?
						</Text>
						<TouchableOpacity style={{
							position: 'absolute',
							left: Common.getLengthByIPhone7(20),
							// width: Common.getLengthByIPhone7(10),
							alignItems: 'center',
							justifyContent: 'center',
						}}
						onPress={() => {
							navigation.goBack();
						}}>
							<Image
								source={require('./../../assets/ic-arrow-back.png')}
								style={{
									width: Common.getLengthByIPhone7(24),
									height: Common.getLengthByIPhone7(24),
									resizeMode: 'contain',
								}}
							/>
						</TouchableOpacity>
					</View>);
				}}
				backgroundStyle={{
					
				}}
				ref={bottomSheetRef}
				index={0}
				snapPoints={snapPoints}
				onChange={handleSheetChanges}
			>
				<View style={{
					zIndex: 100,
					flex: 1,
					alignItems: 'center',
					backgroundColor: 'white',
				}}>
					<View style={{
						flex: 1,
						marginBottom: 20,
						overflow: 'hidden',
					}}>
						<SwitchView
							title={'Взрослые'}
							subtitle={'От 13 лет'}
							style={{
								// marginTop: Common.getLengthByIPhone7(30),
								borderTopWidth: 1,
								borderTopColor: 'rgba(0, 0, 0, 0.33)',
								borderBottomWidth: 1,
								borderBottomColor: 'rgba(0, 0, 0, 0.33)',
								height: Common.getLengthByIPhone7(80),
							}}
							onChange={value => {
								setAdult(value);
							}}
						/>
						<SwitchView
							title={'Дети'}
							subtitle={'От 2-12 лет'}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: 'rgba(0, 0, 0, 0.33)',
								height: Common.getLengthByIPhone7(80),
							}}
							onChange={value => {
								setChildren(value);
							}}
						/>
						<SwitchView
							title={'Младенцы'}
							subtitle={'Младше 2 лет'}
							style={{
								borderBottomWidth: 1,
								borderBottomColor: 'rgba(0, 0, 0, 0.33)',
								height: Common.getLengthByIPhone7(80),
							}}
							onChange={value => {
								setBabies(value);
							}}
						/>
						<SwitchView
							title={'Домашние животные'}
							style={{
								height: Common.getLengthByIPhone7(80),
							}}
							onChange={value => {
								setAnimals(value);
							}}
						/>
					</View>
					<View style={{
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
						marginBottom: Common.getLengthByIPhone7(100),
					}}>
						<TouchableOpacity style={{

						}}
						onPress={() => {
							let obj = JSON.parse(JSON.stringify(searchObj));
							obj.people = null;
							setSearchObj(obj);
							navigation.navigate('MainSearch');
						}}>
							<Text style={{
								fontFamily: 'SFProDisplay-Regular',
								fontSize: Common.getLengthByIPhone7(14),
								color: '#141414',
								textDecorationLine: 'underline',
							}}>
								Пропустить
							</Text>
						</TouchableOpacity>
						<GradientButton
							title={'Искать'}
							style={{
								width: Common.getLengthByIPhone7(115),
								height: Common.getLengthByIPhone7(46),
							}}
							onPress={() => {
								let obj = JSON.parse(JSON.stringify(searchObj));
								obj.people.adult = adult;
								obj.people.children = children;
								obj.people.babies = babies;
								obj.people.animals = animals;
								setSearchObj(obj);
								navigation.navigate('MainSearch');
							}}
						/>
					</View>
				</View>
			</BottomSheet>
        </View>
  );
};

const mstp = (state: RootState) => ({
	searchObj: state.user.searchObj,
	categoryList: state.user.categoryList,
});

const mdtp = (dispatch: Dispatch) => ({
	setSearchObj: payload => dispatch.user.setSearchObj(payload),
});

export default connect(mstp, mdtp)(SelectPeopleScreen);