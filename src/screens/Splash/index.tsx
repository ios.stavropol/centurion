import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, StatusBar, View, Image, TouchableOpacity, Text, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { StackNavigationProp } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';

const SplashScreen = ({getProfile, getAdvantages, getSchtick, getCategoryList}) => {

  const navigation = useNavigation();

  useEffect(() => {
    
    getCategoryList()
    .then(() => {

    })
    .catch(err => {

    });
    getAdvantages()
    .then(() => {

    })
    .catch(err => {

    });
    getSchtick()
    .then(() => {

    })
    .catch(err => {

    });
    
    StorageHelper.getData('token')
    .then(token => {
      console.warn('token: ', token);
      if (token?.length) {
        API.setToken(token);
        getProfile()
        .then(() => {
          navigation.navigate('LoginIn');
        })
        .catch(err => {
          navigation.navigate('Login');
        });
      } else {
        navigation.navigate('Login');
      }
    })
    .catch(err => {
      navigation.navigate('Login');
    });
  }, []);

  return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
        }}>
          <Image source={require('./../../assets/splash.png')}
            style={{
              width: Common.getLengthByIPhone7(0),
              flex: 1,
              resizeMode: 'cover',
            }}
          />
        </View>
  );
};

const mstp = (state: RootState) => ({
	// isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
	getProfile: () => dispatch.user.getProfile(),
  getAdvantages: () => dispatch.user.getAdvantages(),
  getSchtick: () => dispatch.user.getSchtick(),
  getCategoryList: () => dispatch.user.getCategoryList(),
});

export default connect(mstp, mdtp)(SplashScreen);

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.backgroundColor,
  },
});
