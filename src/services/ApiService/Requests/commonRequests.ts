import axios from 'axios';
import { BASE_URL } from './../../../constants';

class CommonAPI {
	getCode = (phone: string) => {
		return axios.get(BASE_URL + '/api/auth/get-auth-code?phone=' + phone);
	};

	sendCode = (phone: string, code: string) => {
		return axios.get(BASE_URL + '/api/auth/check-auth-code?phone=' + phone + '&code=' + code);
	};

	getProfile = () => {
		return axios.get(BASE_URL + '/api/auth/user-detail');
	};

	editProfile = (first_name: string, last_name: string, sex: string, email: string) => {
		let params = JSON.stringify({
			first_name,
			last_name,
			email,
			sex,
		});
		return axios.post(BASE_URL + '/api/auth/edit-profile', params);
	};

	setPushId = (uid: string) => {
		let params = JSON.stringify({
			uid,
		});
		return axios.post(BASE_URL + '/api/auth/edit-push-uid', params);
	};

	getCategoryList = (level = null, slug = null) => {
		let str = '';
		if (level !== null) {
			str = '?level=' + level + '&slug=' + slug;
		}
		console.warn(BASE_URL + '/api/rents/category-list' + str);
		return axios.get(BASE_URL + '/api/rents/category-list' + str);
	};

	getAdvantages = (category_id = null) => {
		let str = '';
		if (category_id !== null) {
			str = '?category_id=' + category_id;
		}
		console.warn(BASE_URL + '/api/rents/facility-list' + str);
		return axios.get(BASE_URL + '/api/rents/facility-list' + str);
	};

	getSchtick = () => {
		return axios.get(BASE_URL + '/api/rents/schtick-list');
	};

	createAdv = (header, description, geo, appartament_number, region, street, zip_number, country, guest, beds, rooms, bathrooms, status, price, categories, facilities, schticks, additional_tags, popular_facilities, safety_facilities) => {
		let params = JSON.stringify({
			header,
			description,
			geo,
			appartament_number,
			region,
			street,
			zip_number,
			country,
			guest,
			beds,
			rooms,
			bathrooms,
			status,
			price,
			categories,
			facilities,
			schticks,
			additional_tags,
			popular_facilities,
			safety_facilities,
		});
		console.warn('params: ', params);
		return axios.post(BASE_URL + '/api/rents/object', params);
	};

	updateAdv = (id, header, description, geo, appartament_number, region, street, zip_number, country, guest, beds, rooms, bathrooms, status, price, categories, facilities, schticks, additional_tags, popular_facilities, safety_facilities) => {
		let params = JSON.stringify({
			id,
			header,
			description,
			geo,
			appartament_number,
			region,
			street,
			zip_number,
			country,
			guest,
			beds,
			rooms,
			bathrooms,
			status,
			price,
			categories,
			facilities,
			schticks,
			additional_tags,
			popular_facilities,
			safety_facilities,
		});
		return axios.post(BASE_URL + '/api/rents/object-change', params);
	};

	searchAdv = (data) => {
		let params = JSON.stringify(data);
		console.warn(BASE_URL + '/api/rents/search', params);
		return axios.post(BASE_URL + '/api/rents/search', params);
	};

	getFavors = () => {
		return axios.get(BASE_URL + '/api/rents/favourites');
	};

	setFavor = (object_id) => {
		let params = JSON.stringify({
			object_id: object_id,
		});
		return axios.post(BASE_URL + '/api/rents/create-favourite', params);
	};

	getAllChats = () => {
		return axios.get(BASE_URL + '/api/rents/all-chats');
	};

	startChat = (object_id) => {
		return axios.get(BASE_URL + '/api/rents/start-chat/' + object_id);
	};

	getChat = (object_id) => {
		return axios.get(BASE_URL + '/api/rents/all-messages/' + object_id);
	};

	sendMessage = (object_id, message_text) => {
		let params = JSON.stringify({
			message_text: message_text,
		});
		return axios.post(BASE_URL + '/api/rents/send-message/' + object_id, params);
	};

	uploadPhotoToAdv = (id: number, photo: string) => {
		var formData = new FormData();
		formData.append("id", id);
		formData.append("file", {uri: photo, name: 'test.jpg', type: 'image/jpeg'});

		return axios.post(BASE_URL + '/api/rents/object-photo', formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
			}
		});
	};

	getObject = (id: number) => {
		return axios.get(BASE_URL + '/api/rents/object?extend=true&id=' + id);
	};

	getAddressFromCoords = (lat: any, lon: any) => {
		console.warn('https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address?lat='+ lat + '&lon=' + lon);
		return axios.get('https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address?lat='+ lat + '&lon=' + lon, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Token bc46b32a85322ea3c27c3d043c54772dcfe61a9c',
			}
		});
	};

	searchAddress = (query: string) => {
		let params = JSON.stringify({
			query,
		});
		return axios.post('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address', params, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Token bc46b32a85322ea3c27c3d043c54772dcfe61a9c',
			},
		});
		// return axios.get('https://kladr-api.ru/api.php?token=57bnYFG9FKk6fR9R6y9YH89FyeTa9Rbs&query=' + query + '&contentType=building&withParent=1');
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
