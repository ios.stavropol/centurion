//@ts-nocheck
import { createModel } from '@rematch/core';
import { API, StorageHelper } from './../../services';
import { ErrorsHelper } from './../../helpers';
import { Dispatch } from 'store';
import type { RootModel } from './../models';
import AsyncStorage from '@react-native-async-storage/async-storage';

type ButtonState = {
	showToast: boolean,
	toastText: string,
	backFun: any,
	biometryType: any,
	biometryOptions: any,
};

const buttons = createModel<RootModel>()({
	state: {
		showToast: false,
		toastText: '',
		backFun: null,
		biometryType: null,
		biometryOptions: null,
	} as ButtonState, 
	reducers: {
		setShowToast: (state, payload: boolean) => ({
			...state,
			showToast: payload,
		}),
		setToastText: (state, payload: string) => ({
			...state,
			toastText: payload,
		}),
		setBackFun: (state, payload: any) => ({
			...state,
			backFun: payload,
		}),
		setBiometryType: (state, payload: any) => ({
			...state,
			biometryType: payload,
		}),
		setBiometryOptions: (state, payload: any) => ({
			...state,
			biometryOptions: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			showToastMessage(text) {
				dispatch.buttons.setToastText(text);
				dispatch.buttons.setShowToast(true);
			},
			async getFavorites(): Promise<any> {
				const data = await AsyncStorage.getItem('favorites');
				if (data && data.length) {
					data = JSON.parse(data);
					if (data) {
						dispatch.buttons.setFavorites(data);
					} else {
						dispatch.buttons.setFavorites([]);
					}
				} else {
					dispatch.buttons.setFavorites([]);
				}
			},
			async like(id): Promise<any> {
				let array = [];
				const data = await AsyncStorage.getItem('favorites');
				if (data && data.length) {
					console.warn('like: ', typeof data);
					data = JSON.parse(data);
					if (data) {
						array = data;
					}
				}
				// console.warn('like: ', array);
				if (array.includes(id)) {
					for(let i = 0; i < array.length; i++) {
						if (array[i] === id) { 
							array.splice(i, 1);
							break;
						}
					}	
				} else {
					array.push(id);
				}
				console.warn('array: ', array);
				dispatch.buttons.setFavorites(array);
				if (array.length) {
					AsyncStorage.setItem('favorites', JSON.stringify(array));
				} else {
					AsyncStorage.setItem('favorites', '');
				}
			},
			setSecurity(payload){
				AsyncStorage.setItem('biometry', JSON.stringify(payload));
				dispatch.buttons.setBiometryOptions(payload);
			},
		}
	},
});

export default buttons;