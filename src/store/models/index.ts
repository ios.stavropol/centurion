import { Models } from "@rematch/core";
import buttons from "./buttons";
import user from "./user";

export interface RootModel extends Models<RootModel> {
	buttons: typeof buttons;
	user: typeof user;
}

export const models: RootModel = { buttons, user };