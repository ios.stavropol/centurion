//@ts-nocheck
import { createModel } from '@rematch/core';
import { API, StorageHelper } from './../../services';
import { ErrorsHelper } from './../../helpers';
import { Dispatch } from 'store';
import type { RootModel } from './../models';
import { BASE_URL } from './../../constants';

type UserState = {
	isRequestGoing: false,
	userProfile: null,
	categoryList: [],
	advantageList: [],
	schtickList: [],
	searchList: [],
	chatList: [],
	rentCreateObj: {
		header: null,
		description: null,
		geo: null,
		appartament_number: null,
		region: null,
		street: null,
		zip_number: null,
		country: null,
		guest: null,
		beds: null,
		rooms: null,
		bathrooms: null,
		status: null,
		price: null,
		categories: [],
		facilities: [],
		schticks: [],
		additional_tags: [],
		popular_facilities: [],
		safety_facilities: [],
	},
	rentUpdateObj: {
		id: null,
		header: null,
		description: null,
		geo: null,
		appartament_number: null,
		region: null,
		street: null,
		zip_number: null,
		country: null,
		guest: null,
		beds: null,
		rooms: null,
		bathrooms: null,
		status: null,
		price: null,
		categories: [],
		facilities: [],
		schticks: [],
		additional_tags: [],
		popular_facilities: [],
		safety_facilities: [],
	},
	searchObj: {
		price_from: 0,
		price_to: 1000,
		categories: [],
		mainCat: null,
		slug: null,
		features: [],
		city: null,
		people: {
			adult: 0,
			children: 0,
			babies: 0,
			animals: 0,
		},
		flexible_date: false,
		flexible_date_range: 0,
		date_from: null,
		date_to: null,
	},
};

const user = createModel<RootModel>()({
	state: {
		isRequestGoing: false,
		userProfile: null,
		categoryList: [],
		mainCat: null,
		slug: null,
		advantageList: [],
		schtickList: [],
		searchList: [],
		chatList: [],
		rentCreateObj: {
			header: null,
			description: null,
			geo: null,
			appartament_number: null,
			region: null,
			street: null,
			zip_number: null,
			country: null,
			guest: null,
			beds: null,
			rooms: null,
			bathrooms: null,
			status: null,
			price: null,
			categories: [],
			facilities: [],
			schticks: [],
			additional_tags: [],
			popular_facilities: [],
			safety_facilities: [],
		},
		rentUpdateObj: {
			id: null,
			header: null,
			description: null,
			geo: null,
			appartament_number: null,
			region: null,
			street: null,
			zip_number: null,
			country: null,
			guest: null,
			beds: null,
			rooms: null,
			bathrooms: null,
			status: null,
			price: null,
			categories: [],
			facilities: [],
			schticks: [],
			additional_tags: [],
			popular_facilities: [],
			safety_facilities: [],
		},
		searchObj: {
			price_from: 0,
			price_to: 1000,
			categories: [],
			features: [],
			city: null,
			people: {
				adult: 0,
				children: 0,
				babies: 0,
				animals: 0,
			},
			flexible_date: false,
			flexible_date_range: 0,
			date_from: null,
			date_to: null,
		},
	} as UserState, 
	reducers: {
		setRequestGoingStatus: (state, payload: boolean) => ({
			...state,
			isRequestGoing: payload,
		}),
		setUserProfile: (state, payload: object) => ({
			...state,
			userProfile: payload,
		}),
		setCategoryList: (state, payload: object) => ({
			...state,
			categoryList: payload,
		}),
		setAdvantageList: (state, payload: object) => ({
			...state,
			advantageList: payload,
		}),
		setSchtickList: (state, payload: object) => ({
			...state,
			schtickList: payload,
		}),
		setRentCreateObj: (state, payload: object) => ({
			...state,
			rentCreateObj: payload,
		}),
		setRentUpdateObj: (state, payload: object) => ({
			...state,
			rentUpdateObj: payload,
		}),
		setSearchObj: (state, payload: object) => ({
			...state,
			searchObj: payload,
		}),
		setSearchList: (state, payload: object) => ({
			...state,
			searchList: payload,
		}),
		setChatList: (state, payload: object) => ({
			...state,
			chatList: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			async getCode(phone) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getCode(phone)
					.then(response => {
						console.warn('getCode -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async sendCode(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.sendCode(payload.phone, payload.code)
					.then(response => {
						console.warn('sendCode -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							StorageHelper.saveData('token', response.data.token);
							API.setToken(response.data.token);
							resolve()
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getProfile() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getProfile()
					.then(response => {
						console.warn('getProfile -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data.data);
							resolve(response.data.data)
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async editProfile(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.editProfile(payload.first_name, payload.last_name, payload.sex, payload.email)
					.then(response => {
						console.warn('editProfile -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data.data);
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async setPushId(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.setPushId(payload)
					.then(response => {
						console.warn('setPushId -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data.data);
							resolve()
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getCategoryList(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getCategoryList(payload?.level, payload?.slug)
					.then(response => {
						console.warn('getCategoryList -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setCategoryList(response.data.data);
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getCategoryList2(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getCategoryList(payload?.level, payload?.slug)
					.then(response => {
						console.warn('getCategoryList2 -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async searchAddress(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.searchAddress(payload)
					.then(response => {
						console.warn('searchAddress -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.suggestions);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getAddressFromCoords(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getAddressFromCoords(payload.lat, payload.lon)
					.then(response => {
						console.warn('getAddressFromCoords -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.suggestions);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getAdvantages() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getAdvantages()
					.then(response => {
						console.warn('getAdvantages -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setAdvantageList(response.data.data);
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getAdvantages2(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getAdvantages(payload)
					.then(response => {
						console.warn('getAdvantages2 -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getSchtick() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getSchtick()
					.then(response => {
						console.warn('getSchtick -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setSchtickList(response.data.data);
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async createAdv(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.createAdv(payload.header, payload.description, payload.geo, payload.appartament_number, payload.region, payload.street, payload.zip_number, payload.country, payload.guest, payload.beds, payload.rooms, payload.bathrooms, payload.status, payload.price, payload.categories, payload.facilities, payload.schticks, payload.additional_tags, payload.popular_facilities, payload.safety_facilities)
					.then(response => {
						console.warn('createAdv -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setRentUpdateObj(response.data.url);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async updateAdv(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.updateAdv(payload.id, payload.header, payload.description, payload.geo, payload.appartament_number, payload.region, payload.street, payload.zip_number, payload.country, payload.guest, payload.beds, payload.rooms, payload.bathrooms, payload.status, payload.price, payload.categories, payload.facilities, payload.schticks, payload.additional_tags, payload.popular_facilities, payload.safety_facilities)
					.then(response => {
						console.warn('updateAdv -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async uploadPhotoToAdv(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.uploadPhotoToAdv(payload.id, payload.photo)
					.then(response => {
						console.warn('uploadPhotoToAdv -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async searchAdv(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.searchAdv(payload)
					.then(response => {
						console.warn('searchAdv -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setSearchList(response.data.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async setFavor(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.setFavor(payload)
					.then(response => {
						console.warn('setFavor -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getFavors() {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getFavors()
					.then(response => {
						console.warn('getFavors -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async startChat(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.startChat(payload)
					.then(response => {
						console.warn('startChat -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getAllChats() {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getAllChats()
					.then(response => {
						console.warn('getAllChats -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setChatList(response.data.data);
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getChat(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getChat(payload)
					.then(response => {
						console.warn('getChat -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async sendMessage(payload) {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.sendMessage(payload.object_id, payload.message_text)
					.then(response => {
						console.warn('sendMessage -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
			async getObject(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getObject(payload)
					.then(response => {
						console.warn('getObject -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve(response.data.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						reject('Произошла неизвестная ошибка! Повторите снова.');
					});
				});
			},
		}
	},
});

export default user;