export const BLUE_COLOR = '#226DDD';
export const BLACK_COLOR = '#18212D';
export const YELLOW_COLOR = '#FF9900';
export const GREEN_COLOR = '#27AE60';
export const GRAY_COLOR = '#6C7682';
export const WHITE_COLOR = '#FFFFFF';
export const LIGHT_GRAY_COLOR = '#F5F6FA';
export const RED_COLOR = '#F24D4D';