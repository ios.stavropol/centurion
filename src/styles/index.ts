import * as colors from './colors';
import * as typography from './typography';
import * as spacing from './spacing';
import * as shadows from './shadows';

export { colors, typography, spacing, shadows };
